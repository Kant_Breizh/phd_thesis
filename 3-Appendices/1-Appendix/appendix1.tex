%!TEX root = ../thesis.tex
% ******************************* Thesis Appendix A ****************************
\chapter{Derivation of $A_{\Pi_T,g}(\tau,\nu)$}
\label{app:A}
Consider a rectangular window of length $T$, noted $\Pi_T$ and a filter $g$ of length $T_g$. We assume that $g$ respects the following properties: it is equal to $0$ anywhere except for $t \in [a,\ a+T_g]$ and is Lebesgue integrable on its non null area. Note that this is not a strong assumption and that filters commonly used in the literature respect it. We also add the condition that $T_g > T$. Under these assumptions, it is possible to define $g$ as a truncation of a periodic signal $g_\infty(t)$ defined as
\begin{equation}
g_\infty(t) = g(\frac{t-a}{T_g}), \forall t \in \mathbb{R}.
\end{equation}

With this definition, $g_\infty(t)$ is a $T_g$-periodic signal which is Lebesgue integral on one period. Therefore, it can be decomposed in Fourier series as
\begin{equation}
g_\infty(t) = \sum_{k \in \mathbb{Z}}G_k e^{j2\pi k\frac{t}{T_g}}, t \in \mathbb{R}
\end{equation}
where $G_k$ are the Fourier coefficients of $\tilde{g}$.
Therefore, the cross-ambiguity function of $g$ and a rectangular window of length $T$ is equal to 
\begin{align}
A_{\Pi_T,g}(\tau,\nu) &= \int\limits_{0}^{T}g(t-\tau)e^{j2\pi t\nu}dt,
\\
&= \sum_{k \in \mathbb{Z}}G_ke^{-j2\pi k\frac{\tau}{T_g}}\int\limits_{0}^{T}e^{j2\pi t(\frac{k}{T_g}+\nu)}dt
\end{align}
which simplifies into different expressions according to the values of $\tau$.\\

\subsubsection{$T_g\geq T$ case}
Here, we consider the case $T_g\geq T$. 

\textbf{Case I:} $\tau > T-a$ or $\tau < -(a+T_g)$.

In that case, $g(t-\tau)$ does not overlap the rectangular window and 
\begin{equation}
A_{\Pi_T,g}(\tau,\nu) = 0
\label{eq:caseI}
\end{equation}

\textbf{Case II:} $-(a+T_g)\leq \tau \leq T-(a+T_g)$

In that case, only a small part of the filter overlaps the beginning of the rectangular window and 
\begin{equation}
A_{\Pi_T,g}(\tau, \nu) =\sum_{k \in \mathbb{Z}}G_ke^{-j2\pi k\frac{\tau}{T_g}}\int\limits_{0}^{a+\tau+T_g}e^{j2\pi t(\frac{k}{T_g}+\nu)}dt
\label{eq:caseII}
\end{equation}

\textbf{Case III:} $-a \leq \tau \leq T-a$

In that case, only a small part of the filter overlaps the end of the rectangular window and 
\begin{equation}
A_{\Pi_T,g}(\tau, \nu) =\sum_{k \in \mathbb{Z}}G_ke^{-j2\pi k\frac{\tau}{T_g}}\int\limits_{a+\tau}^{T}e^{j2\pi t(\frac{k}{T_g}+\nu)}dt
\label{eq:caseIII}
\end{equation}

\textbf{Case IV:} $T-(a+T_g) \leq \tau \leq -a$

In that case, the filter $g$ overlaps with the whole rectangular window and 
\begin{equation}
A_{\Pi_T,g}(\tau, \nu) =\sum_{k \in \mathbb{Z}}G_ke^{-j2\pi k\frac{\tau}{T_g}}\int\limits_{0}^{T}e^{j2\pi t(\frac{k}{T_g}+\nu)}dt
\label{eq:caseIV}
\end{equation}

In order to give a simplified expression of $A_{\Pi_T,g}(\tau,\nu)$ in all the listed cases, we derive in the following lines the generic expression of $I_b^c = \int\limits_{b}^{c}e^{j2\pi t(\frac{k}{T_g}+\nu)}$.
\begin{align}
I_b^c &=\frac{e^{j2\pi c(\frac{k}{T_g}+\nu)}-e^{j2\pi b(\frac{k}{T_g}+\nu)}}{j2\pi(\frac{k}{T_g}+\nu)}\\
&= (c-b)e^{j\pi(\frac{k}{T_g}+\nu)(b+c)}\text{sinc}\left(\pi(\frac{k}{T_g}+\nu)(c-b)\right)
\label{eq:Ibc}
\end{align}


Putting $(\ref{eq:Ibc})$ into $(\ref{eq:caseI})-(\ref{eq:caseIV})$, we obtain the expression of (\ref{eq:CrossAmbTsmaller}) for $A_{\Pi_T,g}(\tau,\nu)$.

		\begin{flalign}
		\small
		&A_{\Pi_T,g}(\tau,\nu) =&\nonumber\\
		&\begin{cases}
		0, &\tau > T-a \text{ or } \tau < -(a+T_g).\\
		(a+\tau+T_g)e^{j\pi\nu(a+\tau+T_g)}\sum\limits_{k \in \mathbb{Z}}G_ke^{j\pi\frac{k}{T_g}(a-\tau+T_g)}\text{sinc}(\pi(\frac{k}{T_g}+\nu)(a+\tau+T_g)), &-(a+T_g)\leq \tau \leq T-(a+T_g) \\
		(T-(a+\tau))e^{j\pi\nu(a+\tau+T)}\sum\limits_{k \in \mathbb{Z}}G_ke^{j\pi\frac{k}{T_g}(a-\tau+T)}\text{sinc}(\pi(\frac{k}{T_g}+\nu)(T-(a+\tau))), &-a \leq \tau \leq T-a\\
		Te^{j\pi\nu T}\sum\limits_{k \in \mathbb{Z}}G_ke^{j\pi\frac{k}{T_g}(T-2\tau)}\text{sinc}(\pi(\frac{k}{T_g}+\nu)T), &T-(a+T_g) \leq \tau \leq -a
		\end{cases}&\label{eq:CrossAmbTsmaller}
		\end{flalign}