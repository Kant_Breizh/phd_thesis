%!TEX root = ../../thesis.tex
% ******************************* Thesis Appendix B ********************************
\graphicspath{{3-Appendices/2-Appendix/Images/}}
\chapter{SCEE Testbed}
\label{app:testbed}

Testbeds are becoming an increasingly important part of research on wireless communication. Indeed, given the increasing complexity of communication networks, it is not always feasible to tackle all aspects of a problem through theoretical analysis only.  Therefore, a large number of research groups have constituted testbeds with different capabilities. Famous examples include the Iris testbed at Trinity College Dublin\footnote{\url{https://iris-testbed.connectcentre.ie/}} \cite{irisTestbed}, the 5G USRP testbed at KU Leuven described in \cite{ChenTestbed}\footnote{\url{http://www.esat.kuleuven.be/telemic/research/NetworkedSystems/networked-systems-infrastructure}} and the CorteXlab at INSA Lyon in France \cite{CortexLab}\footnote{\url{http://www.cortexlab.fr}}.

\section{Testbed presentation and layout}
The SCEE Testbed is a software radio platform that has been designed, built and managed by the different PhD students of SCEE research team at CentraleSupelec. The aforementioned testbeds offer remote access to a variety of hardware components and important software capabilities. However, it is sometimes necessary to have access to these tools on-site as the remote access does not necessarily enable users to obtain the hardware configurations they desire, which is why we decided to build the SCEE Testbed, which is much less ambitious than previously mentioned platforms but is more convenient to use on a day-to-day basis. Its goal is to provide the students and professors of the research team, as well as undergrad students interested in wireless communications, with a simple to access, hassle-free radio experimentation platform. The experiment presented in chapter 4 has been performed on top of this testbed. Here, we present its architecture in more details.\footnote{Videos of most experiments that are run on top of the SCEE testbed can be found on SCEE youtube channel: \url{https://goo.gl/8XeJqs} }

%\begin{figure}[t]
%\centering
%\includegraphics[width=0.9\linewidth]{layout}
%\caption{Testbed layout}
%\label{fig:testbed_layout}
%\end{figure}
The testbed structure itself simply consists of a wooden shelf composed of 5 levels. The two upper levels as well as the two bottom levels are composed of five casings which can each house one USRP. In total, the structure can therefore house twenty USRPs. The center level on the other hand is supposed to house larger instrumentation such as the gigabit ethernet switch and the clock distribution module. Furthermore, gaps between the different levels of the structure have been designed to allow for easy and elegant cabling.

\section{Available hardware material}
\begin{table}[t]
\caption{USRPS available in SCEE Testbed}
\resizebox{\textwidth}{!}{%
\small
\begin{tabularx}{\linewidth}{|c|c|X|c|c|X|}
\hline\hline
\textbf{Model} & \makecell{\textbf{Mounted}\\\textbf{daughter-}\\\textbf{board}} & \makecell{\textbf{Transmit}\\\textbf{frequencies}} & \textbf{Bandwidth} & \textbf{Quantity} & \ \makecell{\textbf{Comment}}\\
\hline\hline
N210 & SBX & 400 MHz - 4.4 GHz & 40 MHz & 10 & \makecell{Used for most\\ day-to-day\\ experiments}\\
\hline
X310 & UBX-160 $\times$ 2 & 10 MHz - 6 GHz & 160 MHz & 1 &\makecell{Used for FPGA\\ reconfiguration\\ applications}\\
\hline
E110 & WBX & 50 MHz - 2.2 GHz & 40MHz & 2 & \makecell{Standalone USRP\\equipped with\\an embedded\\processor.}\\
\hline
B210 & N/A & 70 MHz - 6 GHz & 56 MHz & 1 & \makecell{USB powered,\\small form-factor\\convenient USRP\\easily usable\\ by students}\\
\hline\hline
\end{tabularx}}
\label{tab:USRPs}
\end{table}
We list the USRPs that are available in the SCEE testbed in Table \ref{tab:USRPs}. Most day-to-day experiments and demonstrations are run with N210 USRPs that are available a large amount. Other experimentations with specific needs can be performed on either the available X310, E110 or B210 USRPs. All additional material is referenced in \ref{tab:other_hw}, where we have referenced the different network components, antennas and spare daughterboards available at the testbed. Note that we have not drawn an extensive list of all SMA, ethernet or MIMO cables but they are of course available in sufficient quantity.

\begin{table}[t]
\caption{Other hardware}
\resizebox{\textwidth}{!}{%
\small
\begin{tabularx}{\linewidth}{|c|c|c|c|c|c|}
\hline\hline
\multicolumn{6}{|X|}{\textbf{Hardware material}}\\
\hline
{Designation} & \multicolumn{5}{X|}{\makecell{Details}}\\
\hline\hline
Dell Inspiron T5810 Workstation& \multicolumn{5}{X|}{\makecell{Workstation used to access all USRPs\\
\emph{OS:} Ubuntu 16.04.2 LTS \\
\emph{CPU:} Intel Xeon E5-1650 v4 @ 3.6 GHz\\
\emph{Memory:} 4 $\times$ 8 Go DDR4 SDRAM\\
\emph{Network interfaces:} Gb ethernet $\times$ 2, 10Gb ethernet $\times$ 2 }}\\\hline
Gb Ethernet Switch & \multicolumn{5}{X|}{24-ports Switch used to connect all USRPs and the workstation}\\\hline
OctoClock-G CDA-2990 & \multicolumn{5}{X|}{8-Channel clock distribution module}\\
\hline\hline\hline
\multicolumn{6}{|X|}{\textbf{Spare daughterboards}}\\
\hline
Model & \multicolumn{3}{c|}{Transmit Frequencies} & Bandwidth & Quantity\\
\hline\hline
WBX & \multicolumn{3}{c|}{50 MHz - 2.2 GHz} & 40 MHz & 4\\\hline
CBX & \multicolumn{3}{c|}{1.2 GHz - 6 GHz} & 40 MHz & 1\\
\hline\hline\hline
\multicolumn{6}{|X|}{\textbf{Antennas}}\\
\hline
Model & \multicolumn{3}{c|}{Transmit Frequencies} & Details & Quantity\\\hline\hline
Vert2450 & \multicolumn{3}{c|}{\makecell{Dual-band,\\ 2.4 - 2.48 GHz \&\\ 4.9 - 5.9 GHz}} & \makecell{omni-directional,\\ 3dBi Gain} & 28\\\hline
Vert900 & \multicolumn{3}{c|}{\makecell{Dual-band,\\ 824 - 960 MHz \&\\ 1710 - 1990 MHz}} & \makecell{omni-directional,\\ 3dBi Gain} & 5\\\hline
Vert400 & \multicolumn{3}{c|}{\makecell{Tri-band,\\  144 MHz,\\ 400 MHz \&\\ 1200 MHz}} & omni-directional & 28\\\hline
LP0965 & \multicolumn{3}{c|}{\makecell{Log-periodic,\\ 850 MHz - 6.5 GHz}} & \makecell{Directional antenna,\\ 5-6 dBi gain} & 1\\\hline
LP0410 & \multicolumn{3}{c|}{\makecell{Log-periodic,\\ 400 MHz - 1 GHz}} & \makecell{Directional antenna,\\ 5-6 dBi gain} & 1\\
\hline\hline
\end{tabularx}}
\label{tab:other_hw}
\end{table}

\section{Testbed organization and network architecture}
\begin{figure}
\centering
\begin{tikzpicture}[xscale=0.7, yscale=0.7]
\draw[ystep=2.0, xstep=3.0, very thick, black] (0,0) grid (15,-4);
\draw[ystep=2.0, xstep=3.0, shift={(0,1)}, very thick, black] (0,-5) grid (15,-10);
\foreach \i in {0,...,1}{
	\node[] at(-0.5,-2*\i-1) {$\i$};
	}
\foreach \i in {2,...,3}{
	\node[] at(-0.5,-2*\i-2) {$\i$};
	}
\foreach \j in {0,...,4}{
	\node[] at(3*\j+1.5,0.5) {$\j$};
	}
\foreach \i in {0,...,1}{
\foreach \j in {3,...,4}{
	\draw[fill=green] (3*\j,-2*\i) rectangle node[]{N210}(3*\j+3,-2*\i-2);
	}
}
\foreach \i in {2,...,3}{
\foreach \j in {3,...,4}{
	\draw[fill=green] (3*\j,-2*\i-1) rectangle node[]{N210}(3*\j+3,-2*\i-3);
	}
}
\draw[fill=green] (3*2,-2*0) rectangle node[]{N210}(3*2+3,-2*0-2);
\draw[fill=green] (3*2,-2) rectangle node[]{N210}(3*2+3,-4);
\draw[fill=red] (0,0) rectangle node[]{B210}(3,-2);
\draw[fill=orange] (3,-2) rectangle node[]{E110}(6,-4);
\draw[fill=orange] (3,0) rectangle node[]{E110}(6,-2);
\draw[fill=violet] (0,-2) rectangle node[]{X310}(3,-4);
\foreach \i in {2,...,3}{
\foreach \j in {0,...,2}{
	\draw[fill=lightgray] (3*\j,-2*\i-1) rectangle node[]{Free}(3*\j+3,-2*\i-3);
	}
}
\draw[fill=black] (6,-4) rectangle  (9,-5);
\draw[fill=teal] (9,-4) rectangle node[]{Octoclock} (15,-5);
\draw[fill=teal] (0,-4) rectangle node[]{Ethernet switch} (6,-5);
\draw[dashed, very thick, brown] (8.9,0.1) rectangle (15.1,-9.1);
\node[] at (12,-9.5) {\textcolor{brown}{Synchronized MIMO capable block}};
\draw[very thick, green, ->,>=latex](0,0) -- node[near end, above]{$j$}(1.5,0);
\draw[very thick, green, ->,>=latex](0,0) -- node[at end, left]{$i$}(0,-1.5);
\end{tikzpicture}
\caption{Testbed organization and USRP identification}
\label{fig:testbed_org}
\end{figure}
The testbed is organized as a (4,5) matrix, and each USRP is identified by its id $5i+j+1$ where $i$ indexes rows and $j$ columns. Note that index start at 0. The grid indexation and USRP identification method is detailed in Fig.~\ref{fig:testbed_org}. For example, according to our identification scheme, the USRP B210 has id $1$ and the 2 USRP E110 have respectively ids $2$ and $7$. In Fig.~\ref{fig:testbed_org}, we also show that the 8 USRP N210 hosted in columns $3$ and $4$ compose a fully synchronized block. Indeed, each of them is connected to the octoclock. Furthermore, they are connected in pairs via MIMO cables, which makes it possible to perform $4\times 4$ MIMO transmission and reception. In details, the USRPs are connected in pairs as follows: (4,9), (5, 10), (14, 19) and (15, 20). Note that 6 compartments are still free and can be used in the future to make the testbed evolve. 

In Fig.\ref{fig:ethernet switch}, we represent the ethernet cabling plan of the testbed. As we see, the Gb ethernet switch disposes of 24 ports. 20 of them are therefore dedicated to USRP communication and we have represented on each of them the id of the testbed compartment it should be connected to. The 4 ports on the left side are each assigned a specific purpose: one is reserved for administration purpose and is therefore left free. Just beneath the latter is the port to which the testbed workstation is connected. The octoclock is also connected to one port of the switch. Finally, the port labeled as "local access" can be used to directly plug one's personal computer, which allows anyone to bypass the workstation and execute experimentation software directly on their laptop.

\begin{figure}
\centering
\begin{tikzpicture}[scale=0.5]
\draw[] (0,0) rectangle (16,3);
\draw[thick, shift={(0,0.5)}] (1,0) grid (5,2);
\draw[thick, shift={(0,0.5)}] (6,0) grid (10,2);
\draw[thick, shift={(0,0.5)}] (11,0) grid (15,2);
\draw[fill=darkgray] (1,1.5) rectangle(2,2.5);
\draw[->, >=latex] (0,3.5)  -- node[at start, above]{Reserved} (0.5,3.5) -- (1.5,2);
\draw[->, >=latex] (0,-0.5)  -- node[at start, below]{Workstation} (0.5,-0.5) -- (1.5,1);
\draw[->, >=latex] (4,-1.2)  -- node[near start, below]{Octoclock} (3.5,-1.2) -- (2.5,1);
\draw[->, >=latex] (4,3.8)  -- node[near start, above]{Local access} (3.5,3.8) -- (2.5,2);
\edef\a{0}
\foreach \i in {0,...,1}{
	\foreach \j in {3,4,6,7,8,9,11,12,13,14}{
		\pgfmathparse{\a+1}
		\xdef\a{\pgfmathresult}
		\node[] at (\j+0.5,2-\i) {\pgfmathprintnumber{\a}};
	}
}
\end{tikzpicture}
\caption{Ethernet switch cabling}
\label{fig:ethernet switch}
\end{figure}

\begin{figure}
\centering
\begin{tikzpicture}[xscale=1.1]
\draw[very thick] (9,2.87) -- (10.75,2.87);
\draw[very thick] (9,1.62) -- (10.75,1.62);
\draw[very thick] (9,0.37) -- (10.75,0.37);
\draw[very thick] (9,-0.88) -- (10.75,-0.88);
\node[] at (-0.5,2.12) {\rotatebox{90}{Workstation}};
\draw[] (0,0) rectangle (2,4.25);
\draw[fill=lightgray] (0.5,0.5) rectangle node[]{10 Gb eth.} (2.5,1.25);
\draw[fill=lightgray] (0.5,1.75) rectangle node[]{USB 3} (2.5,2.5);
\draw[fill=lightgray] (0.5,3) rectangle node[]{1 Gb eth.} (2.5,3.75);
\draw[fill=violet] (3.5, 0) rectangle node[]{6}(5,0.75);
\draw[fill=red] (3.5, 1.25) rectangle node[]{1}(5,2);
\draw[fill=orange] (3.5, 2.5) rectangle node[]{2}(5,3.25);
\draw[fill=orange] (5.5, 2.5) rectangle node[]{7}(7,3.25);
\draw[fill=green] (5.5, 1.25) rectangle node[]{3}(7,2);
\draw[fill=green] (5.5, 0) rectangle node[]{8}(7,0.75);
\draw[fill=green] (7.5, 1.25) rectangle node[]{9}(9,2);
\draw[fill=green] (7.5, 0) rectangle node[]{5}(9,0.75);
\draw[fill=green] (7.5, 2.5) rectangle node[]{4}(9,3.25);
\draw[fill=green] (7.5, -1.25) rectangle node[]{10}(9,-0.5);
\draw[fill=teal] (3.5, 3.5) rectangle node[]{Ethernet switch} (12.75, 4.25);
\draw[fill=teal] (9.5, -1.25) rectangle node[]{\rotatebox{90}{Octoclock}} (10.25, 3.25);
\draw[fill=green] (10.75, 1.25) rectangle node[]{19}(12.25,2);
\draw[fill=green] (10.75, 0) rectangle node[]{15}(12.25,0.75);
\draw[fill=green] (10.75, 2.5) rectangle node[]{14}(12.25,3.25);
\draw[fill=green] (10.75, -1.25) rectangle node[]{20}(12.25,-0.5);
\draw[red, thick] (2.5, 2.12) -- (3.5,1.72);
\draw[blue, dashed, thick] (2.5, 0.87) -- (3.5,0.45);
\draw[blue, thick] (2.5, 3.38) -- (3.5,3.82); 
\draw[blue, thick] (4.25, 3.25) -- (4.25,3.5);
\draw[blue, thick] (6.25, 3.25) -- (6.25,3.5);
\draw[blue, thick] (5.3, 3.5) -- (5.3,1.62) -- (5.5, 1.62);
\draw[blue, thick] (5.1, 3.5) -- (5.1,0.375) -- (5.5, 0.375);
\draw[blue, thick] (8.25, 3.25) -- (8.25,3.5);
\draw[blue, thick] (7.4, 3.5) -- (7.4,1.62) -- (7.5, 1.62);
\draw[blue, thick] (7.25, 3.5) -- (7.25,0.375) -- (7.5, 0.375);
\draw[blue, thick] (7.1, 3.5) -- (7.1,-0.8) -- (7.5, -0.8);
\draw[blue, thick] (11.5, 3.25) -- (11.5,3.5);
\draw[blue, thick] (9.9, 3.25) -- (9.9,3.5);
\draw[blue, thick] (12.35, 3.5) -- (12.35,1.62) -- (12.25, 1.62);
\draw[blue, thick] (12.5, 3.5) -- (12.5,0.375) -- (12.25, 0.375);
\draw[blue, thick] (12.65, 3.5) -- (12.65,-0.8) -- (12.25, -0.8);
\draw[brown, very thick,|-|] (7.7, 2.7) -- (7.7,1.8);
\draw[brown, very thick,|-|] (12.05, 2.7) -- (12.05,1.8);
\draw[brown, very thick,|-|] (7.7, 0.2) -- (7.7,-0.7);
\draw[brown, very thick,|-|] (12.05, 0.2) -- (12.05,-0.7);
\draw[fill=lightgray] (0,-0.5) rectangle (5.5,-3);
\draw[thick, blue] (0.25, -1) -- (0.75,-1);
\node[right] at (0.825, -1) {1 Gb link};
\draw[thick, blue, dashed] (3, -1) -- (3.5,-1);
\node[right] at (3.6, -1) {10 Gb link};
\draw[thick, red] (3, -1.75) -- (3.5,-1.75);
\node[right] at (3.55, -1.75) {USB link};
\draw[very thick, brown, |-|] (0.25, -1.75) -- (0.75,-1.75);
\node[right] at (0.825, -1.75) {MIMO cable};
\draw[very thick, black] (0.25, -2.5) -- (0.75,-2.5);
\node[right] at (0.825, -2.5) {SMA link};
\end{tikzpicture}
\caption{Detailed cabling diagram of all testbed components. USRPs are represented via their id.}
\label{fig:cabling}
\end{figure}

Note that even if a port of the switch is reserved for each compartment of the testbed, not all USRPs are connected to the former. Indeed, the B210 and X310 USRPs do not have a Gb ethernet interface. The former is plugged onto a USB port of the workstation, while the latter is directly connected to it through a 10 Gb ethernet interface. All these different cablings are summarized in Fig.~\ref{fig:cabling}. All USRPs connected to the ethernet switch are addressed in the same subnet and assigned the ip address 192.168.10.1xx where xx is replaced by their id. For example, USRP 7 is addressed as 192.168.10.107.



\begin{figure}[t]
\centering
\includegraphics[width=0.95\linewidth]{monitor}
\caption[Interface of the SCEE testbed monitor.]{Interface of the SCEE testbed monitor. This example was captured with a prior testbed configuration that does not correspond to the one we have presented. Here, two different USRPs are used by two different persons, and the monitoring software has successfully detected it.}
\label{fig:monitor}
\end{figure}

\section{Usage monitoring}
The main goal of this testbed is to make it easy for multiple people to access it and run experiments at the same time. Therefore, multiple people could try to access the same USRP at the same time. Unfortunately and quite surprisingly, no locking mechanism is implemented by the USRP drivers, so that it is impossible to prevent an USRP from being preempted by someone while it is used in an experiment. Therefore, we have implemented a usage monitor which makes it possible, via a web application, to see in real time what USRPs are used and by whom. 

The software, available online at \url{https://goo.gl/6FAHLU}, is based on a Python Flask web server. The application itself functions by monitoring all UDP and TCP sockets that are opened from the workstation to any of the IP addresses corresponding to the testbed USRPs. If it detects a relevant socket, the software then retrieves the associated pid which enables it to find who is using the USRP in question. This process is executed at a customizable frequency. Once the state of each monitored IP has been retrieved, the web server updates the interface. A snapshot of the said interface is shown in Fig.~\ref{fig:monitor}. This monitoring system has proven to be quite easy to use and is an efficient way to share resources fairly between all persons accessing the testbed. Furthermore, it can be adapted quite easily to any USRP testbed architecture.