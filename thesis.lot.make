\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\addvspace {10\p@ }
\contentsline {xchapter}{General Introduction}{3}{chapter.1}
\addvspace {10\p@ }
\contentsline {xchapter}{Background and problem statement}{15}{chapter.2}
\contentsline {table}{\numberline {2.1}{\ignorespaces Examples of channel models}}{20}{table.caption.21}
\contentsline {subtable}{\numberline {(a)}{\ignorespaces {}}}{20}{subtable.1.1}
\contentsline {subtable}{\numberline {(b)}{\ignorespaces {}}}{20}{subtable.1.2}
\contentsline {table}{\numberline {2.2}{\ignorespaces Waveform configurations considered throughout the thesis\relax }}{40}{table.caption.45}
\addvspace {10\p@ }
\contentsline {xchapter}{Related work and corresponding contributions}{51}{chapter.3}
\contentsline {table}{\numberline {3.1}{\ignorespaces Interference tables in dB computed according to the PSD-based model for studied waveforms.\relax }}{56}{table.caption.62}
\addvspace {10\p@ }
\contentsline {xchapter}{Analysis of interference between CP-OFDM and FB-MC waveforms}{65}{chapter.4}
\addvspace {10\p@ }
\contentsline {xchapter}{Coexistence with CP-OFDM incumbent systems in CR setups}{93}{chapter.5}
\addvspace {10\p@ }
\contentsline {xchapter}{Asynchronous D2D operation in 5G networks}{105}{chapter.6}
\contentsline {table}{\numberline {6.1}{\ignorespaces Useful and interference channels for Fig.~\ref {fig:simple_model}\relax }}{108}{table.caption.91}
\contentsline {table}{\numberline {6.2}{\ignorespaces Simulation parameters\relax }}{114}{table.caption.102}
\contentsline {table}{\numberline {6.3}{\ignorespaces Bandwidth Efficiency of Waveforms\relax }}{125}{table.caption.117}
\contentsline {table}{\numberline {6.4}{\ignorespaces Simulation parameters\relax }}{129}{table.caption.122}
\addvspace {10\p@ }
\contentsline {xchapter}{Conclusions and perspectives}{145}{chapter.7}
\addvspace {10\p@ }
\contentsline {xchapter}{Derivation of $A_{\Pi _T,g}(\tau ,\nu )$}{153}{Appendix.a.A}
\addvspace {10\p@ }
\contentsline {xchapter}{SCEE Testbed}{157}{Appendix.a.B}
\contentsline {table}{\numberline {B.1}{\ignorespaces USRPS available in SCEE Testbed\relax }}{158}{table.caption.158}
\contentsline {table}{\numberline {B.2}{\ignorespaces Other hardware\relax }}{163}{table.caption.159}
\addvspace {10\p@ }
\contentsline {xchapter}{Network-level simulator}{165}{Appendix.a.C}
\addvspace {10\p@ }
\contentsline {xchapter}{Publications and Involvement in R\&D Projects}{173}{Appendix.a.D}
