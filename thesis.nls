\begin{thenomenclature} 

 \nomgroup{A}

  \item [{3G}]\begingroup 3rd generation of communication networks\nomeqref {1.0}
		\nompageref{4}
  \item [{3GPP}]\begingroup 3rd Generation Partnership Project\nomeqref {1.0}
		\nompageref{6}
  \item [{4G}]\begingroup 4th generation of communication networks\nomeqref {1.0}
		\nompageref{4}
  \item [{5G}]\begingroup 5th generation of communication networks\nomeqref {1.0}
		\nompageref{4}
  \item [{5GNOW}]\begingroup 5th Generation Non-Orthogonal Waveforms for Asynchronous Signalling\nomeqref {2.18}
		\nompageref{34}
  \item [{ACCENT5}]\begingroup Advanced Waveforms, MAC Design and Dynamic Radio Resource Allocation for Device-to-Device in 5G Wireless Networks\nomeqref {2.18}
		\nompageref{32}
  \item [{AI}]\begingroup Artificial Intelligence\nomeqref {1.0}
		\nompageref{3}
  \item [{AIM}]\begingroup Asynchronous Interference Modeling\nomeqref {3.12}
		\nompageref{58, 59}
  \item [{ANR}]\begingroup Agence Nationale de la Recherche\nomeqref {2.18}
		\nompageref{32}
  \item [{AWGN}]\begingroup Additive White Gaussian Noise\nomeqref {2.0}
		\nompageref{17}
  \item [{BER}]\begingroup Bit Error Rate\nomeqref {2.0}\nompageref{17}
  \item [{BLT}]\begingroup Balian-Low Theorem\nomeqref {2.18}
		\nompageref{34}
  \item [{CDMA}]\begingroup Code Division Multiple Access\nomeqref {2.13}
		\nompageref{24}
  \item [{CFO}]\begingroup Carrier Frequency Offset\nomeqref {2.18}
		\nompageref{30}
  \item [{COQAM}]\begingroup Circular OQAM\nomeqref {2.19}
		\nompageref{37}
  \item [{CP}]\begingroup Cyclic Prefix\nomeqref {2.13}\nompageref{24}
  \item [{CR}]\begingroup Cognitive Radio\nomeqref {2.18}
		\nompageref{27}
  \item [{CU}]\begingroup Cellular User\nomeqref {C.0}\nompageref{171}
  \item [{CUE}]\begingroup Cellular User Equipment\nomeqref {6.10}
		\nompageref{120}
  \item [{CVX}]\begingroup Matlab Software for Disciplined Convex Programming\nomeqref {5.3}
		\nompageref{98}
  \item [{D2D}]\begingroup Device To Device\nomeqref {1.0}
		\nompageref{10}
  \item [{DSA}]\begingroup Dynamic Spectrum Access\nomeqref {2.18}
		\nompageref{27}
  \item [{DUE}]\begingroup D2D User Equipment\nomeqref {6.10}
		\nompageref{120}
  \item [{DVB-T}]\begingroup Digital Video Broadcasting-Terrestrial\nomeqref {1.0}
		\nompageref{8}
  \item [{EMPhAtiC}]\begingroup Enhanced Multicarrier Techniques for Professional Ad-Hoc and Cell-Based Communications\nomeqref {2.18}
		\nompageref{34}
  \item [{EPA}]\begingroup Extended Pedestrian A\nomeqref {2.6}
		\nompageref{20}
  \item [{ETU}]\begingroup Extended Typical Urban\nomeqref {2.6}
		\nompageref{20}
  \item [{EVM}]\begingroup Error Vector Magnitude\nomeqref {2.0}
		\nompageref{17}
  \item [{f-OFDM}]\begingroup Filtered OFDM\nomeqref {2.19}
		\nompageref{38}
  \item [{FB-MC}]\begingroup Filter Bank Multi-Carrier\nomeqref {1.0}
		\nompageref{8}
  \item [{FBMC/PAM}]\begingroup Filter Bank Multi Carrier with PAM\nomeqref {2.18}
		\nompageref{36}
  \item [{FDMA}]\begingroup Frequency Division Multiple Access\nomeqref {2.13}
		\nompageref{24}
  \item [{FFR}]\begingroup Fractional Frequency Reuse\nomeqref {6.10}
		\nompageref{120}
  \item [{FFT}]\begingroup Fast Fourier Transform\nomeqref {2.13}
		\nompageref{24}
  \item [{FMT}]\begingroup Filtered Multi-Tone\nomeqref {2.18}
		\nompageref{35}
  \item [{GFDM}]\begingroup Generalized Frequency Division Multiplexing\nomeqref {2.18}
		\nompageref{37}
  \item [{GUI}]\begingroup Graphical User Interface\nomeqref {5.3}
		\nompageref{98}
  \item [{ICI}]\begingroup Inter Carrier Interference\nomeqref {2.13}
		\nompageref{24}
  \item [{IFFT}]\begingroup Inverse Fast Fourier Transform\nomeqref {2.13}
		\nompageref{24}
  \item [{IoT}]\begingroup Internet of Things\nomeqref {1.0}
		\nompageref{3}
  \item [{IQR}]\begingroup Inter-Quartile Range\nomeqref {6.18}
		\nompageref{130}
  \item [{ISI}]\begingroup Inter Symbol Interference\nomeqref {2.13}
		\nompageref{24}
  \item [{ISM}]\begingroup Industrial, Scientific and Medical\nomeqref {1.0}
		\nompageref{6}
  \item [{ITU-R}]\begingroup International Telecommunication Union Radiocommunication Sector\nomeqref {2.6}
		\nompageref{20}
  \item [{LO}]\begingroup Local Oscillator\nomeqref {6.18}
		\nompageref{130}
  \item [{LPWAN}]\begingroup Low Power Wide Area Network\nomeqref {1.0}
		\nompageref{5}
  \item [{LTE}]\begingroup Long Term Evolution\nomeqref {1.0}
		\nompageref{6}
  \item [{LTE-M}]\begingroup LTE for Machines\nomeqref {1.0}
		\nompageref{6}
  \item [{LTE-U}]\begingroup LTE Unlicensed\nomeqref {1.0}
		\nompageref{7}
  \item [{M2M}]\begingroup Machine to Machine\nomeqref {2.18}
		\nompageref{34}
  \item [{MAC}]\begingroup Medium Access Control\nomeqref {1.0}
		\nompageref{8}
  \item [{METIS}]\begingroup Mobile and wireless communications Enablers for the Twenty-twenty Information Society\nomeqref {2.18}
		\nompageref{34}
  \item [{MMSE}]\begingroup Minimum Mean Square Error\nomeqref {2.13}
		\nompageref{24}
  \item [{MNO}]\begingroup Mobile Network Operator\nomeqref {1.0}
		\nompageref{6}
  \item [{MTC}]\begingroup Machine Type Communications\nomeqref {1.0}
		\nompageref{6}
  \item [{NB-IoT}]\begingroup Narrow Band-IoT\nomeqref {1.0}
		\nompageref{6}
  \item [{NMSE}]\begingroup Normalized Mean Square Error\nomeqref {2.0}
		\nompageref{17}
  \item [{OFDM}]\begingroup Orthogonal Frequency Division Multiplexing\nomeqref {1.0}
		\nompageref{8}
  \item [{OFDM/OQAM}]\begingroup Offset QAM OFDM\nomeqref {2.18}
		\nompageref{36}
  \item [{OFDMA}]\begingroup Orthogonal Frequency Division Multiple Access\nomeqref {2.13}
		\nompageref{24}
  \item [{OOB}]\begingroup Out Of Band\nomeqref {2.18}\nompageref{30}
  \item [{OSA}]\begingroup Opportunistic Spectrum Access\nomeqref {2.18}
		\nompageref{27}
  \item [{PAM}]\begingroup Pulse Amplitude Modulation\nomeqref {2.18}
		\nompageref{36}
  \item [{PAPR}]\begingroup Peak to Average Power Ratio\nomeqref {2.18}
		\nompageref{26}
  \item [{PHY}]\begingroup PHYsical\nomeqref {1.0}\nompageref{8}
  \item [{PHYDYAS}]\begingroup PHYsical layer for DYnAmic Spectrum access and cognitive radio\nomeqref {2.18}
		\nompageref{34}
  \item [{PLC}]\begingroup Power Line Communication\nomeqref {7.0}
		\nompageref{151}
  \item [{PPM}]\begingroup Parts Per Million\nomeqref {6.18}
		\nompageref{130}
  \item [{PPP}]\begingroup Poisson point process\nomeqref {6.10}
		\nompageref{120}
  \item [{PROFIL}]\begingroup Evolution de la PROfessional Mobile Radio large bande basée sur la modulation FILter Bank MultiCarrier\nomeqref {2.18}
		\nompageref{34}
  \item [{PSD}]\begingroup Power Spectral Density\nomeqref {2.13}
		\nompageref{24}
  \item [{PUSCH}]\begingroup Physical Uplink Shared Channel\nomeqref {6.12}
		\nompageref{122}
  \item [{QAM}]\begingroup Quadrature Amplitude Modulation\nomeqref {2.18}
		\nompageref{36}
  \item [{QoSMOS}]\begingroup Quality of Service and MObility driven cognitive radio Systems\nomeqref {2.18}
		\nompageref{30}
  \item [{RA}]\begingroup Resource Allocation\nomeqref {6.6}
		\nompageref{111}
  \item [{RB}]\begingroup Resource Block\nomeqref {6.9}\nompageref{112}
  \item [{SC-FDMA}]\begingroup Single Carrier FDMA\nomeqref {2.13}
		\nompageref{24}
  \item [{SCEE}]\begingroup Signal, Communications and Embedded Electronics\nomeqref {1.0}
		\nompageref{12}
  \item [{SINR}]\begingroup Signal to Interference plus Noise Ratio\nomeqref {2.13}
		\nompageref{24}
  \item [{SMS}]\begingroup Short Message Service\nomeqref {1.0}
		\nompageref{4}
  \item [{SNR}]\begingroup Signal to Noise Ratio\nomeqref {2.13}
		\nompageref{24}
  \item [{SU}]\begingroup Secondary User\nomeqref {2.18}\nompageref{27}
  \item [{TDMA}]\begingroup Time Division Multiple Access\nomeqref {2.13}
		\nompageref{24}
  \item [{TO}]\begingroup Timing Offset\nomeqref {1.0}\nompageref{6}
  \item [{TSE}]\begingroup Time-Spectral Efficiency\nomeqref {3.12}
		\nompageref{63}
  \item [{TVWS}]\begingroup TV White Space\nomeqref {2.0}
		\nompageref{15}
  \item [{UE}]\begingroup User Equipment\nomeqref {6.10}
		\nompageref{120}
  \item [{UF-OFDM}]\begingroup Universal Filtered OFDM\nomeqref {2.19}
		\nompageref{38}
  \item [{UFMC}]\begingroup Universal Filtered Multi-Carrier\nomeqref {2.19}
		\nompageref{38}
  \item [{VR}]\begingroup Virtual Reality\nomeqref {1.0}\nompageref{3}
  \item [{Wi-Fi}]\begingroup Wireless Fidelity\nomeqref {1.0}
		\nompageref{7}
  \item [{WONG5}]\begingroup Waveforms MOdels for Machine Type CommuNication inteGrating 5G Networks\nomeqref {2.18}
		\nompageref{34}
  \item [{ZF}]\begingroup Zero Forcing\nomeqref {2.13}\nompageref{24}

 \nomgroup{H}

  \item [{(x.x)}]\begingroup Reference to equation (x.x)\nomeqref {1.0}
		\nompageref{3}
  \item [{[xx]}]\begingroup Citation referring to [xx] in bibliography\nomeqref {1.0}
		\nompageref{3}
  \item [{\textbf{bold}}]\begingroup Emphasis\nomeqref {1.0}
		\nompageref{3}
  \item [{\textit{italic}}]\begingroup Term referring to a specific notion in the literature\nomeqref {1.0}
		\nompageref{3}

 \nomgroup{O}

  \item [{$\cdot *\cdot$}]\begingroup Convolution product\nomeqref {2.13}
		\nompageref{24}
  \item [{$\hat{\cdot}$}]\begingroup Estimate of $\cdot$\nomeqref {2.0}
		\nompageref{16}
  \item [{$\Im\{\cdot\}$}]\begingroup Imaginary part of $\cdot$\nomeqref {2.6}
		\nompageref{20}
  \item [{$\lfloor x \rfloor$}]\begingroup Integer part of $x$\nomeqref {2.0}
		\nompageref{16}
  \item [{$\lfloor x \rfloor^+$}]\begingroup $\max(0,\lfloor x \rfloor)$\nomeqref {2.0}
		\nompageref{16}
  \item [{$\lvert\cdot\rvert$}]\begingroup Absolute value of $\cdot$\nomeqref {2.0}
		\nompageref{17}
  \item [{$\mathbb{N}$}]\begingroup Set of natural integers\nomeqref {2.18}
		\nompageref{35}
  \item [{$\mathbb{R}$}]\begingroup Set of real numbers\nomeqref {2.18}
		\nompageref{35}
  \item [{$\mathbb{Z}$}]\begingroup Set of all integers\nomeqref {2.18}
		\nompageref{35}
  \item [{$\mathbf{X}$}]\begingroup Matrix\nomeqref {2.13}
		\nompageref{24}
  \item [{$\mathbf{x}$}]\begingroup Vector\nomeqref {2.0}
		\nompageref{16}
  \item [{$\mathbf{X}^H$}]\begingroup Hermitian Matrix of $\mathbf{X}$\nomeqref {2.13}
		\nompageref{24}
  \item [{$\mathcal{F}\{\cdot\}$}]\begingroup Fourier Transform of $\cdot$\nomeqref {2.6}
		\nompageref{20}
  \item [{$\mathcal{X}$}]\begingroup Set\nomeqref {2.6}\nompageref{20}
  \item [{$\Re\{\cdot\}$}]\begingroup Real part of $\cdot$\nomeqref {2.6}
		\nompageref{20}
  \item [{$x$}]\begingroup Scalar\nomeqref {2.0}\nompageref{16}

 \nomgroup{U}

  \item [{dB}]\begingroup decibel\nomeqref {2.0}\nompageref{17}
  \item [{Hz}]\begingroup hertz\nomeqref {2.0}\nompageref{17}
  \item [{s}]\begingroup second\nomeqref {2.0}\nompageref{17}

\end{thenomenclature}
