\thispagestyle {empty}
\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\contentsline {chapter}{Abstract}{vii}{chapter*.2}
\contentsline {chapter}{List of Figures}{ix}{chapter*.3}
\contentsline {chapter}{List of Tables}{xiii}{chapter*.4}
\contentsline {chapter}{Nomenclature}{xv}{chapter*.5}
\contentsline {chapter}{R\IeC {\'e}sum\IeC {\'e} \IeC {\'e}tendu en fran\IeC {\c c}ais}{1}{chapter*.7}
\contentsline {chapter}{\numberline {1}General Introduction}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Background and Motivations}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}From 4G to 5G: a road towards heterogeneity}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Coexistence in heterogeneous networks}{6}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Thesis scope}{8}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Objectives and Contributions}{10}{section.1.2}
\contentsline {section}{\numberline {1.3}Thesis outline}{10}{section.1.3}
\contentsline {chapter}{\numberline {2}Background and problem statement}{15}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction}{15}{section.2.1}
\contentsline {section}{\numberline {2.2}Review on current wireless networks}{16}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Digital communications background}{16}{subsection.2.2.1}
\contentsline {subsubsection}{Digital communication chain}{16}{section*.17}
\contentsline {subsubsection}{Wireless channel modeling}{18}{section*.19}
\contentsline {subsection}{\numberline {2.2.2}Multicarrier systems}{22}{subsection.2.2.2}
\contentsline {subsubsection}{Generic mulitcarrier structure}{22}{section*.23}
\contentsline {subsubsection}{OFDM}{24}{section*.26}
\contentsline {subsubsection}{OFDM-based multiuser schemes}{26}{section*.28}
\contentsline {section}{\numberline {2.3}Arising coexistence issues}{26}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Coexistence and cognitive radio}{26}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Foreseen coexistence scenarios}{28}{subsection.2.3.2}
\contentsline {subsubsection}{TV White Spaces: a paramount example of CR application}{28}{section*.30}
\contentsline {subsubsection}{D2D communication in LTE bands}{30}{section*.33}
\contentsline {section}{\numberline {2.4}Enhanced waveforms for 5G}{33}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Designing new waveforms: objectives and constraints}{33}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Linear FB-MC}{35}{subsection.2.4.2}
\contentsline {subsubsection}{Filtered Multi-Tone (FMT)}{35}{section*.36}
\contentsline {subsubsection}{Offset QAM OFDM (OFDM/OQAM)}{35}{section*.37}
\contentsline {subsubsection}{FBMC-PAM}{36}{section*.38}
\contentsline {subsection}{\numberline {2.4.3}Circular convolution based filter banks}{36}{subsection.2.4.3}
\contentsline {subsubsection}{GFDM}{36}{section*.40}
\contentsline {subsubsection}{COQAM}{37}{section*.41}
\contentsline {subsection}{\numberline {2.4.4}Band-filtered waveforms}{38}{subsection.2.4.4}
\contentsline {subsubsection}{UFMC}{38}{section*.42}
\contentsline {subsubsection}{f-OFDM}{38}{section*.43}
\contentsline {subsection}{\numberline {2.4.5}Comparison criteria}{39}{subsection.2.4.5}
\contentsline {subsubsection}{Selected waveform parameters}{39}{section*.44}
\contentsline {subsubsection}{Spectral localization}{41}{section*.46}
\contentsline {subsubsection}{Robustness to asynchronism in coexistence scenarios}{42}{section*.48}
\contentsline {subsubsection}{Time-spectral efficiency}{44}{section*.50}
\contentsline {subsubsection}{Raw BER performance}{45}{section*.52}
\contentsline {subsubsection}{Other criteria}{46}{section*.54}
\contentsline {subsubsection}{Summary}{48}{section*.55}
\contentsline {section}{\numberline {2.5}Open research problems}{48}{section.2.5}
\contentsline {chapter}{\numberline {3}Related work and corresponding contributions}{51}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{51}{section.3.1}
\contentsline {section}{\numberline {3.2}Interference modeling in coexistence scenarios}{52}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Spectral coexistence system model}{52}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}PSD-based modeling}{54}{subsection.3.2.2}
\contentsline {subsubsection}{Model definition}{54}{section*.58}
\contentsline {subsubsection}{PSD-based model applied to FB-MC waveforms}{55}{section*.60}
\contentsline {subsubsection}{Limitations of the PSD-based model}{57}{section*.63}
\contentsline {subsection}{\numberline {3.2.3}Asynchronous interference modeling}{57}{subsection.3.2.3}
\contentsline {subsubsection}{Model definition}{57}{section*.64}
\contentsline {subsubsection}{Asynchronous interference modeling applied to FB-MC waveforms}{58}{section*.65}
\contentsline {subsubsection}{Limitations of the asynchronous interference model}{59}{section*.66}
\contentsline {subsection}{\numberline {3.2.4}Summary}{59}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Experimental studies}{60}{section.3.3}
\contentsline {section}{\numberline {3.4}Enhanced waveforms for coexistence with incumbent CP-OFDM systems}{61}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}CR context}{61}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}D2D context}{62}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Summary}{62}{subsection.3.4.3}
\contentsline {section}{\numberline {3.5}Conclusion}{63}{section.3.5}
\contentsline {chapter}{\numberline {4}Analysis of interference between CP-OFDM and FB-MC waveforms}{65}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction}{66}{section.4.1}
\contentsline {section}{\numberline {4.2}System model}{66}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Coexistence setup}{66}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Generic multicarrier waveform mathematical model}{68}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}EVM-based measurement of interference}{70}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Why we need a new model}{70}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Principle of the EVM-based measure of interference}{71}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Simulation setup and obtained results}{73}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}Analytical aspects}{75}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Statistics of interference signal}{75}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Closed-form expression of interference power}{78}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Extension to multipath channels}{82}{subsection.4.4.3}
\contentsline {subsubsection}{Zero Forcing (ZF) equalization}{82}{section*.76}
\contentsline {subsubsection}{Minimum Mean Square Error (MMSE) equalization}{84}{section*.77}
\contentsline {subsection}{\numberline {4.4.4}Case study}{84}{subsection.4.4.4}
\contentsline {section}{\numberline {4.5}Experimental Validation}{86}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Experimental setup}{86}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Developed GUI, obtained results and discussion}{87}{subsection.4.5.2}
\contentsline {section}{\numberline {4.6}Extension of the EVM-approach to subband-filtered waveforms}{90}{section.4.6}
\contentsline {section}{\numberline {4.7}Conclusion}{91}{section.4.7}
\contentsline {chapter}{\numberline {5}Coexistence with CP-OFDM incumbent systems in CR setups}{93}{chapter.5}
\contentsline {section}{\numberline {5.1}Introduction}{93}{section.5.1}
\contentsline {section}{\numberline {5.2}Guard band dimensioning}{94}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}System setup}{94}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Obtained results}{95}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}Optimal power allocation}{97}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}System setup}{97}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Obtained results}{98}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}Optimal transmission in a given time frame}{100}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}System setup}{100}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Obtained results}{101}{subsection.5.4.2}
\contentsline {section}{\numberline {5.5}Conclusion}{104}{section.5.5}
\contentsline {chapter}{\numberline {6}Asynchronous D2D operation in 5G networks}{105}{chapter.6}
\contentsline {section}{\numberline {6.1}Introduction}{105}{section.6.1}
\contentsline {section}{\numberline {6.2}Preliminary mono-cellular analysis}{106}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}System Model}{106}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Interference Model and Problem Formulation}{108}{subsection.6.2.2}
\contentsline {subsubsection}{Interference Model}{108}{section*.92}
\contentsline {subsubsection}{Optimization Problem Formation}{109}{section*.94}
\contentsline {paragraph}{RB Assignment:}{112}{section*.97}
\contentsline {paragraph}{Power-loading:}{112}{section*.100}
\contentsline {subsection}{\numberline {6.2.3}Obtained Results}{113}{subsection.6.2.3}
\contentsline {paragraph}{Effects of inter-D2D interference for both OFDM/OQAM and CP-OFDM:}{114}{section*.104}
\contentsline {paragraph}{OFDM/OQAM and CP-OFDM performance in varying scenario set-ups:}{117}{section*.106}
\contentsline {subsection}{\numberline {6.2.4}Summary of the study}{119}{subsection.6.2.4}
\contentsline {section}{\numberline {6.3}Network level study}{120}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}System Model}{120}{subsection.6.3.1}
\contentsline {subsubsection}{Network Set-up}{120}{section*.110}
\contentsline {subsubsection}{Resource Allocation}{122}{section*.112}
\contentsline {subsubsection}{Channel Modelling}{123}{section*.113}
\contentsline {subsubsection}{Performance Measures}{123}{section*.114}
\contentsline {paragraph}{SINR:}{123}{section*.115}
\contentsline {paragraph}{Achieved Rate:}{124}{section*.116}
\contentsline {subsubsection}{Interference Model}{125}{section*.119}
\contentsline {subsection}{\numberline {6.3.2}Evaluation of System Performance}{128}{subsection.6.3.2}
\contentsline {subsubsection}{Scenario Under Investigation}{128}{section*.120}
\contentsline {subsubsection}{System Performance}{130}{section*.123}
\contentsline {paragraph}{DUE SINR Performance:}{130}{figure.caption.125}
\contentsline {paragraph}{DUE Rate Performance:}{131}{figure.caption.127}
\contentsline {paragraph}{CUE Performance:}{132}{figure.caption.129}
\contentsline {subsubsection}{DUE Transmit Power}{135}{section*.130}
\contentsline {subsubsection}{Cell Radius}{136}{section*.132}
\contentsline {subsubsection}{Cluster Radius}{138}{section*.134}
\contentsline {subsubsection}{Amount of Time and Frequency Misalignment between Devices}{139}{section*.136}
\contentsline {paragraph}{Maximum Possible Timing Offset:}{139}{section*.137}
\contentsline {paragraph}{Maximum Possible CFO:}{141}{section*.139}
\contentsline {subsection}{\numberline {6.3.3}Concluding Remarks}{143}{subsection.6.3.3}
\contentsline {section}{\numberline {6.4}Conclusion}{144}{section.6.4}
\contentsline {chapter}{\numberline {7}Conclusions and perspectives}{145}{chapter.7}
\contentsline {section}{\numberline {7.1}General conclusion}{145}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Context of the study}{145}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Main results and findings of the thesis}{146}{subsection.7.1.2}
\contentsline {subsubsection}{Analysis of interference between CP-OFDM and FB-MC waveforms}{146}{section*.141}
\contentsline {subsubsection}{Coexistence with CP-OFDM incumbent systems in CR setups}{146}{section*.142}
\contentsline {subsubsection}{Asynchronous D2D operation in 5G networks}{147}{section*.143}
\contentsline {section}{\numberline {7.2}Perspectives for future work}{147}{section.7.2}
\contentsline {paragraph}{Accurate study of the interference distribution:}{147}{section*.144}
\contentsline {paragraph}{Design of precoders for incumbent protection:}{148}{section*.145}
\contentsline {paragraph}{Analysis of channel coding effects:}{148}{section*.146}
\contentsline {paragraph}{Taking into consideration the synchronization procedures of the incumbent:}{148}{section*.147}
\contentsline {paragraph}{Taking into account hardware impairments:}{148}{section*.148}
\contentsline {paragraph}{Taking into account potential bandpass filtering at the incumbent:}{148}{section*.149}
\contentsline {paragraph}{Extending our analysis to coexisting systems with different subcarrier widths:}{148}{section*.150}
\contentsline {paragraph}{Extending the FB-MC formalism to subband-filtered waveform:}{149}{section*.151}
\contentsline {paragraph}{Correction of the Gaussian approximation in network simulations:}{149}{section*.152}
\contentsline {paragraph}{Study of metrics other than SINR in network simulations:}{149}{section*.153}
\contentsline {paragraph}{Evaluating the impact of Full Duplex technology on the led studies:}{149}{section*.154}
\contentsline {paragraph}{Comparing the gains brought by new waveforms with those achieved by multi-antenna techniques:}{150}{section*.155}
\contentsline {paragraph}{Adopting a simulation-driven approach to wireless networks management: }{150}{section*.156}
\contentsline {section}{\numberline {7.3}Final words}{150}{section.7.3}
\contentsline {chapter}{\numberline {A}Derivation of $A_{\Pi _T,g}(\tau ,\nu )$}{153}{Appendix.a.A}
\contentsline {subsubsection}{$T_g\geq T$ case}{154}{section*.157}
\contentsline {chapter}{\numberline {B}SCEE Testbed}{157}{Appendix.a.B}
\contentsline {section}{\numberline {B.1}Testbed presentation and layout}{157}{section.a.B.1}
\contentsline {section}{\numberline {B.2}Available hardware material}{158}{section.a.B.2}
\contentsline {section}{\numberline {B.3}Testbed organization and network architecture}{159}{section.a.B.3}
\contentsline {section}{\numberline {B.4}Usage monitoring}{161}{section.a.B.4}
\contentsline {chapter}{\numberline {C}Network-level simulator}{165}{Appendix.a.C}
\contentsline {section}{\numberline {C.1}Simulator description}{165}{section.a.C.1}
\contentsline {section}{\numberline {C.2}Simulation parameters}{167}{section.a.C.2}
\contentsline {section}{\numberline {C.3}Simulation Workflow}{171}{section.a.C.3}
\contentsline {chapter}{\numberline {D}Publications and Involvement in R\&D Projects}{173}{Appendix.a.D}
\contentsline {chapter}{Bibliography}{177}{chapter*.180}
