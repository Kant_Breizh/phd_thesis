The modulation format and multiple access technique for 5G is not yet known, with many contenders under consideration, each proving advantageous in certain scenarios and lacking in others. 5G could potentially be the first generation to permit the use of different waveforms for different use cases, each one optimal for a given scenario \cite{Sexton20175G}. In this paper, we explore this possibility and focus on two different, yet coexisting, 5G use cases: high-rate clustered device-to-device (D2D) communication and ordinary cellular traffic.

Intelligent process control, autonomous manufacturing systems, smart factories, and self-organising warehouses are but a few examples of 5G use cases which require high-rate inter-machine communication in a spatially clustered environment.
The overhead associated with achieving and maintaining synchronous communication for D2D user equipment (DUE) in such a scenario may be significant, and it may be desirable to reduce the control burden placed on the base station to achieve this. As highlighted in \cite{Lin2014Overview}, achieving synchronisation in D2D communications is challenging, as D2D devices may span multiple cells and varying distances to the controlling base station (BS) may result in different timing advances. This is particularly relevant for clustered D2D scenarios, in which the close proximity of the multiple D2D pairs to each other makes them particularly vulnerable to leakage interference arising from synchronisation errors.

%Removing the requirement for the BS to synchronise DUEs would permit the network operator to treat resource allocation for high-rate clustered D2D scenarios in a different manner than for CUEs. For example, the network could release spectral resources to a smart factory without actively managing the directly communicating machinery, which may operate autonomously or via a local controller. Secondly, negating the necessity to undergo a bulky synchronisation procedure also reduces the latency experienced by the DUEs.

Hence, while ordinary cellular user equipment (CUE) is well served using synchronous communication, clustered DUEs may instead be best served using asynchronous communication. It is for this reason that we consider clustered D2D communication to be sufficiently different from ordinary cellular traffic to warrant investigation into what waveform choice results in the best performance. Orthogonal frequency division multiplexing (OFDM), employed in LTE, performs quite well when synchronism can be achieved, and may continue to be the best choice for cellular communication. However, OFDM's deficiencies in the presence of timing and frequency offsets are well known, with several 5G waveform candidates shown to perform better in asynchronous scenarios \cite{Aminjavaheri2015Impact}. Hence, we are motivated to examine the coexistence of various combinations of waveforms for CUEs and DUEs. We study and quantify how each of the waveforms under consideration performs when employed by DUEs operating in an asynchronous manner, compared to a baseline case consisting of synchronous OFDM. In the remainder of this paper, we use the term \textit{alternative waveforms} to refer to the multitude of modulation formats that have been proposed in the literature for 5G as an alternative to OFDM.

Supporting a different waveform for cellular D2D communication would not be practical, as it would require CUEs to support two different waveforms. However, the types of devices that we are considering, such as machinery in a smart factory, are designed for specific purposes and may not need to support traditional cellular communication through a BS. In this case, it makes sense for them to use a waveform that is suited for their particular needs. 

In this paper, we expand upon our previous work \cite{Sexton2016Coexistence}, which demonstrated the effects of inter-D2D interference arising from misaligned communications in a spatially clustered single-cell scenario, and how the use of a waveform that exhibits improved spectral localization over OFDM can mitigate this interference. We expand our work to a multi-cell, multi-cluster network, and consider a wider range of waveforms. We also consider a network consisting of cells which employ fractional frequency reuse (FFR).

%We build upon our work in \cite{Bodinier20165G}, which provides interference tables capturing the effects of misaligned D2D users in time onto OFDMA-based cellular users in the uplink band. We also draw upon the work of \cite{Medjahdi2010a} and \cite{Bodinier2016ICT} in order to characterize the interference imposed between entities utilizing different waveforms. 
We evaluate the relative asynchronous performance of several waveforms for use in the type of clustered D2D scenario outlined in this section, compared against a baseline case consisting of synchronous OFDM. We also investigate how the level of asynchronism between devices affects the SINR performance of DUEs, examining the effects of both timing offset (TO) and carrier frequency offset (CFO). We stress that our interest lies in investigating the coexistence and relative performance of different waveforms in such a scenario; we are not concerned with developing a new resource allocation scheme for underlay D2D.

The main contributions of this paper are the following:
\begin{itemize}
\item We compute, and tabulate, the interference arising from the asynchronous coexistence between a large number of alternative waveforms for various timing and frequency offsets.
\item We demonstrate that it is feasible for cellular networks to serve high-rate clustered D2D use cases through the coexistence of alternative waveforms and OFDM, and quantify the benefit of doing so.
\item We characterize the performance of several prominent alternative waveforms across a range of scenarios by varying key system parameters such as cell size, cluster size, DUE transmit power, and maximum possible timing offset and CFO.
\end{itemize}

The rest of the paper is structured as follows. Section \ref{section:related_work} discusses related work, and highlights the novelty of this paper. Section \ref{section:system_model} describes the system model and metrics of interest. Section \ref{section:interference_model} describes the waveforms considered in this paper, and details how leakage interference between pairs of waveforms is modelled.  %Section \ref{section:investigated_scenario} outlines the scenario that was investigated, and the simulation set-up. 
Section \ref{section:sim_results} presents and discusses our results, and Section \ref{section:conclusion} concludes the paper.































% Our use case
%However, there exists a set of IoT use cases that will present requirements for low-latency and potentially high data-rate communication, resulting from the increased use of robotics, artificial intelligence, and machine learning across multiple sectors such as health, industry, and automotive. 

%Intelligent process control, autonomous manufacturing systems, smart factories, and self-organising warehouses are but a few examples of 5G use cases which each require high-rate, latency-intolerant inter-machine communication in a spatially clustered environment. It is these requirements that we focus on in this paper, investigating how 5G cellular networks can meet the demands of high-rate clustered devices without compromising the performance of traditional cellular user equipment (CUE).
%
%In particular, we focus on two enabling technologies: device-to-device (D2D) communication and enhanced waveforms.
%D2D communication is well suited to the type of latency-intolerant, high-rate traffic being considered, allowing devices to reduce their latency and achieve high-rates with lower transmit powers by communicating directly and avoiding a round trip through the base station (BS). Enhanced waveforms\footnote{We use the term \textit{enhanced waveforms} to refer to the multitude of modulation formats that have been proposed in the literature for 5G as an alternative to OFDM} with improved spectral localisation over orthogonal frequency division multiplexing (OFDM) can enable asynchronous communication between devices, and hence reduce the control overhead associated with achieving synchronism. In contrast, OFDM's strict synchronicity requirements make it unsuited to asynchronous communication, with its large side-lobes resulting in significant leakage.
%
%In the context of the type of 5G scenarios under consideration, two additional benefits are obtained by using an enhanced waveform to enable asynchronous communication between clustered D2D user equipment (DUE). First, we wish to enable 5G network operators to serve spatially clustered D2D use cases with minimal additional control overhead.  
%Removing the requirement for the BS to synchronise DUEs would permit the network operator to treat resource allocation for high-rate clustered D2D scenarios in a different manner than for CUEs. For example, the network could release spectral resources to a smart factory without actively managing the directly communicating machinery, which may operate autonomously or via a local controller. 
%Secondly, negating the necessity to undergo a bulky synchronisation procedure also reduces the latency experienced by the DUEs.
%
%In particular, we are interested in the coexistence of waveforms in 5G networks. The modulation format and multiple access technique for 5G is not yet known, with many contenders, each proving advantageous in certain scenarios and lacking in others. 5G could potentially be the first generation to permit the use of different waveforms for different use cases, each one optimal for a given scenario. Supporting a different waveform for cellular D2D communication would not be practical, as it would require CUEs to support two different waveforms. However, the types of devices that we are considering, such as machinery in a smart factory, are designed for specific purposes and may not need to support traditional cellular communication through a BS. In this case, it makes sense for them to use a waveform that is suited for their particular needs. Hence, inter-communicating machinery or devices could use an enhanced waveform with better spectral localization, allowing relaxed synchronization and providing the aforementioned advantages. 
%CUEs could continue to use OFDM (or whichever waveform is chosen for 5G) for all communication. 
%
%In this paper, we expand upon our previous work \cite{Sexton2016Coexistence}, which demonstrated the effects of inter-D2D interference arising from misaligned communications in a spatially clustered single-cell scenario, and how the use of a waveform that exhibits improved spectral localization over OFDM can mitigate this interference. In this paper, we investigate whether through the use of enhanced waveforms cellular networks can provision high-rate, latency-intolerant clustered D2D applications using minimal additional control. We also consider a network consisting of cells which employ strict fractional frequency reuse (FFR).
%
%We build upon our work in \cite{Bodinier20165G}, which provides interference tables capturing the effects of misaligned D2D users in time onto OFDMA-based cellular users in the uplink band. We also draw upon the work of \cite{Medjahdi2010a} and \cite{Bodinier2016ICT} in order to characterize the interference imposed between entities utilizing different waveforms. We perform system-level simulations in order to evaluate the relative performance of several waveforms for use in the type of clustered D2D scenarios outlined in this section. We also investigate how the level of asynchronism between devices affects the SINR performance of DUEs, examining the effects of both timing offset (TO) and carrier frequency offset (CFO). We stress that our interest lies in investigating the relative performance of different waveforms in such a scenario; we are not concerned with developing a new resource allocation scheme for underlay D2D, of which there are many in the literature.
%
%The main contributions of this paper are the following:
%\begin{itemize}
%\item We rate, with a high level of accuracy, the interference arising from the asynchronous coexistence between a large number of enhanced waveforms. We then use the tabulated interference values in system-level simulations.
%\item We demonstrate that it is feasible for cellular networks to serve high-rate clustered D2D use cases through a coexistence of enhanced waveforms and OFDM, and quantise the benefit of doing so.
%\item We characterize the performance of several prominent enhanced waveforms across a range of scenarios by varying key system parameters such as cell size, cluster size, DUE transmit power, and maximum possible timing offset and CFO.
%\end{itemize}
%
%The rest of the paper is structured as follows. Section \ref{section:related_work} discusses related work, and highlights the novelty of this paper. Section \ref{section:system_model} describes the system model and metrics of interest. Section \ref{section:interference_model} describes the waveforms considered in this paper, and details how leakage interference between couples of waveforms is modelled.  %Section \ref{section:investigated_scenario} outlines the scenario that was investigated, and the simulation set-up. 
%Section \ref{section:sim_results} presents and discusses our results, and Section \ref{section:conclusion} concludes the paper.


% Introduce D2D
%The use of direct communication based on D2D technology permits communicating entities to set up a direct link and avoid the overhead of making a round-trip through the base station (BS).

%D2D is currently standardized for overlay in LTE-A; however, in this paper we examine an underlay scenario whereby directly communicating smart user equipment (DUEs) in a spatially clustered IoT scenario may reuse the same spectral resources as cellular user equipment (CUEs).


% Why asynchronous
%When utilizing orthogonal frequency division multiplexing (OFDM) for direct communications, each directly communicating pair must be synchronized with the incumbent CUEs in order to avoid interference leakage between the two. However, the signalling overhead and complexity associated with achieving perfect synchronization by the BS may be significant, becoming infeasible as larger numbers of directly communicating DUEs are considered. Hence, in this paper, we consider scenarios consisting of fully asynchronous direct communication, as well as scenarios where only coarse synchronisation has been achieved, i.e., we do not assume that DUEs are perfectly synchronized with either CUEs or one another.

% Additional Benefits
%By combining the two technologies and using an enhanced waveform to enable asynchronous D2D communication between DUEs, two additional benefits are obtained. First, we wish to enable 5G network operators to serve spatially clustered IoT use cases with minimal additional control overhead. Removing the requirement for the BS to synchronise DUEs would permit the network operator to treat resource allocation for high-rate clustered IoT scenarios in a different manner than for CUEs. For example, the network could release spectral resources to a smart factory for use with certain conditions without actively managing the smart equipment, which may operate autonomously or via a local controller. Secondly, negating the necessity to undergo a bulky synchronisation procedure also reduces the latency experienced by the DUEs.

% New Waveforms
%OFDM's strict synchronicity requirements make it unsuited to asynchronous communication, with its large side-lobes resulting in significant leakage. Motivated by this, we investigate the use of enhanced waveforms with improved spectral localization that may be more appropriate for asynchronous communication, and hence may enable asynchronous direct communication to be considered as a potential solution for serving clustered IoT scenarios.

% FFR
%We also consider the use of strict fractional frequency reuse (FFR) in this work, a frequently employed inter-cell interference coordination (ICIC) scheme in LTE networks. Strict FFR is a modification of traditional frequency reuse schemes, offering improvements in spectral efficiency and cell-edge user performance. Each cell is segregated into an inner and outer region, with the sub-bands assigned to each region coordinated in a static manner between cells to reduce interference. 

% 5G is diverse. Coexistence of IoT and traditional cellular use. Impossible to design a network using one-size-fits-all technologies. None more obvious example than many potential waveform candidates, each suited to different use cases. 5G may see the coexistence of multiple waveforms.
%Unlike previous generations, which largely targeted greater data-rates, 5G must deal with a diverse range of use cases \cite{Osseiran2014Scenarios}. This is exemplified by the coexistence of IoT and traditional cellular use; two vastly different services to be supported by the same network. Previous generations were primarily defined by their air interface. The diversity of the use cases for 5G necessitates a more adaptable solution, with a one-size-fits-all approach to the air interface unlikely to succeed. The modulation format and multiple access technique for 5G is not yet known, with many contenders each proving advantageous in certain scenarios and lacking in others. 5G could potentially be the first generation to permit the use of different waveforms for different use cases, each one optimal for a given scenario.