%!TEX root = ../../thesis.tex
\subsubsection{Scenario Under Investigation} \label{subsection:scenario}
% Simulation set-up, params and general description
 



\begin{figure}[t]
  \centering
    \includegraphics[width=0.8\linewidth,clip,keepaspectratio]{example_scenario.pdf}
    \caption[Example scenario consisting of 19 cells]{Example scenario consisting of 19 cells, with each region coloured according to the spectral resources permitted for use. Each cross represents an ordinary cellular user, whereas DUEs involved in direct communication are clustered in groups.}
    \label{fig:example_scenario}
\end{figure}

We first present detailed results for the scenario defined by the parameters listed in Table \ref{tab:sim_params}. We then examine how the performance of the system changes depending on the scenario by varying a key parameter of interest while maintaining the other parameters as per their value in Table \ref{tab:sim_params}. For each set of results, we compare the asynchronous performance of all waveforms under consideration, and use the performance of synchronous CP-OFDM as a baseline for comparison. 


\bgroup
\def\arraystretch{1.03}
\begin{table}[!t]
\caption{Simulation parameters}
\begin{center}
\scriptsize
\begin{tabular}{|c|c||c|c|}
\hline
\hline
\textbf{Parameter} & \textbf{Value} & \textbf{Parameter} & \textbf{Value} \\ \hline \hline
Number RBs in system & 50 & $P_{\textrm{O\_PUSCH}}$ & -96 dBm \\ \hline
Cell Radius & 250 m &  $\alpha$ & 1 \\\hline
Inner Radius & 163 m & Max Tx Power CUE & 24 dBm \\ \hline
Number of Cells & 19 & Max Tx Power DUE & -5 dBm \\ \hline
CUEs Per Square Km & 200 & BS Antenna Gain & 15 dBi \\ \hline
DUEs Per Cluster & 30 & UE Antenna Gain & 0 dBi\\ \hline
Clusters Per Square Km & 3 & BS Noise Figure & 5 dB \\ \hline
Average Cluster Radius & 60 m & UE Noise Figure & 9 dB \\ \hline
Average Tx-Rx Distance & \makecell{Uniformly distributed \\in the range [10, 50] m} & Max Timing Offset & $T + T_{CP}$ \\ \hline
Carrier Frequency & 2 GHz & \makecell{Max Local Oscillator (LO) \\Inaccuracy} & 2.5 ppm  \\ \hline
Subcarrier Spacing ($\Delta \mathrm{F}$) & 15 kHz & Waveforms & \makecell{CP-OFDM, FMT,\\ OFDM/OQAM, FBMC-PAM,\\ GFDM, f-OFDM, UFMC} \\ \hline
Noise Per RB \tablefootnote{Noise power per RB is calculated using the expression ${-174}\textrm{dBm/Hz}$$ + 10\log_{10}(180\textrm{kHz})$, where ${-174}\textrm{dBm/Hz}$ is the thermal noise and $180\textrm{kHz}$ is the bandwidth of an LTE RB.} ($\sigma_\nu^2$) &  -116 dBm & Number of Iterations & 5000  \\ \hline\hline
\end{tabular}
\end{center}
\label{tab:sim_params2}
\end{table}
\egroup

For the synchronous CP-OFDM baseline case, we assume quasi-orthogonality in which all timing offsets are absorbed by the CP. We also do not consider CFO in this case for two reasons. First, the scenario that we are considering typically consists of low mobility, resulting in negligible Doppler shifts and frequency offsets. Secondly, the 3GPP standards specify stringent frequency errors for UEs of less than +/- 0.1 parts per million (ppm)\cite{3GPPUERef} compared to the carrier frequency received from the BS. In contrast, for asynchronous scenarios, we consider timing offsets uniformly distributed in the range of 0 to $T + T_{CP}$, where $T$ is the length of an OFDM symbol and $T_{CP}$ is the length of the CP. We also consider less stringent hardware-related frequency error requirements, with local oscillator (LO) inaccuracies of +/- 2.5 ppm\footnote{Generally, strict frequency error requirements require more accurate and expensive clocks. 2.5ppm is the stated frequency accuracy of the NI USRP-292x range of devices.} permitted. 

The cell radius value of 250m is based on the 3GPP LTE system scenarios \cite{3GPPScenarios}, representing an urban macro-cell environment. The antenna gain values, noise figures, and the carrier frequency value are also based on \cite{3GPPScenarios}. The values for the maximum CUE transmit power, subcarrier spacing, and number of resource blocks are based on the LTE standard, with 50 resource blocks corresponding to a bandwidth of 10MHz. The maximum DUE transmit power of -5 dBm was chosen as we found through experimentation that it yielded good results. The effects of varying the maximum DUE transmit power will be discussed later in this section.

Fig. \ref{fig:example_scenario} shows an example of our simulation scenario. We explore the case whereby each macro-cell is fully loaded, with all available resource blocks being utilised, and hence consider a large number of CUEs per square kilometre to ensure this. The parameters relating to the size and frequency of occurrence of clusters are scenario dependant. A cluster of radius 60m, containing 30 inter-communicating devices and with an average of 3 clusters per square kilometre might, for example, represent a factory in an urban area with moderate industrial activities.

For each of the candidate waveforms, we initially investigate two cases:
\begin{enumerate}
\item Case 1: DUEs pairs deploy waveforms from the pool of waveforms under study, CUEs use CP-OFDM.
\item Case 2: Both DUE pairs and CUEs deploy waveforms from the pool of waveforms under study.
\end{enumerate}
We also examine the effects of the timing offset on the relative performance of all waveforms, ranging from perfectly synchronised to fully asynchronous communication. An analogous investigation is performed for CFO by varying LO inaccuracies.

\nomenclature[A]{LO}{Local Oscillator}
\nomenclature[A]{IQR}{Inter-Quartile Range}
\nomenclature[A]{PPM}{Parts Per Million}
\subsubsection{System Performance}

For the SINR and rate metrics, we present our findings using box plots which allow us to present the results for each waveform in a single plot in a manner that is easily readable. A box plot is a convenient way of representing a distribution using a box that spans from the first quartile (bottom edge) to the third quartile (top edge). A solid horizontal line in the box represents the median, while the mean is marked with a dashed horizontal line. 

The span of the box, from the top edge to the bottom edge, is known as the inter-quartile range ($ \rm IQR$). Whiskers, the lines extending from either end of the box, denote the maximum and minimum values. Values lying outside the whiskers are marked individually and classified as outliers. A value is considered to be an outlier if it is either $1.5 \times \rm IQR$ or more above the third quartile or $1.5 \times \rm IQR$ or more below the first quartile.


\paragraph{DUE SINR Performance:}

\begin{figure}[t]
  \centering
    \includegraphics[width=0.8\linewidth,keepaspectratio]{due_sinr}
    \caption{The box plots of DUE SINR show that a large performance increase can be obtained by choosing an appropriate alternative waveform.}
    \label{fig:basecase_d2d_sinr}
\end{figure}

Fig. \ref{fig:basecase_d2d_sinr} presents box plots summarizing the SINR distribution for DUEs according to each considered waveform couple. The baseline CP-OFDM case, assuming no timing or frequency offsets, performs quite well and achieves an average SINR value of approximately 22dB. This, however, reduces to approximately 13dB when asynchronous communication is considered, with UFMC and GFDM exhibiting similar average values. When both CUEs and DUEs employ an alternative waveform in the set \{OFDM/OQAM, FMT, FBMC-PAM, f-OFDM\}, performance comparable to the baseline case is achieved even though communication is asynchronous. Interestedly, this same set of waveforms performs quite well in the coexistence scenarios in which CUEs use CP-OFDM and DUEs use an alternative waveform, with averages values approximately 3dB less than the baseline case, but almost 6dB greater than asynchronous CP-OFDM in some cases.

We note that the number of outliers is relatively small (approx. $2.2\%$) compared to the number of DUEs in the data set. The presence of outliers is not unusual; while the majority of DUEs will experience a similar SINR to one another, especially favourable or unfavourable channel conditions will inevitably result in DUEs with SINRs that are considerably higher or lower than average. As stated previously, our aim is to compare the relative performance of waveforms, not to propose a specific resource allocation technique for DUEs. Employing a more intelligent RA scheme for DUEs that accounts for interference coordination and fairness could result in less outliers and better performance equality among DUEs, but is not within the scope of this study.

\paragraph{DUE Rate Performance:}

\begin{figure}[t]
  \centering
    \includegraphics[width=0.8\linewidth,keepaspectratio]{DUE_rate}
    \caption{Rate performance of DUEs taking into account bandwidth efficiency.}
    \label{fig:basecase_d2d_rate}
\end{figure}

We also wish to show the achieved rate of DUEs, taking into account the bandwidth efficiency of each modulation scheme. Fig. \ref{fig:basecase_d2d_rate} shows the achieved rate of DUE pairs for each waveform. The greatest performance is achieved when both CUEs and DUEs use OFDM/OQAM, closely followed by FBMC-PAM. The baseline synchronous CP-OFDM case yields the next highest achieved rate. The variation in achieved rate, despite each of the aforementioned cases exhibiting similar SINR values, can be attributed to the different bandwidth efficiencies of the waveforms, listed in Table \ref{tab:speceff}.

In the coexistence scenarios, in which CUEs use CP-OFDM and DUEs use an alternative waveform, both OFDM/OQAM and FBMC-PAM exhibit the best performance. In particular, in the scenario in which OFDM/OQAM is used by DUEs, the achieved rate is just 7.0\% less than the synchronous CP-OFDM baseline case, but 43.3\% greater than the asynchronous CP-OFDM case. This is an encouraging result, as DUEs could enjoy the benefits that asynchronous communication would allow without suffering a large degradation in performance. It also allows for the possibility that 5G will permit multiple waveforms, whereby different services employ the waveform that is best suited to them. 

\paragraph{CUE Performance:}

\begin{figure}[t]
  \centering
    \includegraphics[width=0.8\linewidth,keepaspectratio]{CUE_i}
    \caption{The DUE to CUE interference is similar to the value of noise per resource block for coexistence cases.}
    \label{fig:basecase_reduction_in_cu_sinr}
\end{figure}

It is imperative that CUE performance is not significantly degraded by the inclusion of DUEs in the network. Interference in this case comes from two sources. First, CUEs will experience interference from DUEs operating on the same resource block as them. Secondly, CUEs will be affected by interference leakage from DUEs on different resource blocks, resulting from the misaligned communications. Fig. \ref{fig:basecase_reduction_in_cu_sinr} demonstrates that the average DUE to CUE interference is quite low. This can be attributed to two factors: the low transmit power of DUEs (-5dB) and the use of strict FFR. 

In the baseline case, the average DUE to CUE interference is less than -120dBm. In cases where both CUEs and DUEs use an alternative waveform from the set \{OFDM/OQAM, FMT, FBMC-PAM, f-OFDM\}, the interference experienced by CUEs is lower than the baseline case. In all other cases, the interference is comparable to the noise per RB. 

In the synchronous baseline case, CUEs will still suffer slightly from leakage interference from DUEs, owing to the fact that different cells in LTE are misaligned in time. Hence, the use of an appropriate alternative waveform by both sets of users can assist in reducing the interference that CUEs experience from DUEs. However, if CUEs use CP-OFDM, then employing an alternative waveform for DUEs has little effect on CUEs; its main benefit is to increase the performance of the DUEs themselves. For CUEs, the interference from DUEs using the same RB will generally be a greater factor than leakage. In contrast, for DUEs, leakage interference from other DUEs in close proximity is the dominant type of interference, and hence they can benefit from adopting a waveform with better spectral localization than CP-OFDM.

Although the DUE to CUE interference is quite low, it is not negligible and requires attention, particularly in the asynchronous coexistence cases. 
In particular, the presence of outliers suggests that while the majority of CUEs suffer little degradation to their performance, a small number of users suffer a large reduction in performance. These users are victims of the specific spatial distribution of transmitting users at that instant, in which strict FFR and the low transmit power of DUEs fail to offer sufficient protection. 

Fig. \ref{fig:basecase_reduction_in_cu_sinr} demonstrates that employing a different waveform for DUEs, while CUEs continue to use CP-OFDM, does little to mitigate DUE to CUE interference, which is consistent with our preliminary analysis of section \ref{sec:prel_analysis} and the interference study detailed in chapter \ref{chapter:3}. In these cases, additional protection is needed to ensure that interference to CUEs is kept at an acceptable level and that the minority of users who suffer significant degradation can also obtain adequate performance. This may take the form of intelligent resource allocation schemes that aim to protect vulnerable CUEs by assigning resources in a manner that reduces DUE to CUE interference. As stated previously, such schemes are not within the scope of this study as we are solely interested in demonstrating the effect on performance of employing different waveforms.

%\begin{figure}[t]
%  \centering
%    \includegraphics[width=3.5in,height=3.5in,clip,keepaspectratio]{basecase_cu_outage_probability}
%    \caption{DUEs in the network increase the outage probability of CUEs, although this is less significant at lower outage thresholds.}
%    \label{fig:basecase_cu_outage_probability}
%\end{figure}
%
%Fig. \ref{fig:basecase_cu_outage_probability} shows the outage probability for CUEs for three different thresholds: -$3 \rm dB$, $0 \rm dB$, $3 \rm dB$. For the sake of comparison, we plot the outage probability for each threshold for two cases: when DUEs are not present, and when they are. The introduction of transmitting DUEs into the network increases the outage probability of CUEs, as was hinted by the outliers in Fig. \ref{fig:basecase_reduction_in_cu_sinr}. This increase is more prominent at higher outage thresholds. At a threshold of $3 \rm dB$, a further $3\%$ of CUEs are in outage for the case in which both sets of devices use OFDM. At lower thresholds, such as -$3 \rm dB$, the increase is less significant (approximately $2\%$).

\subsubsection{DUE Transmit Power}

%We note that the $\rm IQR$ for each distribution presented in the previous subsection is quite similar, and that the distributions are basically shifted versions of one another. Hence, we can summarize DUE and CUE performance using average values, without providing box plots.

\begin{figure}[t]
  \centering
    \includegraphics[width=0.7\linewidth,keepaspectratio]{tx_power.pdf}
    \caption{Increasing DUE transmit power results in an increase in DUE SINR at the cost of increased interference to CUEs.}
    \label{fig:tx_pow_trend}
\end{figure}

We investigate the effect that DUE transmit power has on system performance by varying the DUE transmit power from -15dBm to 15dBm in 5dBm increments, while holding all other parameters at the same value as in Table \ref{tab:sim_params2}. We do not include cases in which both CUEs and DUEs use an alternative waveform, as we are more interested in the coexistence cases. Fig. \ref{fig:tx_pow_trend} provides insight into how the DUE SINR performance and the interference experienced by CUEs changes according to different DUE transmit powers. The upper sub-plot shows average DUE SINR, while the lower sub-plot shows the average DUE to CUE interference.

As intuition suggests, increasing the DUE transmit power will increase DUE SINR at the cost of increased interference to CUEs. The case in which DUEs employ CP-OFDM for asynchronous communication exhibits the worst performance while the synchronous baseline case achieves the greatest performance for transmit powers below 7.5dBm, at which point it is overtaken by both OFDM/OQAM and f-OFDM. Leakage interference from D2D pairs in other cells will be present even in the synchronous case as neighbouring cells do not achieve time alignment. While OFDM/OQAM and f-OFDM successfully mitigate this type of leakage interference, it becomes significant at high transmit powers for synchronous CP-OFDM and, hence, the curve representing the baseline case begins to taper as the transmit power is increased.

In particular, we draw the readers' attention to two points. 
\begin{enumerate}
\item First, for the case in which DUEs do not use an alternative waveform from the set \{OFDM/OQAM, FMT, FBMC-PAM, f-OFDM\}, successively higher transmit powers provide increasingly diminishing returns since increasing DUE transmit power will also increase the inter-DUE leakage interference. This is evident in Fig. \ref{fig:tx_pow_trend}, in which the set of curves at the bottom of the upper sub-plot gradually begin to level off as the DUE transmit power is increased. In contrast, the curves at the top of the upper sub-plot, representing scenarios in which DUEs employ a waveform from the set \{OFDM/OQAM, FMT, FBMC-PAM, f-OFDM\}, increase almost linearly with DUE transmit power since inter-DUE leakage interference is effectively negligible in these cases.
\item Secondly, the benefit to DUEs of using an alternative waveform will be greater at higher values of DUE transmit power since inter-DUE leakage will be more prominent. However, as the DUE transmit power is increased, there is a linear increase in the interference experienced by CUEs. %Hence, while the greatest DUE performance benefits associated with using an alternative waveform are obtained at high DUE transmit powers, degradation of CUE performance becomes substantial.
\end{enumerate}

The main consequence of these observations is that higher DUE transmit powers provide increasingly diminishing returns unless an alternative waveform that adequately mitigates leakage interference is employed. A maximum permissible transmit power should be chosen for DUEs that achieves a balance between adequate average DUE SINR and an acceptable level of interference to CUEs. The value of -5dBm chosen in Table \ref{tab:sim_params} reasonably achieves this, with DUEs achieving a SINR close to 20dB when they employ OFDM/OQAM while limiting interference to CUEs to approximately the noise value per RB.
We also note that we can trade off some DUE performance for reduced interference to CUEs. We observe, however, that the DUE to CUE interference is at a minimum for the synchronous baseline case. 

\subsubsection{Cell Radius}

\begin{figure}[t]
  \centering
    \includegraphics[width=0.7\linewidth,keepaspectratio]{cell_radius.pdf}
    \caption{As the cell radius increases, DUE SINR increases and reduction in CUE SINR decreases.}
    \label{fig:cell_radius_trend}
\end{figure}

In this subsection, we investigate the influence that cell size has on performance by varying the cell radius from 200m to 1000m in 100m increments while holding all other parameters at the same value as in Table \ref{tab:sim_params2}. We display the results in Fig. \ref{fig:cell_radius_trend}: for cell radii under 500m, we consider an urban environment and use the appropriate pathloss models for this scenario, while for cell radii greater than 500m, we consider a suburban environment.

At the smallest cell radius considered (200m), average DUE SINR is at its lowest and average DUE to CUE interference is at its greatest. This is understandable, and readily explained. According to the strict FFR scheme employed, DUEs reuse the resources of CUEs in neighbouring reuse regions. At small cell sizes, the average distance between devices in neighbouring reuse regions is reduced. This results in greater CUE to DUE interference and reduces DUE SINR. As the cell radius increases, so too does the distance between reuse regions, and DUE SINR increases. This increase is mainly observed at smaller cell sizes; at large cell sizes, CUE to DUE interference is almost negligible and further increases to cell radius exhibit little or no increase in DUE SINR.

DUE to CUE interference, on the other hand, occurs at base stations. In small cells, the average distance between clusters and the base stations serving neighboring reuse regions is shorter, resulting in higher DUE to CUE interference. This is shown in the lower sub-plot in Fig. \ref{fig:cell_radius_trend}, in which we observe that the average DUE to CUE interference decreases as the cell radius increases.

Therefore, as the cell size increases, average DUE to CUE interference decreases and average DUE SINR increases. Essentially, the greater the cell size the more protection strict FFR offers against the various types of interference, as the reuse regions are further apart. 

We note at the maximum cell radius considered (1000m), f-OFDM, FBMC-PAM, and OFDM/OQAM each offer approximately a $7 \rm dB$ improvement in the coexistence scenario over the asynchronous CP-OFDM case. At large cell sizes, even further gains are achievable as DUEs could transmit at a higher power without affecting CUEs. Unfortunately, at the other end of the scale, DUEs in very small cells might not be able to transmit at a power level that allows them to obtain the required data rates without interfering too much with CUEs. In these cases, additional measures may need to be developed and deployed to enable DUE communication, such as advanced resource allocation schemes that minimize the interference from DUEs to CUEs. Failing that, DUE communication may only be permitted to underlay sufficiently large cells.

Over the range of cell radii considered, synchronous CP-OFDM provides the best performance and asynchronous CP-OFDM provides the worst. However, as the cell radius increases, the interference from CUEs in neighbouring reuse regions to DUEs is reduced and the performance of several alternative waveforms approaches that of synchronous OFDM. As observed in previous plots, the baseline synchronous OFDM case results in the least amount of interference to CUEs, although this interference is quite low in all cases as the cell radius increases.    


%\begin{figure}[t]
%  \centering
%    \includegraphics[width=\linewidth,keepaspectratio]{cell_radius_outage}
%    \caption{The increase in the outage probability over the case in which no DUEs are present is small for cell sizes with a 500m radius or more, but grows dramatically for cells smaller than this.}
%    \label{fig:cell_radius_outage}
%\end{figure}
%
%The lower sub-plot in Fig. \ref{fig:cell_radius_trend} shows the average reduction in CUE SINR; however, we need also to concern ourselves with the more vulnerable users. Fig. \ref{fig:cell_radius_outage} shows the outage probability of CUEs for a threshold value of $0 \rm dB$. The dotted black line represents the baseline for comparison in which no DUEs are present in the network and CUEs use OFDM. We note that for a radius of 500m and higher, the addition of DUEs only results in a maximum of a further 2.5\% of CUEs experiencing outage, with this value reducing as the cell size becomes larger. Intuitively, we also observe that the outage probability increases in accordance with cell size, as cellular users in the outer region of the cell are less likely to be able to compensate for pathloss without exceeding their maximum power budget of $24 \rm dBm$.
%
%However, for cell-radii less than 500m, the percentage of CUEs in outage increases substantially. This reinforces our earlier point that for small cell sizes, DUE communication may not be possible without the assistance of intelligent resource allocation procedures. 
%
%Fig. \ref{fig:cell_radius_outage} also hints at the reason behind this. We note that the curves representing the cases whereby both sets of users use a waveform in the set \{FBMC/OQAM, FMT, FBMC-PAM, f-OFDM\} track the baseline case (in which there are no DUEs) closely after a cell size of 500m. This indicates that above this value, DUE-to-CUE interference is predominantly leakage. Below a cell radius of 500m, the curves rise dramatically, from which we infer that DUE-to-CUE interference in these cases is predominately caused by DUEs operating on the same resource blocks as CUEs. This type of interference is substantially more significant than leakage in this case, and arises from the short distance between the reuse regions defined by strict FFR.

\subsubsection{Cluster Radius}

\begin{figure}[t]
  \centering
    \includegraphics[width=0.7\linewidth,keepaspectratio]{cluster_radius.pdf}
    \caption{Employing an appropriate alternative waveform for DUEs yields the greatest benefit in small clusters in which inter-DUE leakage interference is most significant.}
    \label{fig:cluster_radius_trend}
\end{figure}

We study the impact that cluster radius has on performance. We present the results in Fig. \ref{fig:cluster_radius_trend}, varying the cluster radius from 30m to 100 m in 10 m increments. Reducing the cluster radius necessitates a corresponding change in the distance between a DUE transmitter and receiver, which we modeled using a uniform random variable. Accordingly, we choose the parameters $a$ and $b$, representing the minimum and maximum Tx-Rx distances, respectively, of the uniform random variable $U_{[a, b]}$ as follows: $a = 5\rm m$; $b = (\textrm{cluster radius}) - 10\rm m$.

Increasing the cluster radius has two opposing influences on DUE SINR. On the one hand, it results in reduced inter-DUE interference, which should boost the SINR. On the other hand, it also results in reduced received signal power, which should cause the SINR to decrease. This makes it difficult to predict how the average SINR will change. In Fig. \ref{fig:cluster_radius_trend}, we see that the reduction in received power is more influential and DUE SINR decreases as cluster radius increases. We concede, however, that this is somewhat dependant on how the distance between DUE transmitters and receivers is modelled (such as the parameters $a$ and $b$), as this affects by how much the received power will decrease. We also note that for small cluster sizes, and in cases where DUEs do not use a waveform in the set \{OFDM/OQAM, FMT, FBMC-PAM, f-OFDM\}, SINR decreases slowly at first as the reduction in inter-DUE interference is almost significant enough to counter-act the effect of lower received signal powers.

Reducing the cluster radius increases the density of DUEs in the cluster, resulting in greater inter-DUE interference. Hence, employing an appropriate alternative waveform for DUEs yields the greatest benefit in dense clusters in which inter-DUE leakage interference is most significant. The synchronous baseline case again performs the best; however, the performance for cases where DUEs use a waveform in the set \{OFDM/OQAM, FBMC-PAM, f-OFDM\} approach that of the baseline for small cluster sizes. This can be attributed to reduced leakage interference from CUEs in the same reuse region, as smaller clusters are less likely to encompass CUEs in the same cell.


\subsubsection{Amount of Time and Frequency Misalignment between Devices}
The final parameters whose influence on performance interests us are the maximum permitted timing offset and CFO. Both CFO and TO affect DUE performance similarly, and so it makes sense to isolate them when studying their effects on performance. Hence, when examining the effect of timing offset on DUE performance, we consider a case involving no CFO. Similarly, when investigating the effects of CFO, we consider devices to be perfectly aligned in time.

\paragraph{Maximum Possible Timing Offset:}
As expressed in Section \ref{section:system_model}, rather than relying on average values of interference like many papers in the literature \cite{Medjahdi2010a}, we employ a more realistic approach of assigning specific values of timing offset $\delta_\mathrm{t}$ to individual DUEs using a uniform random variable. 


\begin{figure}[t]
  \centering
    \includegraphics[width=0.7\linewidth,keepaspectratio]{timing_offset.pdf}
    \caption{DUE SINR performance as the maximum permitted timing offset is varied.}
    \label{fig:max_to}
\end{figure}


We vary the maximum permissible timing offset as a fraction of the time spacing between two OFDM symbols from 0 (full synchronism) to 1 (full asynchronism) in 0.1 increments. Limiting the maximum permissible timing offset corresponds to a case in which coarse alignment has been obtained; for example, 0.2 would correspond to the case in which devices are synchronized to within 20\% of an OFDM symbol time. %As outlined in Section \ref{section:interference_model}, the time spacing between two OFDM symbols is equal to $T + T_\text{CP}$, which is equivalent to $\frac{9}{8\Delta\text{F}}$.

Fig. \ref{fig:max_to} illustrates the results. The black line representing the case whereby both DUEs and CUEs use CP-OFDM will be our baseline for comparison, and it can be seen that SINR drops rapidly when the timing offset is greater than the cyclic prefix. The cyclic prefix duration $T_\text{CP}$ for CP-OFDM is $12.5\%$ of the symbol duration $T$. We can divide the rest of the graph into two scenarios:
\begin{enumerate}[i)]
%\item \textit{CUE SINRS}: The group of lines at the bottom of the graph present CUE SINR performance. The proximity of the lines to each other demonstrate that the use of alternative waveforms has little effect on CUE performance, due to the fact that interference from DUEs operating on the same resource block tends to be more significant than leakage.
\item \textit{Scenario in which DUEs use a waveform in the set \{GFDM, UFMC\}, and CUEs use CP-OFDM}: These curves become quite similar as the maximum permissible timing offset increases, and are out-performed by our baseline OFDM-OFDM case. This seems surprising at first glance, but can be explained. Indeed, we saw in Fig.~\ref{fig:interference_tables} that, with the chosen parameters, UFMC and GFDM still cause a significant amount of interference between coexisting users in homogeneous links in which both users are deploying one of these waveforms; thus, inter-DUE interference is quite important if DUEs use either GFDM or UFMC. Moreover, CP-OFDM based users are orthogonal with one another as long as $\delta\mathrm{t}$ is contained in the CP duration. However, GFDM or UFMC users never achieve orthogonality with OFDM users, which explains that if CUEs use OFDM, CUE to DUE interference is on average more significant if DUEs use UFMC or GFDM than if they also employ OFDM.
\item \textit{Coexistence scenario in which DUEs use a waveform in the set \{OFDM/OQAM, FBMC-PAM, f-OFDM, FMT\} and CUEs use OFDM}: As the timing offset increases, the curves exhibit similar performance. At a maximum timing offset, the benefit to using one of these alternative waveforms for DUEs is considerable. With the exception of f-OFDM, the performance of these waveforms varies little according to the timing offset, and they are outperformed by the baseline CP-OFDM case for very low timing offsets ($< 20\%$). f-OFDM has an interesting behaviour, as it is the only waveform that is affected differently by CP-OFDM according to the value of $\delta_\mathrm{t}^\mathrm{max}$. This is due to the fact that for small timing offsets, f-OFDM and OFDM achieve quasi-orthogonality, which is then lost as $\delta\mathrm{t}$ increases.
%\item \textit{Scenario in which both DUEs and CUEs use a waveform in the set \{FBMC/OQAM, FBMC-PAM, f-OFDM, FMT\}}: These waveform choices provide the greatest performance increases at timing offsets greater than $20\%$, with only modest deviations from the baseline case in smaller timing offsets. Performance is unchanging across the range of possible maximum timing offset values, with f-OFDM and FBMC/OQAM outperforming FBMC-PAM and FMT by approximately one decibel.
\end{enumerate}


\paragraph{Maximum Possible CFO:}
Having investigated the effect of TO, we now examine the relative performance of the waveforms under various levels of CFO. The LO inaccuracy is varied from 0ppm to 3.5ppm in increments of 0.5, corresponding to frequency offsets of 0kHz to +/- 7kHz in 1kHz increments at a carrier frequency of 2GHz. 

\begin{figure}[t]
  \centering
    \includegraphics[width=0.7\linewidth,keepaspectratio]{cfo.pdf}
    \caption{DUE SINR performance as the maximum CFO is varied.}
    \label{fig:cfo_trend}
\end{figure}

In Fig. \ref{fig:cfo_trend}, for the case in which CP-OFDM is used by both sets of users, we observe that the average DUE SINR reduces as the frequency offsets become greater. This is in line with the sensitivity to asynchronism of CP-OFDM we have illustrated in chapter \ref{chapter:2}. In a similar fashion to the study on the effects of TO, we again take the case in which both sets of users employ CP-OFDM to be our baseline case, and divide the rest of Fig. \ref{fig:cfo_trend} into two scenarios:



\begin{enumerate}[i)]
%\item \textit{CUE SINRS}: The group of lines at the bottom of the graph present CUE SINR performance. The proximity of the lines to each other demonstrate that the use of alternative waveforms has little effect on CUE performance, due to the fact that interference from DUEs operating on the same resource block tends to be more significant than leakage.
\item \textit{Scenario in which DUEs use a waveform in the set \{GFDM, UFMC\}, and CUEs use CP-OFDM}:
When DUEs employ GFDM or UFMC, DUE SINR decreases as the maximum possible LO inaccuracy is increased; however, the decrease occurs at a lower rate than for CP-OFDM. For low LO inaccuracies, the baseline OFDM case outperforms the scenarios in which CUEs use CP-OFDM and DUEs use either UFMC or GFDM. While CP-OFDM users achieve near orthogonality at low CFOs, GFDM or UFMC users never achieve orthogonality with CP-OFDM users. However, as the LO inaccuracy is increased, CP-OFDM suffers from increasingly large interference leakage and the waveform choices involving UFMC or GFDM begin to outperform the baseline CP-OFDM case.   
\item \textit{Coexistence scenario in which DUEs use a waveform in the set \{OFDM/OQAM, FBMC-PAM, f-OFDM, FMT\} and CUEs use CP-OFDM}:
The waveform choices involving OFMC/OQAM, FBMC-PAM, and f-OFDM are largely unaffected by varying CFO, as made evident by the horizontal lines in Fig. \ref{fig:cfo_trend}. At the LO inaccuracies considered, frequency offsets are contained within +/- half a subcarrier. Given that these schemes use a guard band of half a subcarrier at either side of an RB, and that leakage is confined within a similar range for these alternative waveforms, it is not surprising that very little variation in performance is observed as the CFO is increased. FMT, on the other hand, uses 12 subcarriers per RB. Hence, we observe that the SINR performance of DUEs using FMT reduces as the maximum possible LO inaccuracy is increased.
Out of the waveform couples considered in this scenario, f-OFDM exhibits the best performance and is never outperformed by the baseline CP-OFDM case. The  waveform choices involving FBMC-PAM and OFDM/OQAM only begin to outperform the baseline OFDM case after approximately 1ppm. For DUE users using FMT, improvements in SINR over the baseline case are only observed after a maximum LO inaccuracy of 1.3ppm (based on an interpolated value).
%\item \textit{Scenario in which both DUEs and CUEs use a waveform in the set \{FBMC/OQAM, FBMC-PAM, f-OFDM, FMT\}}: These waveform choices again offer the greatest benefits, performing at least as well as the baseline case when no CFO is present and offering increasingly substantial gains as the CFO increases. Similar to the previous scenario, the performance of these waveform couples is largely unaffected by increasing LO inaccuracy.
\end{enumerate}