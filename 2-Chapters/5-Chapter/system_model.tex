%!TEX root = ../../thesis.tex
% Expansion of previous paper. Why uplink.
In this section, we expand upon the previous study and consider clustered deployments of D2D systems underlaying a legacy LTE-A network. Our study, which is application-agnostic can find applications in a number of scenarios: intelligent process control, autonomous manufacturing systems, smart factories, and self-organising warehouses are but a few examples of 5G use cases which require high-rate inter-machine communication in a spatially clustered environment.

% % Network model.
\subsubsection{Network Set-up}
% Network set-up.
As in our preliminary study, we stay consistent with the literature and consider uplink resource sharing for two reasons. Firstly, and most importantly, D2D user equipments (DUEs) should not interfere with crucial pilot information broadcast in the downlink. Secondly, for uplink resource sharing, DUE to CU equipment (CUE) interference occurs centrally at the base station. Therefore, the BS possesses full knowledge of this type of interference and can attempt to coordinate and mitigate it through resource allocation schemes \cite{Fodor2012}. 

\nomenclature[A]{DUE}{D2D User Equipment}
\nomenclature[A]{CUE}{Cellular User Equipment}
\nomenclature[A]{UE}{User Equipment}
\nomenclature[A]{FFR}{Fractional Frequency Reuse}

We consider an OFDMA network with parameters selected based on the 3GPP LTE standard, as outlined in Table \ref{tab:sim_params2} in Subsection \ref{subsection:scenario}. Cells are modeled as hexagons, with the network consisting of a central cell of interest and two rings of neighboring cells (totaling nineteen cells). We assume that each cell is fully loaded, with each CUE assigned a single uplink RB. DUEs underlay the OFDMA cell by reusing a single uplink resource block (RB). In reality, LTE networks actually use SC-FDMA in the uplink. However, as explianed in Chapter \ref{chapter:2}, SC-FDMA is simply OFDMA in which users apply DFT precoding to their transmit signal. In the scope of our study, this precoding is inconsequential and we therefore do not consider it.

% Distribution of CUEs and DUEs.
CUEs are distributed throughout the entire network according to a Poisson point process (PPP). DUEs are employed in high-rate spatially clustered applications such as process control, robotics control, or machine-to-machine communications. In order to capture this clustering effect in our model, we distribute DUE transmitters in the network using a Mat{\'e}rn point process which generates clusters according to a two-step process. In the first step, a PPP is used to generate a set of parent points. In the second step, a Poisson distributed number of child points is uniformly distributed around each parent point within a disk of a given radius. The rate of occurrence of clusters (i.e. parent points), DUE transmitters per clusters (i.e. child points per parent point), and the cluster radii can be configured in the simulation parameters. For each DUE transmitter, we distribute a receiver at a distance $d$ according to a uniform random variable $U_{[a, b]}$, with $a$ and $b$ representing the minimum and maximum distance, respectively.

\begin{figure}[t]
\centering
	\includegraphics[width=0.7\linewidth,trim={2cm 8cm 2cm 1.5cm},clip,keepaspectratio]{FigureFFR69}
	\caption[Frequency reuse scheme]{The inner region of each cell uses the same set of sub-bands, while reuse three is employed in the outer regions. CUEs and DUEs are allocated sub-bands in a  manner that aims to reduce interference between them.}
	\label{fig:ffr_allocation}
\end{figure}

\nomenclature[A]{PPP}{Poisson point process}
% Strict FFR overview.
When multiple cells are considered, harmful inter-cell interference occurs. However, its effects can be lowered through the use of adapted frequency reuse schemes, in which devices in different cells are assigned different frequency bands. In particular, fractional frequency reuse (FFR) \cite{Novlan2011} has drawn a lot of attention in the recent years. Therefore, in this study, we consider the use of strict FFR. Fig. \ref{fig:ffr_allocation} illustrates the division of sub-bands between cells. The CUEs in the inner region of each cell are provisioned using a common set of sub-bands. Frequency reuse three is employed for the outer regions of the cells, with cell-edge CUEs in these regions provisioned from one of three sets of sub-bands. DUEs are permitted to reuse the spectral resources of cellular users according to the scheme outlined in \cite{chae2011radio} for D2D communication underlaying a network employing strict FFR. Hence, DUEs in the inner region of a cell may reuse the spectral resources assigned to CUEs in the outer regions of neighbouring cells. DUEs in the outer region of a cell may use any spectral resource, except the set assigned to CUEs in the same region.
%DUEs in the outer region of a cell may use the spectral resources assigned for use by CUEs in the inner regions of the cells, or by CUEs in the outer regions of neighbouring cells.


% Strict FFR splitting sub-bands.
The ratio of the radius of the inner region ($R_{\textrm{inner}}$) to the radius of the cell ($R_{\textrm{cell}}$) is an important parameter in strict FFR systems and influences how sub-bands are divided between regions. We follow the approach used in \cite{Saquib2013Fractional}, and choose the ratio $R_{\textrm{inner}}/R_{\textrm{cell}}$ to be $0.65$, which was shown in \cite{Novlan2010Comparison} to maximise the average network throughput for uniformly distributed CUEs. Given $N_{\textrm{band}}$ available sub-bands in the system, we can determine the number of resources allocated to each region as follows \cite{Novlan2010Comparison}
\begin{equation}
N_{\textrm{inner}} = \left[N_{\textrm{band}}\left(\frac{R_{\textrm{inner}}}{R_{\textrm{cell}}}\right)^2\right],
\end{equation}
\begin{equation}
N_{\textrm{outer}} = [(N_{\textrm{band}}-N_{\textrm{inner}})/3].
\end{equation}

\nomenclature[A]{PUSCH}{Physical Uplink Shared Channel}

% % Resource Allocation.
\subsubsection{Resource Allocation}
% RB assignment. 
%The use of proportional fair scheduling is prevailing in LTE networks. However, given that this is a simulation-oriented work based on Monte Carlo methods, each simulation iteration represents a snapshot of the network at an instant in time and is memoryless with respect to the preceding state of the system. Hence, any scheduling technique such as proportional fair that relies on historical knowledge of the system is not applicable, and RBs can instead be assigned randomly under the condition that an RB may only be assigned to a single CUE, and reused by a single DUE, in a given cell.
RBs are assigned under the condition that an RB may only be assigned to a single CUE, and reused by a single DUE, in a given cell. CUEs transmit on the physical uplink shared channel (PUSCH) and use a power control procedure that assigns each CUE a power level that results in acceptable signal reception at the base station. The power control procedure \cite{cox2012introduction} for a CUE transmitting during subframe $i$ is described by the formula
\begin{equation*}
P(i) = P_{\textrm{O\_PUSCH}} + 10\log_{10}(M_{\textrm{PUSCH}}(i))+\Delta_{\textrm{TF}}(i)+\alpha{L}+f(i),
\end{equation*}
where $P_{\textrm{O\_PUSCH}}$ is the power that the base station expects to receive over a single resource block, $M_{\textrm{PUSCH}}(i)$ is the number of resource blocks on which the device is transmitting, $\Delta_{\textrm{TF}}(i)$ is an optional adjustment for higher modulation orders and coding rates, $L$ is the estimation of downlink path loss, $\alpha$ is a weighting factor to reduce the impact of changes in path loss, and $f(i)$ relates to power control commands from the base station. As previously stated, we assume that each CUE uses only a single RB, implying that $10\log_{10}(M_{\textrm{PUSCH}}(i))=0$. The maximum power at which a CUE may transmit is also capped at $P_{\textrm{cmax}}$. Therefore, the transmit power of a CUE operating during subframe $i$ is given by
\begin{equation}\label{eq:pc}
P_{\textrm{CUE}}(i) = \min(P(i), P_{\textrm{cmax}}).
\end{equation}


Our focus in this study is on evaluating the relative performance of the waveforms under consideration for direct communication between devices in spatially clustered use cases, not on proposing a new resource allocation scheme. Hence, in order to avoid bias towards any particular scheme, we consider a simple power allocation scheme for DUEs where DUEs are permitted to transmit at maximum power, which is capped by the controlling base station.

%In reality, such a scheme is well rationalised in our analysis. If feasible, the use of asynchronous communication between DUEs would remove several duties of control signalling from the BS and potentially open up possibilities for new types of resource allocation schemes, as was mentioned in the introduction section. Before such schemes are designed, it is necessary 
%to first demonstrate that a set-up involving asynchronous direct communication in a strict FFR network is possible. It is also necessary 
%to detail the specific needs of such a set-up, since inter-DUE leakage becomes significant in the type of clustered asynchronous communication being considered. We evaluate how well different waveforms mitigate this type of interference, and in which scenarios further mitigation will be needed through other means.

% % Channel Modelling.
\subsubsection{Channel Modelling}
% Sources of interference.
CUEs in the same cell do not interfere with each other, as we assume they are perfectly synchronized by the BS. Therefore, there are four main interference types requiring consideration:
\begin{enumerate}
\item DUE pairs interfere with the CUEs' transmissions. Since we are investigating uplink resource sharing, this interference is observed at base stations. 
\item Conversely, the CUEs interfere with the DUE pairs at DUE receivers. 
\item DUEs interfere with each other (inter-DUE interference).
\item CUEs in different cells are not synchronized and, hence, interfere with each other (inter-CUE interference).
\end{enumerate}

% Channel Model.
As in our preliminary study, we employ the WINNER II channel models \cite{Kyosti2008} to provide us with a distance based path loss, which also incorporates the probability of line-of-sight. Distinct path loss models are used for the different types of links in the system in order to represent the network in a realistic manner. Path loss models employed for D2D channels have been modified so that both transceivers in a D2D link are the same height above the ground. 
The distribution of shadow fading is log-normal, with the standard deviation specified by the Winner II channel models for each scenario.

% % Metrics of interest.
\subsubsection{Performance Measures}
Below, we present several metrics and details of their formulation that we will use to evaluate the performance of the system. All metrics are evaluated for DUEs and CUEs in the central cell, which represents the cell of interest.
\paragraph{SINR:}
The SINR of a CUE $j$ in the central cell $o$ using RB $k$ is given by:
\begin{equation}\label{eqn:cue_sinr}
\gamma_{j_o}^k\ = \frac{P_{j_o}^kh_{j_oB}^k}{\sigma_w^2 + I_{\rm C_{\rm N}} + I_{\rm D_{\rm N}} + I_{\rm D_{\rm S}}},
\end{equation}
where $P_{j_o}^k$ is the transmit power of the CUE (given by Eq. (\ref{eq:pc})), $h_{j_oB}$ is the channel gain between the $j^{\rm th}$ CUE and the BS of the central cell $o$, and $\sigma_w^2$ is the AWGN variance. $I_{\rm C_{N}}$ is the interference from CUEs in neighbouring cells and is given by
\begin{equation}\label{eq:Inc}
I_{\rm C_{\rm N}} = \sum_{n \in \mathcal{N}} \sum_{c_n \in \mathcal{C}_n} \sum_{r \in \mathcal{R}} P_{c_n}^r h_{c_nB}^r \Omega_{\textrm{wf}_{c_n}\rightarrow\textrm{wf}_{j_o}}(|r - k|, \delta_\mathrm{t}, \delta_\mathrm{f}),
\end{equation}
where $n$ indexes the set of neighbouring cells $\mathcal{N}$, $c_n$ indexes the CUEs in the set $\mathcal{C}_n$ of CUEs in the $n^{\rm th}$ neighbouring cell, and $r$ indexes the set of resource blocks $\mathcal{R}$ available to the system.  $P_{c_n}^r$ is the transmit power of the $c_n^{\rm th}$ CUE operating on RB $r$ (given by Eq. (\ref{eq:pc})), $h_{c_nB}^r$ is the channel gain between the $c_n^{\rm th}$ CUE and the BS of the central cell. If the $c_n^{\rm th}$ CUE is not operating on RB $r$, then $P_{c_n}^r$ is $0$. Finally, $\Omega_{\textrm{wf}_{c_n}\rightarrow\textrm{wf}_{j_o}}(|r - k|, \delta_\mathrm{t}, \delta_\mathrm{f})$  is the fraction of power injected by CUE $c_n$ using waveform $\textrm{wf}_{c_n}$ and resource block $r$ onto CUE $j_o$ using waveform $\textrm{wf}_{j_o}$ and resource block $k$, at a timing offset of $\delta_\mathrm{t}$ and CFO $\delta_\mathrm{f}$. For synchronous communication, both $\delta_\mathrm{t}$ and $\delta_\mathrm{f}$ can be set to 0.\\
$I_{\rm D_{\rm N}}$ is the interference from DUEs in the neighbouring cells and is given by
\begin{equation}\label{eq:Ind}
I_{\rm D_{\rm N}} = \sum_{n \in \mathcal{N}} \sum_{d \in \mathcal{D}_n} \sum_{r \in \mathcal{R}} P_{\rm D} h_{d_nB}^r \Omega_{\textrm{wf}_{d_n}\rightarrow\textrm{wf}_{j_o}}(|r - k|, \delta_\mathrm{t}, \delta_\mathrm{f}),
\end{equation}
which is defined in a similar fashion to Eq. (\ref{eq:Inc}), where $\mathcal{D}_n$ represents the set of DUEs in the $n^{\rm th}$ neighbouring cell, and $P_{\rm D}$ is the transmit power of DUE devices.
Finally, $I_{\rm D_{\rm S}}$ represents the interference from DUEs in the same cell, i.e. the central cell, and is formulated in a similar fashion to Eq. (\ref{eq:Ind}). 


The SINR of a DUE $d$ in the central cell $o$ operating on RB $r$ is given by:
\begin{equation}\label{eqn:due_sinr}
\gamma_{d_o}^r\ = \frac{P_{\rm D}h_{d_o}^r}{\sigma_w^2 + I_{\rm C_{\rm N}} + I_{\rm C_{\rm S}} + I_{\rm D_{\rm S}} + I_{\rm D_{\rm N}}},
\end{equation}
where $P_{\rm D}$ is the transmit power of the DUE devices, $h_{d_o}^r$ is the channel gain between the transmitter and receiver of the $d^{\rm th}$ DUE using RB $r$, and $\sigma_w^2$ is the AWGN variance. $I_{\rm C_{\rm S}}$ and $I_{\rm C_{\rm N}}$ represent the aggregate interference from CUEs in the same cell and neighbouring cells, respectively. $I_{\rm D_{\rm S}}$ and $I_{\rm D_{\rm N}}$ represent the aggregate interference from DUEs in the same cell and neighbouring cells respectively. The expressions for each of the above aggregate interference terms are similar to Eqs. (\ref{eq:Inc}) and (\ref{eq:Ind}), with the channel gain $h$ considered between the interfering device and the DUE receiver.

%\subsubsection{Outage Probability}
%The outage probability of a device is the probability that its instantaneous SINR $\gamma$ is less than a threshold $\gamma_{\rm th}$
%\begin{equation}\label{eqn:outage}
%\Pr(\rm outage) = \Pr(\gamma < \gamma_{\rm th}).
%\end{equation}

\paragraph{Achieved Rate:}
We are also interested in the rate achieved by devices, after the bandwidth efficiency of each waveform has been taken into account. The rate of a device using a waveform $\rm wf$ can be calculated as
\begin{equation}\label{eqn:rate}
b = \rho_{\rm wf} B\log_2(1 + \gamma) [b/s],
\end{equation}
where $B$ is the bandwidth of an LTE resource block, and $\rho_{\rm wf}$ is the TSE of waveform $\rm wf$ that we have evaluated in section \ref{sec:5G_waveforms}. Here, we use the asymptotic value of their TSE when the time to transmit is infinite, which therefore does not encompass the low efficiency of linear FB-MC for short time frames. Furthermore, we consider a setup in which OFDM/OQAM, FBMC-PAM and f-OFDM only use $11$ subcarriers out of the $12$ that are available in a RB in order to absorb interference between two asynchronous users that would be transmitting on directly adjacent RBs. The value of $\rho_{\rm wf}$ achieved by each waveform is presented in Table \ref{tab:speceff}. Note that we exclude COQAM of this study to keep our results readable; furthermore, we have seen earlier in this thesis that COQAM achieves approximately the same results as GFDM.

\begin{table}[t]
\caption{Bandwidth Efficiency of Waveforms}
\begin{center}
\begin{tabular}{|c|c|}
\hline
\hline
\textbf{Waveform ($\rm wf$)} & \textbf{Bandwidth Efficiency ($\rho_{\rm wf}$)} \\ \hline \hline
CP-OFDM & $8/9$ \\ \hline
FMT & $8/9$ \\ \hline
GFDM &  $5/(5 + 1/8)$ \\ \hline
OFDM/OQAM & $11/12$ \\ \hline
FBMC-PAM & $11/12$ \\ \hline
f-OFDM & $8/9*11/12$ \\ \hline
UFMC & $8/9$ \\ \hline \hline
\end{tabular}
\end{center}
\label{tab:speceff}
\end{table}

\begin{figure}[t]
	\includegraphics[width=\linewidth]{tables2}
	\caption[Representative examples of interference tables]{Representative examples of interference tables. Timing offset is expressed in proportion of $T+T_\text{CP}$ of a reference CP-OFDM configuration and CFO is expressed relative to $\Delta\text{F}$. Values lower than $-60$ dB appear in dark blue. \newline a) Organization of interference tables and CP-OFDM to CP-OFDM example. \newline b) OFDM/OQAM to CP-OFDM -- c) f-OFDM to CP-OFDM -- d) UFMC to CP-OFDM. \newline e) OFDM/OQAM to OFDM/OQAM  f) FBMC-PAM to FBMC-PAM -- g) GFDM to GFDM -- h) FMT to FMT -- i) UFMC to UFMC -- j) f-OFDM to f-OFDM.}
	\label{fig:interference_tables}
\end{figure}

\subsubsection{Interference Model}
To model interference between users using each of the waveforms we consider, we follow the EVM-based approach proposed in Chapter 4. %That is, we generate interference tables that measure the interference caused by a given waveform onto another one at a certain spectral distance and timing offset. 
However, whereas our previous analyses were based on interference tables with a spectral granularity of one subcarrier spacing, we base the present system-level analysis on interference tables that are defined at the RB level. This is necessary to be able to carry out system level studies, as resource allocation and other procedures of the upper layers operate with a minimum granularity of one RB. Besides, whereas most studies on asynchronous networks rely on average values of interference \cite{Medjahdi2010a,Pischella2016a}, we compute the interference between given pairs of waveforms for specific values of timing offset $\delta\mathrm{t}$. Moreover, we also take into account CFO. We therefore generate three-dimensional interference tables which give the interference value at a given RB distance for each possible value of the timing offset, $\delta\mathrm{t}$, and CFO, $\delta\mathrm{f}$. % which enables us to encompass the actual specific value of time asynchronism between two users. 
We consider that the timing offset and CFO between users are uniformly distributed in a given interval so that $\delta\mathrm{t} \sim U_{[-\delta\mathrm{t}^\mathrm{max},\delta\mathrm{t}^\mathrm{max}]}$ and $\delta\mathrm{f} \sim U_{[-\delta\mathrm{f}^\mathrm{max},\delta\mathrm{f}^\mathrm{max}]}$. %This allows us to model the relative synchronism achieved by a network comprised of cellular and spatially clustered D2D users. To the best of our knowledge, no work available in the literature tackles inter-user interference caused by asynchronous coexistence with the same level of detail and for so many different waveforms.

In order to present our interference model, we display in Fig.~\ref{fig:interference_tables} some of the interference tables that are available in our system-level simulator. In particular, we present in Fig.~\ref{fig:interference_tables}-a the structure of the interference tables as they are represented in our system level simulator. For each value of $\delta_\mathrm{t}$ and $\delta_\mathrm{f}$, our tables provide the corresponding level of interference, up to a maximum spectral distance of $100$ RBs. Note that we consider heterogeneous scenarios in which DUEs use an alternative waveform and CUEs employ OFDM, and more advanced homogeneous scenarios in which both CUEs and DUEs use an alternative waveform. To model the interference between different users in these different set-ups, we therefore need to generate homogeneous interference tables, from a given waveform to the same waveform, and heterogeneous ones, from a given waveform to OFDM and from OFDM to a given waveform.

In Fig.~\ref{fig:interference_tables}, for each table \textit{Waveform A} to \textit{Waveform B}, an interfering user using \textit{Waveform A} is active on an RB of index 0, and we show the interference power seen by a victim user using \textit{Waveform B} at a given spectral distance specified in number of RBs, and for given values of the timing offset $\delta_\mathrm{t}$. Note that, due to space limitations, we present interference tables only in the case where there is no CFO, i.e. $\delta_\mathrm{f} = 0$, and only for spectral distances lower than $25$ RBs. 

Fig.~ \ref{fig:interference_tables}-b shows the interference table from OFDM/OQAM to CP-OFDM. Consistent with the results presented in Chapter \ref{chapter:3}, we show that the interference to the CP-OFDM receiver decreases very slowly in frequency and does not exhibit any particular behaviour related to the timing offset. As presented earlier in this thesis, this twofold observation holds true for interference from any FB-MC waveform to a CP-OFDM receiver or from a CP-OFDM transmitter to any FB-MC receiver. However, for waveforms that are filtered per block of subcarriers, such as f-OFDM and UFMC, the interference experienced by a CP-OFDM receiver achieves a minimum point for specific values of the timing offset, as can be seen in Fig.~\ref{fig:interference_tables}-c and Fig.~\ref{fig:interference_tables}-d, respectively. This is due to the similarity of these waveforms to the CP-OFDM scheme. Note that this observation is consistent with the analysis of Ahmed \textit{et al.}  in \cite{Ahmed2016}.

More diverse and interesting behaviours can be observed in homogeneous cases; as is commonly known, coexisting CP-OFDM systems will not interfere with each other provided that they are synchronized within the CP duration, which is verified in Fig.~\ref{fig:interference_tables}-a for $\delta_\mathrm{f} = 0$. This statement also applies almost directly to GFDM systems, which achieve quasi-orthogonality even if the timing offset is contained within the CP duration, as can be seen in Fig.~\ref{fig:interference_tables}-g. For both CP-OFDM and GFDM systems, interference between users dramatically increases if their relative asynchronism goes beyond the cyclic prefix. On the other hand, linear FB-MC waveforms (Fig.~\ref{fig:interference_tables}-e,f,h) achieve good coexistence capabilities, with the injected interference dropping rapidly along the frequency axis irrespective of the timing offset value. As we can see in Fig.~\ref{fig:interference_tables}-e, the best performance is achieved by the OFDM/OQAM waveform, as almost no interference leaks onto adjacent resource blocks. 

f-OFDM (see Fig.~\ref{fig:interference_tables}-j) exhibits similar behaviour owing to its filtering at both the transmitter and the receiver. In contrast, UFMC (Fig.~\ref{fig:interference_tables}-h) only achieves good containment for timing offset values that are lower than the duration of the used filter. Outside of this interval, interference rapidly increases, which is consistent with what we observed in Fig.~\ref{fig:TO_CFO}. This is because the particular implementation of UFMC considered here does not involve any windowing at the receiver. We highlight that the performance of certain waveforms, in particular GFDM and UFMC, could be improved by the use of additive windowing techniques at the transmitter and/or the receiver side. However, we intentionally do not implement these techniques in order to keep the focus on the intrinsic filtering properties offered by the waveforms under study. This is important as every additional filtering operation increases the overall complexity of the system. 

 