% Simulation set-up, params and general description
In the following, we perform simulations to investigate the co-existence of OFDM/OQAM and CP-OFDM, presenting a broad set of results. Cellular users are uniformly distributed over the coverage area of the encompassing OFDMA cell. In the clustered scenario, the cluster centre is chosen according to a uniform distribution within the cell area and D2D pairs are uniformly distributed within the cluster area. Fig.~\ref{fig:example_scenario} illustrates an example of a clustered scenario with 10 D2D pairs. 



\begin{figure}[t]
  \centering
    \includegraphics[width=0.8\linewidth,clip,keepaspectratio]{example_scenario_2.eps}
    \squeezeupsmall
    \caption{Example of clustered scenario consisting of 10 D2D pairs.}
    \label{fig:example_scenario}
\end{figure}



Table \ref{tab:sim_params} describes the key simulation parameters. After distributing both the CUs and D2D pairs within the cell, we then perform RB assignment and power-loading as described previously. The average rate per D2D pair is used as the main output metric from simulations. This metric is calculated for two different cases using the SINR expressions described in the previous section:
\begin{enumerate}
\item Case 1: D2D pairs use CP-OFDM, CUs use CP-OFDM.
\item Case 2: D2D pairs use OFDM/OQAM, CUs use CP-OFDM.
\end{enumerate}
In both cases, we compare the predicted average rate per D2D pair calculated using  $\gamma_{jm}^\prime$ (which does not take into account inter-D2D interference), with the actual average rate per D2D pair calculated using  $\gamma_{jm}$ (which takes into account inter-D2D interference). 

\bgroup
\def\arraystretch{1.3}
\begin{table}[t]
\caption{Simulation parameters}
\begin{center}
\begin{tabular}{|c|c|}
\hline
\hline
\textbf{Parameter} & \textbf{Value} \\ \hline \hline
inter-site distance (ISD) & 500 m \\ \hline
macro-cell radius & 250 m \\ \hline
carrier frequency & 700 MHz \\ \hline
subcarrier spacing & 15 kHz \\ \hline
number of RBs & {15, 25} \\ \hline
number of CUs & {15, 25} \\ \hline
%number of D2D pairs & [5 - 15] \\ \hline
scenario type & clustered or non-clustered \\ \hline
maximum cluster radius & ISD/5 m \\ \hline
minimum cluster radius & ISD/10 m \\ \hline
maximum D2D Tx. Rx. distance & (cluster radius) $\times$ 2/3 \\ \hline
pathloss model & WINNER II scenario B1 \\ \hline
CU minimum SINR & 10 dB \\ \hline
noise power per subcarrier \tablefootnote{Noise power per subcarrier is calculated using the expression ${-174}\textrm{dBm/Hz}$$ + 10\log_{10}(15\textrm{kHz})$, where ${-174}\textrm{dBm/Hz}$ is the background noise and $15\textrm{kHz}$ is the LTE subcarrier spacing.} ($\sigma_\nu^2$) & -127 dBm \\ \hline
maximum transmit power & 24 dBm \\ \hline
number of iterations & 40000 \\ \hline \hline
\end{tabular}
\end{center}
\label{tab:sim_params}
\squeezeup
\end{table}
\egroup


%We present a broad collection of results, divided into two sets. In the initial set of results presented, we produce cumulative distribution functions (CDF) for the average rate per D2D pair in order to demonstrate the effect that inter-D2D interference has on the D2D rate in a clustered scenario, and the potential of FBMC to mitigate this interference. In the succeeding set of results, we present the performance of FBMC and OFDM for various scenario set-ups by altering the simulation parameters of interest.

\vspace{-1.5mm}
% basic results

\begin{figure}[h!]
  \centering
    \subfloat[]{\includegraphics[width=0.8\linewidth,clip,keepaspectratio]{basic_2.eps}\label{fig:basic_2}}\\\vspace{-0.3cm}
    \subfloat[]{\includegraphics[width=0.8\linewidth,clip,keepaspectratio]{basic_4.eps}\label{fig:basic_4}}
    \squeezeupsmall
    \caption{(a) Clustered and (b) non-clustered scenario consisting of 10 D2D pairs.}
    \vspace{-2mm}
\end{figure}

\paragraph{Effects of inter-D2D interference for both OFDM/OQAM and CP-OFDM:}
\vspace{-1.0mm}
In the first set of results, we generate the empirical cumulative distribution function (CDF) for the average rate per D2D pair for both the clustered (Fig.~\ref{fig:basic_2}) and non-clustered (Fig.~\ref{fig:basic_4}) scenarios in order to demonstrate the effects of inter-D2D interference for both OFDM/OQAM and OFDM.

In the clustered scenario in Fig.~\ref{fig:basic_2}, we observe that when inter-D2D interference is not taken into account when calculating the rate (which is not representative of reality), there appears to be very little advantage to using OFDM/OQAM over CP-OFDM. OFDM/OQAM does demonstrate some negligible improvement over CP-OFDM due to the fact that it receives less interference from CUs, since the projected interference from CP-OFDM to OFDM/OQAM is slightly less than that from CP-OFDM to CP-OFDM, as shown in Fig.~\ref{fig:interf_tables}. However, the actual average rate per D2D pair, accounting for inter-D2D interference, shows that OFDM/OQAM provides significant improvement over CP-OFDM. In fact, the average rate per D2D pair when using OFDM/OQAM is very similar to the case when inter-D2D interference is not considered. 

%In the non-clustered scenario in Fig.~\ref{fig:basic_4}, we make two observations in comparison with the clustered scenario. First of all, we note that the average rate per D2D pair is greater. This can be attributed to the simulation set-up. In the clustered scenario, we ensure all D2D transmitters are positioned within the macro-cell coverage area by restricting the maximum distance between the cluster centre and the base station to be equal to the macro-cell radius minus the cluster radius. The non-clustered scenario can be considered to be a special case of the clustered scenario, with a cluster centre at the BS and a cluster radius equal to the macro-cell radius. As a result, D2D pairs in the non-clustered scenario are, on average, farther from the BS. Since the interference imposed by D2D transmitters on CUs is observed at the BS, D2D pairs near the macro-cell edge can transmit using a higher power. Hence, the average D2D rate in the non-clustered scenario is higher. Secondly, we observe that the advantage of using FBMC, even when inter-D2D interference is taken into account, is less than the corresponding clustered scenario. Again, this is readily understood, as D2D pairs are farther apart in the non-clustered scenario and, hence, inter-D2D interference does not play such a significant role.

In the non-clustered scenario in Fig.~\ref{fig:basic_4}, we observe that the advantage of using OFDM/OQAM, even when inter-D2D interference is taken into account, is less than the corresponding clustered scenario. This is intuitive, as D2D pairs are farther apart in the non-clustered scenario and, hence, inter-D2D interference does not play such a significant role. 

Hence, we make two observations. First of all, inter-D2D interference plays a significant role in clustered D2D underlay communication. 
Second, we observe that when OFDM/OQAM is used, the actual values of achieved average rate per D2D pair are very close to those calculated without taking inter-D2D interference into account. Thus, OFDM/OQAM successfully mitigates inter-D2D interference. Conversely, when CP-OFDM is used, the gap between the actual and predicted values of rate (calculated using $\gamma_{jm}$ and $\gamma_{jm}'$ respectively) is significantly larger. Therefore, Fig.~\ref{fig:basic_2} and Fig.~\ref{fig:basic_4} show that permitting D2D users to use OFDM/OQAM can significantly facilitate the resource allocation process in the considered scenarios.
%Second, we can mitigate this interference, without requiring an increase in the complexity of RA, by permitting the coexistence of FBMC and OFDM. 

\begin{figure}[t]
  \centering
    \includegraphics[width=0.9\linewidth,clip,keepaspectratio]{basic_52.eps}
    \vspace{-2mm}
    \caption{Average rate per D2D pair for different numbers of D2D pairs.}
    \label{fig:basic_5}
    \vspace{-3mm}
\end{figure}

Fig.~\ref{fig:basic_5} reinforces our observations. We display the actual average rate per D2D pair, calculated using $\gamma_{jm}$ when inter-D2D interference is considered, in a clustered scenario for 5 D2D pairs and 15 D2D pairs. We first observe that the average rate for both CP-OFDM and OFDM/OQAM decreases as the number of D2D pairs increases, since each D2D transmitter must now use a lower transmit power in order to satisfy the interference constraint specified by (\ref{p1:d}). We also observe that the benefit attributed to using OFDM/OQAM increases as the number of D2D pairs increases, i.e. the gap between the OFDM/OQAM and CP-OFDM curves grows larger as the number of D2D pairs increases. This is due to inter-D2D interference becoming more significant as the number of D2D pairs is increased, despite the fact that pairs must now use less power.

\vspace{-1.5mm}
% results from varying parameters
\paragraph{OFDM/OQAM and CP-OFDM performance in varying scenario set-ups:}
\vspace{-1.0mm}
In this subsection, we examine the performance of OFDM/OQAM and CP-OFDM as we vary several parameters of the simulation. We are interested in determining the scenarios that are best suited to the adoption of OFDM/OQAM, i.e. in which OFDM/OQAM offers a significant advantage. We first examine the effect that cluster density has on the relative performance of both waveforms. Cluster density can be varied in two manners: i) the cluster radius can be held constant and the number of D2D pairs in the cluster can be altered, or ii) the number of users can be fixed and the cluster radius can be altered.
We point out that a small cluster with few users and a large cluster with many users may have similar densities but represent two different types of scenario. Hence, we investigate the effects of density for both of the aforementioned cases. We also investigate the effect that the distance from the cluster centre to the BS has on performance.

\begin{figure}[t]
  \centering
    \includegraphics[width=0.9\linewidth,clip,keepaspectratio]{cluster_density2.eps}
    \vspace{-2mm}
    \caption{Average rate per D2D pair versus number of D2D users in a cluster of fixed radius of 70m.}
    \label{fig:cluster_density}
    \vspace{-2mm}
\end{figure}



Fig.~\ref{fig:cluster_density} shows the effect of varying the density of a cluster by fixing the cluster radius at 70 m and varying the number of D2D pairs in the cluster. OFDM/OQAM only seems to offer a small improvement in comparison with CP-OFDM when the rate is calculated without consideration for inter-D2D interference. However, it is the improvement that OFDM/OQAM offers over the actual D2D rate, which takes into account inter-D2D interference, that interests us. As the cluster density is increased by adding additional D2D pairs, the throughput gain that OFDM/OQAM offers over OFDM also increases. OFDM/OQAM is, therefore, most effective in dense clusters, consisting of many users, in which inter-D2D interference has a significant impact on the SINR of D2D devices. 
%We also note that the achieved rates are typically lower than those presented in the first set of results in the previous section. This is due to the fact that the cluster radius is fixed at 100 m for this simulation, which is the maximum allowed cluster radius in the simulations carried out in the previous section. Average rates are lower for larger cluster sizes as the pathloss between a D2D transmitter and receiver is also increased on average. 

\begin{figure}[t]
  \centering
    \includegraphics[width=0.9\linewidth,clip,keepaspectratio]{cluster_radius2.eps}
    \vspace{-2mm}
    \caption{Average rate per D2D pair versus cluster radius for a fixed number of D2D pairs.}
    \label{fig:cluster_radius}
    \vspace{-5mm}
\end{figure}



Fig.~\ref{fig:cluster_radius} demonstrates the effect of cluster size on the average rate per D2D pair. In effect, we are varying the cluster density by fixing the number of D2D pairs, while changing the cluster radius. In order to achieve greater densities, we use a scenario consisting of 25 RBs, 25 CUs, and 20 D2D pairs. As the cluster radius is decreased, we observe that the D2D pairs are able to achieve higher rates as the pathloss between a D2D transmitter and receiver also decreases. We also note that the inter-D2D interference becomes more significant, as is shown by the increasing gap between the curve representing the rate achieved using CP-OFDM when inter-D2D is taken into account, and the rate achieved using OFDM/OQAM. At a cluster radius of 40 meters, the benefit obtained from using OFDM/OQAM is significant. Based on the results presented in Figs.~\ref{fig:cluster_density} and \ref{fig:cluster_radius}, we conclude that OFDM/OQAM is best suited to scenarios consisting of small, dense D2D clusters.

\begin{figure}[t]
  \centering
    \includegraphics[width=0.9\linewidth,clip,keepaspectratio]{cluster_distance2.eps}
    \vspace{-2mm}
    \caption{Average rate per D2D pair versus distance from cluster centre to BS.}
    \label{fig:cluster_distance}
    \vspace{-4mm}
\end{figure}

Fig.~\ref{fig:cluster_distance} demonstrates the effect of varying the distance from the cluster centre to the BS. We make three observations with respect to Fig.~\ref{fig:cluster_distance}. First, we note that the average D2D rate increases as the cluster distance from the BS increases. Indeed, since the interference imposed by D2D pairs onto CUs is observed at the BS, D2D transmitters belonging to clusters that are at a greater distance from the BS are permitted to use higher transmit powers. Second, we note that the benefit of using OFDM/OQAM is greater when the cluster is near the cell edge. This can be attributed to the fact that inter-D2D interference increases, and hence becomes more significant, when D2D pairs are permitted to transmit using higher power values. Finally, we note that the average rate per D2D user increases less rapidly as the distance from the BS increases, particularly after a distance of 100 m. This is as a result of increased interference from cell-edge CUs, which compensate their pathloss to the BS by using higher transmit powers. It should be pointed out that these results were obtained for a single cell scenario. In a multi-cell scenario, we would expect clusters located near the macro-cell edge to experience increased interference from neighbouring cells.