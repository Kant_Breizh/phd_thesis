% Simulation set-up, params and general description
Fig. \ref{fig:example_scenario} shows an example of a typical simulation scenario. Note, we assume that each macro-cell is fully loaded and hence consider a large number of CUEs per square kilometre to ensure this.  



\begin{figure}[t]
  \centering
    \includegraphics[width=3.5in,height=3.5in,clip,keepaspectratio]{example_scenario.eps}
    \caption[Example scenario consisting of 19 cells]{Example scenario consisting of 19 cells, with each region coloured according to the spectral resources permitted for use. Each \textit{x} represents an ordinary cellular user, whereas IUEs involved in direct communication are clustered at factory locations.}
    \label{fig:example_scenario}
\end{figure}



After distributing both the CUEs and IUE pairs across the network, we perform RB assignment and power allocation as described in Section \ref{section:system_model}. The SINR for each CUE and IUE in the centre cell is calculated according to Eqs. \ref{eqn:cue_sinr} and \ref{eqn:due_sinr}, respectively. Using the SINR, we can then calculate the outage probability (Eq. \ref{eqn:outage}) in the centre cell, as well as the achieved rate (Eq. \ref{eqn:rate}) for both CUEs and IUEs.

A simulation-based work such as this allows us to assess the performance of a realistic system with a high degree of conformity, without sacrificing realism through assumptions to obtain tractability. Nevertheless, simulating the network for every possible combination of parameters would be impractical and unnecessary. Hence, we first present detailed results for a base-case using a set of parameters that is representative of a typical scenario. The base-case parameters are listed in Table \ref{tab:sim_params}.

We note that performance will not be uniform across the entire parameter space, and wish to convey this in the results presented in the following section. Most importantly, we wish to avoid presenting results that are only representative of a subset of the parameter space. Accordingly, after presenting the base-case, we then examine how performance changes depending on the scenario by varying a key parameter of interest while maintaining the other parameters as per their value in the base-case.

For each of the candidate waveforms, we investigate two scenarios:
\begin{enumerate}
\item Case 1: IUE pairs use candidate waveform, CUEs use OFDM.
\item Case 2: Both IUE pairs and CUEs use candidate waveform.
\end{enumerate}
We also examine the effects of the timing offset on the relative performance of waveforms, ranging from perfectly synchronised to fully asynchronous communication. Scenarios whereby IUEs achieve coarse synchronization with a stated maximum permitted timing offset are of interest, and accordingly investigated for different ranges of maximum permitted offsets.

\bgroup
\def\arraystretch{1.3}
\begin{table}[t]
\caption{Simulation parameters}
\begin{center}
\begin{tabular}{|c|c|}
\hline
\hline
\textbf{Parameter} & \textbf{Value} \\ \hline \hline
Cell Radius & 500 m\\ \hline
Inner Radius & 325 m\\ \hline
Number of Cells & 19 \\ \hline
CUEs Per Square Km & 200 \\ \hline
IUEs Per Cluster & 30 \\ \hline
Clusters Per Square Km & 3 \\ \hline
Average Cluster Radius & 60 m\\ \hline
Average Tx Rx Distance & $U_{[10, 50]}$ m\\ \hline
Carrier Frequency & 2 GHz \\ \hline
Subcarrier Spacing & 15 kHz \\ \hline
Noise Per RB \tablefootnote{Noise power per RB is calculated using the expression ${-174}\textrm{dBm/Hz}$$ + 10\log_{10}(180\textrm{kHz})$, where ${-174}\textrm{dBm/Hz}$ is the background noise and $180\textrm{kHz}$ is the bandwidth of an LTE RB.} ($\sigma_\nu^2$) &  -116 dBm \\ \hline
Number RBs in system & 50 \\ \hline
PPusch & -96 dBm \\ \hline
Alpha & 1 \\ \hline
Max Tx Power CUE & 24 dBm \\ \hline
Max Tx Power IUE & 0 dBm \\ \hline
Max Timing Offset & 1 \\ \hline
Waveforms & \makecell{OFDM, FMT, FBMC/OQAM, \\FBMC-PAM, GFDM, f-OFDM, UFMC} \\ \hline 
\end{tabular}
\end{center}
\label{tab:sim_params}
\end{table}
\egroup