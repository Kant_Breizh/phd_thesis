%!TEX root = ../../thesis.tex
\subsubsection{Interference Model}

The main measure that we base our analysis upon is the SINR experienced by incumbent CUs and D2D pairs. To rate the latter with accuracy, it is necessary to use models of interference that properly estimate the leakage that two asynchronous users inject onto each other. As mentioned in chapters 2 and 3 of this thesis, to the best of our knowledge, most studies on D2D underlay operation do not consider leakage between adjacent frequency resource blocks. On the other hand, papers that do consider leakage, as in \cite{Xing2014}, rely on the Power Spectral Density (PSD)-based model, the shortcomings of which we have demonstrated in Chapter \ref{chapter:3}.

\begin{figure}[t]
\centering
	\includegraphics[width=0.7\linewidth]{interf_tables_2}
	\caption{Interference tables measuring the value of interference injected between different couples of waveforms according to Chapter \ref{chapter:3} and \cite{Medjahdi2010a}.}
	\label{fig:interf_tables}
\end{figure}

Note that we have extensively analyzed and precisely modeled the leakage between asynchronous users operating on different parts of the spectrum band, and derived interference tables that we will build our analysis upon. %Fu, we draw upon the work of \cite{Medjahdi2010a} to rate the interference from FBMC/OQAM to FBMC/OQAM users, or OFDM to OFDM users. On the other hand, we rate the interference from OFDM to FBMC/OQAM and from FBMC/OQAM to OFDM according to the recent works of \cite{Bodinier20165G} and \cite{Bodinier2016ICT}. 
Our previous developments in Chapter \ref{chapter:3} allow us to rate the value of $I\{\text{A}\rightarrow\text{B}\}(l)$, which corresponds to the interference injected by a subcarrier of waveform A to a subcarrier of waveform B at spectral distance $l$.

In this analysis, we use the interference table shown in Fig.~\ref{fig:interf_tables}.
This figure shows that the use of OFDM/OQAM for D2D operation will only marginally reduce the interference between cellular and D2D users, as $I\{\text{OFDM/OQAM}\rightarrow\text{CP-OFDM}\}$ is only slightly less than $I\{\text{CP-OFDM}\rightarrow\text{CP-OFDM}\}$. This has been thoroughly explained in chapter 4. However, the interference between asynchronous D2D users will be drastically reduced if they use OFDM/OQAM instead of OFDM, since $I\{\text{OFDM/OQAM}\rightarrow\text{OFDM/OQAM}\}$ is considerably lower than $I\{\text{CP-OFDM}\rightarrow\text{CP-OFDM}\}$ . 

\subsubsection{Optimization Problem Formation}
We wish to improve the performance of the cellular network using underlay D2D communication. A D2D pair is allowed to transmit when the interference introduced on the incumbent network does not prevent the incumbent CUs from satisfying their minimum SINR constraints. The D2D transmissions affect the SINR experienced at the BS and hence the CUs suffer from adjacent channel interference. The SINR of the CU indexed by $i$ can therefore be expressed as
\begin{equation}
\gamma_i\ =\ \frac{P_ih_{iB}}{\sigma_w^2 + \sum_{j \in \mathcal{D}} \sum_{r \in \rm \mathcal{R}} \sum_{m \in b_r} \omega_{jr} h_{jB} \Omega_{mi}^{\rm D}},
\end{equation}
where $P_i$ is the transmit power of the $i^{\rm th}$ CU, $h_{iB}$ is the channel gain between the $i^{\rm th}$ CU and the BS, $\sigma_w^2$ is the AWGN variance, $r$ indexes the resource blocks (RB) across the entire band, $m$ indexes the subcarriers in a particular RB band $b_r$, $\omega_{jr}$ is a resource reuse indicator where $\omega_{jr} = 1$ when D2D pair $j$ reuses RB $r$, and $\omega_{jr} = 0$ otherwise. Finally, $\Omega_{mi}^{\rm D}$ is the interference introduced by the $m^{\rm th}$ subcarrier onto the RB used by the $i^{\rm th}$ CU. $\Omega_{mi}^D$ is given by
\begin{equation} \label{omega_1}
\Omega_{mi}^{\rm D}\ =\ \sum_{k \in b_i} \frac{P_{m}}{P_0} I(k-m),
\end{equation}
where $k$ indexes the subcarriers in the incumbent band $b_i$ used by user $i$, and $I(k-m)$ is the appropriate interference table $I\{\text{A}\rightarrow\text{B}\}$ in Fig.2, depending on the waveform being used by the D2D pairs. 

A D2D receiver will experience two types of interference: i) interference from CUs, and ii) interference from other D2D pairs. The SINR experienced on subcarrier $m$ at the D2D receiver of pair $j$ is given by

\begin{equation} \label{d2dSINR}
\gamma_{jm}\ =\ \frac{P_{jm} h_j}{\sigma_w^2 + I_{\rm cu} + I_{\rm D2D}},
\end{equation}
where $P_{jm}$ is the power of the $j^{\rm th}$ D2D pair on subcarrier $m$. $I_{\rm cu}$ is the interference injected on the $m^{\rm th}$ subcarrier from cellular users using CP-OFDM in the incumbent band, and $I_{\rm D2D}$ is the interference from other D2D users.
$I_{\rm cu}$ is defined as

\begin{equation}
I_{\rm cu} = h_{ij} \sum_{i \in \mathcal{C}} \Omega_{im}^{\rm C},
\end{equation}
where $\Omega_{im}^{\rm C}$ is the interference introduced by the $i^{\rm th}$ CU onto the $m^{\rm th}$ subcarrier of D2D pair $j$, and is specified in a similar fashion to equation (\ref{omega_1}). %, is given by:
%\begin{equation}
%\Omega_{ij}^{\rm C}  =\ \sum_{l \in b_i} \sum_{r \in \rm R} \sum_{m \in b_r} \omega_{jr} \frac{P_l}{P_0}h_{ij}\max I_l^m(\delta_{\rm t},\delta_{\rm f})
%\end{equation}
%where $p_l$ is the power on the $l^{th}$ subcarrier occupied by the $i^{th}$ user and can be assumed equal to $P_i$ divided by $|b_i|$.
Finally, $I_{\rm D2D}$ is the interference from other D2D links given by
\begin{equation}\label{eq:id2d}
I_{\rm D2D} = \sum_{d \in \mathcal{D}, d \neq j} \sum_{r \in \rm R} \sum_{n \in b_r} \omega_{dr} h_{jd} \Omega_{nm}^{\rm D}\ ,
\end{equation}
where $\Omega_{nm}^{\rm D}$ is the interference injected by the $n^{\rm th}$ subcarrier of the $d^{\rm th}$ D2D user onto the $m^{\rm th}$ subcarrier of $j^{\rm th}$ D2D user.

Using the above SINR expressions, we can formulate the optimization problem $P1$, in which the objective is to maximize the sum rate of D2D pairs, subject to a minimum SINR constraint for each CU.

\begin{subequations}\label{first:main}
\begin{align}
{\rm P1:} &\max_{P_m, \omega_{jr}} \sum_{j \in \mathcal{D}} \sum_{r \in \mathcal{R}} \sum_{m \in b_r} \omega_{jr}  \log(1 + \gamma_{jm}) \tag{\ref{first:main}},
\intertext{subject to}
&\omega_{jr} \in \{0,1\}, \forall j,r \label{p1:a},\\
&\sum_{j \in \mathcal{D}} \omega_{jr} \leq 1, \forall r \in \mathcal{R} \label{p1:b},\\
&\sum_{r \in \mathcal{R}} \omega_{jr} = 1, \forall j \in \mathcal{D} \label{p1:c},\\
&\gamma_i \geq \textrm{SINR}_{\rm min}^{\rm C} , \forall i \in \mathcal{C} \label{p1:d},\\
& P_j = \sum_{m \in b_j} P_m < P_{\rm max}^{\rm D},
\end{align}
\end{subequations}
where $\textrm{SINR}_{\rm min}^{\rm C}$ is the minimum acceptable SINR that a CU must achieve.

Optimization problem P1 is a mixed integer non-linear programming (MINLP) problem from which it is difficult to obtain the solution directly. Accordingly, we split the optimization problem into two sub-problems. First, we perform RB assignment, which is a discrete optimization problem. Once RBs have been assigned, we perform power-loading for the D2D pairs. 

Even after splitting P1 into two simpler problems, solving them remains complicated due to the inclusion of inter-D2D interference. The main source of this complexity lies in the fact that $I_{\rm D2D}$ (\ref{eq:id2d}) is a function of the power assigned to each subcarrier of each D2D transmitter, as shown in (\ref{d2dSINR}). Furthermore, incorporating inter-D2D interference into the RA scheme would assume that every D2D pair is able to obtain information regarding the interference contribution from every other D2D pair. This is an unrealistic assumption, requiring an exchange of information between D2D pairs before any resource is assigned. We are instead motivated to develop alternative methods to mitigate inter-D2D interference other than through resource allocation (RA), namely through the use of OFDM/OQAM.
\nomenclature[A]{RA}{Resource Allocation}

Accordingly, in the following, we aim to demonstrate that if D2D pairs use OFDM/OQAM, then there is no significant performance loss incurred by performing RA and power loading without taking into account the inter-D2D interference. Hence, we do not consider the effects of inter-D2D interference when performing both RB assignment and power-loading. This greatly reduces the complexity of the RA schemes and ensures that the power-loading objective function is convex. It is also more realistic as it makes no assumptions regarding the information a D2D pair possesses about every other D2D pair in the cluster. Therefore, we consider a simplification of P1 (\ref{first:main}) where the SINR $\gamma_{jm}$ (\ref{d2dSINR}) is reduced to
\begin{equation}
\gamma_{jm}^\prime\ =\ \frac{P_m h_j}{\sigma_w^2 + I_{\rm cu}}.
\end{equation}


%For these two reasons, it is both much more realistic and practical from a network point-of-view to consider that the RA and power-loading will be decided without knowledge of the consequent inter-D2D interference. Therefore, we consider a simplification of P1 as defined in (6) where the SINR is reduced to (your expression in 10, this should be worked out a bit more).


Given the above simplifications, the two intermediate problems to be solved can be rewritten as follows.

% % % ----------------------------- RB Assignment ----------------------------- % % %
\paragraph{RB Assignment:}
We assume each cellular user is assigned a single RB and that there are as many CUs as RBs. Since we only consider pathloss in our channel model, RBs can be randomly assigned to CUs. We then want to assign one RB to each D2D pair such that the interference experienced by each D2D pair from the CUs is minimized. The interference experienced by D2D pair $j$ on RB $r$ is given by
\begin{equation} \label{I_1}
I_{jr}\ =\ \sum_{i \in \mathcal{C}} \sum_{m \in b_i} \sum_{k \in b_r} \frac{P_{m}}{P_0}h_{ji} I(k-m).
\end{equation}
The RB assignment problem can be specified as follows
\begin{subequations}\label{first:main2}
\begin{align}
 {\rm P2:} &\min_{\omega_{jr}} \sum_{j \in \mathcal{D}}  \omega_{jr} \phi_{jr} \tag{\ref{first:main2}}, \\
 \intertext{subject to}
 &\omega_{jr} \in \{0,1\}, \forall j,r,\\
 &\sum_{j \in \mathcal{D}} \omega_{jr} \leq 1, \forall r \in \mathcal{R},\\
 &\sum_{r \in \mathcal{R}} \omega_{jr} = 1, \forall j \in \mathcal{D},
\end{align}
\end{subequations}
where $\phi_{jr}$ is the interference from CUs experienced by D2D pair $j$ on RB $r$. 
Problem P2 is a combinatorial optimization problem, made complicated by the fact that multiple D2D users may have the same optimal RB assignment. In line with the literature \cite{Feng2013,Han2014Bipartite,Wang2014Fast}, we utilize the well-known Kuhn-Munkres algorithm (commonly known as the Hungarian method), to solve the uplink resource assignment problem for D2D pairs.

\nomenclature[A]{RB}{Resource Block}
% % % ----------------------------- Power Loading ----------------------------- % % %
\paragraph{Power-loading:}
Having assigned a RB to each D2D pair, power-loading can now be performed. The power-loading optimization problem is similar to P1, with the discrete constraints (\ref{p1:a}-\ref{p1:c}), which relate to RB assignment, removed. Since RB assignment has already been performed, the objective function of optimization P1, i.e., equation (\ref{first:main}), can be simplified as follows
\begin{equation} \label{p2_2}
\max_{P_m} \sum_{j \in \mathcal{D}} \sum_{m \in b_j}  \log(1 + \gamma_{jm}^\prime).
\end{equation}
%As previously outlined, we also exclude the effects of inter-D2D interference during power-loading, simplifying $\gamma_{jm}$ (\ref{d2dSINR}) to the following form
%\begin{equation}
%\gamma_{jm}^\prime\ =\ \frac{P_m h_j}{\sigma_\nu^2 + I_{\rm cu}}.
%\end{equation}

The resulting problem is clearly convex and similar to others in the literature, for example \cite{shaat2010computationally}. The solution can be readily obtained using an appropriate software package.