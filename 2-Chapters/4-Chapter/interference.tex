To assign the optimal power distribution to the D2D subcarriers, it is vital to know how much power leaks to the adjacent band depending on which waveform is utilized by the D2D pair. The classical way to compute the leakage is to integrate the \ac{PSD} of the interfering signal on the band that suffers from the interference. However, this model does not take into account the time window of the incumbent. This is of paramount importance as the incumbent only considers a time window with a specific width based on its own parameters. Besides, it has been shown in \cite{Medjahdi2011} that PSD is not a suitable measure to evaluate the inter-system interference for the scenario of interest to this paper. We therefore employ the instantaneous interference model proposed in \cite{Medjahdi2011} to compute the interference $I_m^l$ injected to the $l$th subcarrier of the incumbent by the D2D signal $x_m$ where only subcarrier $m$ is utilized. This allows us to scrutinize the interference injected by each individual subcarrier to the incumbent network. Note that the same subcarrier spacing is used for both the D2D pair and the incumbent network. Therefore, we have
\begin{equation}
\label{eq1}
I_m^l = \int_{t=0}^T\left|\left(g_r^l*x_m\right)(t)\right|^2 dt,
\end{equation}
where $g_r^l$ is the receiver filter on subcarrier $l$ and $*$ denotes the convolution operation.
As the receiver suffering from interference is based on OFDM, in the discrete time domain, (\ref{eq1}) becomes 
\begin{equation}
\label{eq2}
I_m^l = \sum_{n=0}^{N-1}\frac{T_\text{s}}{M+N_\text{CP}}\sum_{k=n(M+N_\text{CP})+N_\text{CP}}^{(n+1)(M+N_\text{CP})}\left|x_m[k]e^{-j2\pi k\frac{l}{M}}\right|^2,
\end{equation}
where $N$ is the total number of OFDM symbols corresponding to the time span $T$, $N_\text{CP}$ is the number of \ac{CP} samples and $M$ the number of samples per OFDM symbol of length $T_\text{s}$. According to the signal models presented in the former section, for any of the analysed waveforms, $x_m$ can be written as 
\begin{equation}
\label{eq3}
x_m[k] = a_m[k]e^{j2\pi k\frac{m}{M}}, \forall k = 0\ldots N(M+N_\text{CP})-1,
\end{equation}
where $a_m$ encompasses the modulated signal on subcarrier $m$.
Therefore, putting (\ref{eq3}) in (\ref{eq2}) and taking into account timing and frequency offsets between the interferer and the receiver, we have
\begin{eqnarray}
\label{eq4}
 I_m^l(\delta_\text{t},\delta_\text{f}) &=& \sum_{n =0}^{N-1}\Bigg(\frac{T_\text{s}}{M+N_\text{CP}}\sum_{k=n(M+N_\text{CP})+N_\text{CP}}^{(n+1)(M+N_\text{CP})}\nonumber\\ & & \left|a_m[k]e^{j2\pi \left(k\frac{m-l+\delta_\text{f}}{M}+\frac{\delta_\text{t}}{M}\right)}\right|^2\Bigg).
\end{eqnarray}
 
Then, the mean interference seen at subcarrier $l$ at the receiver of the incumbent can be written as
\begin{equation}
\label{eq5}
I_{m_\text{mean}}^l(\delta_\text{t},\delta_\text{f}) = \frac{1}{N}\mathbb{E}_{d_m}(I_m^l(\delta_\text{t},\delta_\text{f})),
\end{equation}
where $\mathbb{E}_{d_m}$ represents the expectation with respect to the symbols transmitted on subcarrier $m$.
 We point out that when a larger number of subcarriers are utilized by the D2D pair, the total interference is equal to the sum of the interference caused by each subcarrier.

Finally, the total interference injected by the D2D transmission on the free band $B_\text{f}$ to the incumbent band $B_\text{i}$ is 
\begin{equation}
\label{eq6}
I_{B_\text{i}}^{B_\text{f}} = \sum_{m\in B_\text{f},\ l \in{B_\text{i}}} \frac{P_m}{P_0}I_{m_\text{mean}}^l(\delta_\text{t},\delta_\text{f}),
\end{equation}
where $P_m$ is the power assigned to the $m$th subcarrier and $P_0$ is a reference value of $1W$. This method can be used to compute the interference tables for the different analysed waveforms. 




