In this section, we present a broad set of numerical results analyzing the effects of time-frequency misalignment of the D2D pair and performance of the optimal power allocation scheme discussed in Section \ref{sec:capacity} for different waveforms. 

\subsection{System Setup}
We consider an incumbent system following similar parameters to 3GPP LTE standard. 
The OFDM cellular user occupies 180 subcarriers, which corresponds to 15 LTE resource blocks along the frequency axis. Besides, it uses $M = 180$ samples per symbol and $N_\text{CP} = 12$ samples. The length of the transmission window in time varies from $N_\text{f}=1$ to $100$ OFDM symbols. In the center of the incumbent band, a free band $B_\text{f}$ divided into $M_\text{f}$ subcarriers of $15$ kHz is unused. This band is utilized by the D2D pair. No guard band is considered in this study. The parameters of each waveform under study for utilization by the D2D pair are listed in Table \ref{tab:phy}. RRC refers to the Root Raised Cosine filter.

\subsection{Computation of Interference Tables}
In this subsection, we compute the interference caused by the D2D transmission according to (\ref{eq5}). Fig.~\ref{fig:MSEvsTau} illustrates the interference injected by one active subcarrier on the incumbent band as a function of $\delta_\text{t}$ when $\delta_\text{f} = 0$. We notice that OFDM based D2D transmission does not interfere at all if the timing offset is contained within the CP. However, when $\delta_\text{t}$ falls outside the CP, there is a big increase in the amount of interference to the incumbent band. On the other hand, the interference caused by other waveforms does not have a high variation with respect to $\delta_\text{t}$. This result reveals that it may not be worth synchronising D2D transmission in time with the cellular users when different waveforms are utilised by the D2D pair.

\begin{figure}
	\centering
	\vspace{-10pt}
	\includegraphics[width=0.85\linewidth]{IvsDeltaT.eps}
	\caption{Total injected interference on the incumbent band as a function of $\delta_\text{t}$. Timing offset has limited impact on injected interference except in the case where D2D transmitters use OFDM.}
	\label{fig:MSEvsTau}
	\vspace{-15pt}
\end{figure}

\begin{figure}
	\begin{center}\vspace{-10pt}
		\subfloat[OFDM]{
			\includegraphics[width=\linewidth]{OFDM.eps}
			\label{I_OFDM}\vspace{-15pt}
		}\\\vspace{-10pt}
		\subfloat[FMT]{\vspace{-10pt}
			\includegraphics[width=\linewidth]{FMT.eps}
			\label{I_FMT}\vspace{-15pt}
		}\\\vspace{-10pt}
		\subfloat[OFDM/OQAM]{\vspace{-10pt}
			\includegraphics[width=\linewidth]{OQAM.eps}
			\label{I_OQAM}\vspace{-15pt}
		}\\\vspace{-10pt}
		\subfloat[Lapped FBMC]{\vspace{-10pt}
			\includegraphics[width=\linewidth]{Lapped.eps}
			\label{I_Lapped}\vspace{-15pt}
		}\\\vspace{-10pt}
		\subfloat[GFDM]{\vspace{-10pt}
			\includegraphics[width=\linewidth]{GFDM.eps}
			\label{I_GFDM}\vspace{-15pt}
		}
		\caption{Interference in dB caused by an active D2D at subcarrier 0 on 20 neighboring OFDM subcarriers of the incumbent in function of $\delta_\text{t}$ for different waveforms. Only OFDM shows a figure significantly varying along the $\delta_\text{t}$ axis. For the other studied waveforms, perfect time synchroization does not significantly reduce injected interference.}
		\label{fig:IperSub}
		\vspace{-10pt}
	\end{center}
\end{figure}

Fig.~\ref{fig:IperSub} shows the interference that is injected  by an active subcarrier with unitary power on 20 neighboring OFDM subcarriers as a function of $\delta_\text{t}$ for different waveforms. Interference caused by Lapped FBMC and OFDM/OQAM is weakly affected by the timing offset. GFDM and FMT show slight variations with respect to $\delta_\text{t}$. On the contrary, the figure for OFDM shows high variations along the $\delta_\text{t}$ axis. 

\begin{figure}
	\begin{center}
		\subfloat[Mean Interference Table on OFDM receiver]{
			\includegraphics[width=0.85\linewidth]{meanmsecomp.eps}
			\label{fig:meanItable}}
		\\\vspace{-10pt}
		\subfloat[Maximum Interference Table on OFDM receiver]{
			\includegraphics[width=0.85\linewidth]{maxmsecomp.eps}
			\label{fig:maxItable}}
	\end{center}
	\caption{Maximum and Mean interference seen by a subcarrier of OFDM receiver as a function of its distance to an active 5G waveform subcarrier.}
	\vspace{-15pt}
	\label{fig:tables}
\end{figure}

In addition, we evaluate the interference that is caused by the different waveforms. We present the mean and maximum interference with respect to $\delta_\text{t}$ in Figs.~\ref{fig:meanItable} and \ref{fig:maxItable}. Our observations are threefold: First, it appears that OFDM is the only waveform that shows a significant difference between its mean and maximum injected interference on the OFDM based incumbent. Other waveforms show a difference of approximately 0 to 1dB. Second, if mean interference is considered, GFDM causes the highest interference. However, if the maximum interference is considered, OFDM based D2D pair has the worst performance. Third, we point out that values of interference injected by 5G waveforms are surprisingly high. For OFDM/OQAM for example, the PSD based model predicts an attenuation of $-60$ dB at subcarrier distance of 2 \cite{Medjahdi2011}, whereas our interference tables show that the interference power seen by an OFDM receiver at subcarrier distance of 2 is $-18.5$ dB. This is due to the fact that the OFDM demodulator performs \ac{FFT} on a time window that may be much shorter than the length of the symbol of the other waveform. Therefore the signal suffers from discontinuities that produce projections on the whole incumbent spectrum.

Finally, we point out that interference tables are presented for $\delta_\text{f} = 0$. However, as $\delta_\text{f}$ only acts as a frequency shift in ($\ref{eq5}$), when $\delta_\text{f} \neq 0$, the interference can be directly taken from interference tables by taking the corresponding subcarrier distance into account. As a case in point, if the subcarrier distance is $-2$ and $\delta_\text{f} = 1$, interference value corresponding to an actual subcarrier distance of $-1$ should be read from the table.

\subsection{Transmission Performance}
In this subsection, we consider the total amount of data that can be transmitted by the D2D pair during the transmission window as a measure to evaluate the performance of different waveforms. We use the interference tables derived in the previous subsection to calculate the total amount of data that can be transmitted during the transmission window based on the waveform that is utilised. We consider a transmission band consisting $M_{\text{f}} = 12$ free subcarriers, an interference constraint $I_\text{th}$ of either $1$ W or $1$ mW, and variable number of time symbols $N_\text{f} \in [1,100]$. Besides, 
the maximum frequency misalignment is one subcarrier spacing (i.e. $\delta_{\text{f}_\text{max}} = 1$). Note that, $\sigma_\text{N+I}^2$ is assigned a low value of $10^{-6}$ to keep this paper focused on the interference injected by D2D pairs onto cellular users. 
The amount of data transmitted by the D2D pair as a function of $N_\text{f}$ is obtained for two values of $I_\text{th}$ in Fig.~\ref{fig:bitsTrans}.

\begin{figure}
	\begin{center}
		\subfloat[$I_\text{th} = 1W$]{
			\includegraphics[width=0.85\linewidth]{Ith1_bits.eps}
			\label{b1W}
		}\\\vspace{-10pt}
		\subfloat[$I_\text{th} = 1mW$]{
			\includegraphics[width=0.85\linewidth]{Ith1m_bits.eps}
			\label{b1mW}
		}
	\end{center}
	\caption{Bits transmitted as a function of available OFDM time symbols in the transmission window}
	\label{fig:bitsTrans}
	\vspace{-15pt}
\end{figure}

The presented results bring insight into which waveform performs best for different time window lengths and interference constraint. It seems that for transmission windows shorter than 10 symbols, OFDM is the best choice, as it does not suffer from any transmission delay. Therefore, linearly pulse shaped waveforms can compete only when the transmission window starts getting wider than 10 OFDM symbols. It can be noticed that Lapped FBMC shows a promising performance. This is due to the fact that it has a very short delay about only one symbol, and injects interference comparable to that of OFDM/OQAM and FMT. FMT suffers from a large delay during transmission and seems not to be an appropriate candidate for low latency applications. However, OFDM/OQAM performance stays very close to the Lapped FBMC. Interestingly, the performance of OFDM starts to degrade for time windows of width larger than $N_{\text{f}}=15$ regardless of the interference constraint. This is the result of its spectral efficiency loss due to the presence of a CP. Note that, even though GFDM seems to be a potential competitor to OFDM/OQAM and Lapped FBMC when the interference constraint is very relaxed, it cannot efficiently cope with stringent interference constraints. This is the consequence of its high interference leakage as shown in Fig.~\ref{fig:tables}. 

\begin{figure}
	\centering
	\vspace{-10pt}
	\includegraphics[width=0.9\linewidth]{IthSweep.eps}
	\caption{Bits transmitted by different waveforms in 12 subcarriers during one TTI in function of the interference threshold.}
	\label{fig:TTI}
	\vspace{-15pt}
\end{figure}

Finally, we present results corresponding to a scenario where the time-frequency window is equal to 1 LTE Time Transmission Interval (TTI) and 12 subcarriers in frequency. This transmission window is indeed very short and hence waveforms with linear pulse shaping may suffer from the delay imposed by the transient of their transmit and receive filters.
Fig.~\ref{fig:TTI} depicts the performance of different waveforms as a function of the interference constraint. All the waveforms show a similar behaviour in which the number of transmitted bits saturate after a certain value of $I_\text{th}$. This corresponds to the point where the total power budget becomes the dominating factor to consider in (\ref{P2}). Furthermore, it can be seen once again that Lapped FBMC achieves the best performance as it offers a good trade-off between latency and interference. However, for $I_{\text{th}}\geq -10$ dB, OFDM achieves the best performance as the interference constraint is not restrictive anymore. It is worth mentioning that a specific number of symbols can be transmitted during a TTI corresponding to each waveform (see table~\ref{tab:nSymbols}).
In particular, GFDM performance is limited because it can only fit 10 symbols in the transmission window.