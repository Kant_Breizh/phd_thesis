The interference tables that are derived in the previous section can be used as input to a power optimization block to maximize the rate of D2D users.

\subsection{Power Optimization Problem}
In the scenario of interest to this paper, the D2D transmitter has to optimize its power allocation to maximize its rate while satisfying the total power budget constraint, $P_\text{t}$, and maximum injected interference $I_\text{th}$ to the incumbent band $B_\text{i}$. As shown in the previous section, interference injected onto cellular users depends on both the frequency and time misalignments of the D2D pair with respect to the incumbent network. However, it is assumed that the D2D pair is only aware of $\delta_\text{t}$ and $\delta_\text{f}$ ranges. $\delta_\text{t} \in \big[-0.5(\text{T}_\text{s}+\text{T}_\text{CP}),0.5(\text{T}_\text{s}+\text{T}_\text{CP})\big)$ and $\delta_\text{f} \in \big[-\delta_{f_\text{max}}, \delta_{f_\text{max}}\big]$ where $\delta_{f_\text{max}}$ is the maximum misalignment in frequency. 

In \cite{Medjahdi2011}, the mean value of interference caused by $\delta_\text{t}$ is considered. However, a more stringent policy is required to fully protect the cellular users, as the interference caused by the D2D pair may take higher values than its mean. Therefore, we propose to perform the power allocation considering the maximum value for the injected interference. Hence, we define the maximum interference factor from a given subcarrier, $m$, to the incumbent band as  
\begin{equation}
\label{interfactor}
\Omega_m = \sum_{l\in B_\text{i}} \frac{\underset{\delta_\text{f},\delta_\text{t}}{\text{max}}{I_{m_\text{mean}}^l(\delta_\text{t},\delta_\text{f})}}{P_0}.
\end{equation}

On this basis, we define the following optimization problem for D2D pairs.
\begin{equation}
\begin{aligned}
P1 : & \underset{P_m}{ \text{ max}} \sum_{m=0}^{M_\text{f}-1} \log_2(1+\frac{P_m^2}{\sigma_\text{N+I}^2}), \\
\text{s.t.}  & \\
& \sum_{m \in B_\text{f}}P_m\Omega_m, \leq I_\text{th}, \\
& \sum_{m=0}^{M_\text{f}-1} P_m \leq P_\text{t},\\
& P_m \geq 0,  \forall m \in \{1 \ldots M_\text{f}\}.\\
\end{aligned}
\label{P2}
\end{equation}
where $\sigma_\text{N+I}^2$ is the term accounting for both white noise and interference coming from cellular users.

After deriving the Karush-Kuhn-Tucker (KKT) conditions for the optimization in (\ref{P2}), the optical power allocation on subcarrier m is given by \cite{Shaat2009	} as 
\begin{equation}
\label{eq:optimalpower}
P_m^* = \max\left(0, \frac{1}{\alpha\Omega_m+\beta}-\sigma_\text{N+I}^2\right),
\end{equation}
$\alpha$ and $\beta$ being the Lagrangian coefficients relative to (\ref{P2}).

\begin{table}
	\renewcommand{\arraystretch}{1.1}
	\centering
	\caption{Number of data symbols useful to D2D pairs} 
	\label{tab:nSymbols}
	\begin{tabularx}{0.95\linewidth}{|X|X|}
		\hline
		\hline
		\textbf{Waveform} & Useful D2D Symbols \\
		\hline
		\hline
		\textbf{OFDM} & $N_\text{f}$ \\
		\textbf{FMT} & $N_\text{f}-K+1$ \\
		\textbf{OFDM/OQAM} & $\lfloor N_\text{f}\frac{M+N_\text{CP}}{M}-K+\frac{1}{2}\rfloor$ \\
		\textbf{Lapped FBMC} & $\lfloor N_\text{f}\frac{M+N_\text{CP}}{M}-1\rfloor $ \\
		\textbf{GFDM} & $N_b\lfloor \frac{N_\text{f}}{N_\text{b}}\frac{N_\text{b}(M+N_\text{CP})}{N_\text{b}M+N_\text{CP}}\rfloor$ \\
		\hline
		\hline
	\end{tabularx}
	\vspace{-15pt}
\end{table}


\subsection{Considering Transmission Window Duration}
As a static interference constraint is assumed during the whole D2D transmission, we can simply compute the total number of bits transmitted as
\begin{equation}
\label{eq7}
b = T_{\text{useful}}*\sum_{m=0}^{M_\text{f}-1}\log_2(1+\frac{P_m^*}{\sigma_\text{N+I}^2}),
\end{equation}
where $T_{\text{useful}}$ is the duration in which useful symbols can transmitted. This value depends on the utilized waveform. Indeed, for filter banks with linear pulse shaping like FMT and Lapped FBMC, the overlapping factor $K$ introduces a delay of $K-1$ symbols in the time domain. For OFDM/OQAM, the delay imposed by the transmit and receive filters is $K-\frac{1}{2}$ symbols in time as symbols are separated by $\frac{T_s}{2}$. In contrast, OFDM and GFDM do not suffer from any delay. In fact, the block structure of GFDM brings some limitations, as the length of the whole block is fixed for any number of active symbols. Thus, the transmission window can only be fully utilized in time if its duration is a multiple of the GFDM block length.
Table \ref{tab:nSymbols} presents the number of usable symbols for each waveform as a function of both D2D and incumbent parameters during a transmission window of length $N_\text{f}$ symbols.

\begin{table}[t]
	\caption{Proposed D2D Waveform Parameters}
	\label{tab:phy}
	\renewcommand{\arraystretch}{1.1}
	\centering
	\begin{tabularx}{0.95\linewidth}{|X|X|X|X|X|}
		\hline
		\hline
		\textbf{Waveform} & Samples per symbol & \ac{CP} samples & Filter & Overlapping factor. \\
		\hline
		\hline
		\textbf{OFDM} & $M$ & $N_\text{CP}$ & Irrelevant & Irrelevant \\
		\hline
		\textbf{FMT} & $M+N_\text{CP}$ & 0 & RRC, rolloff 0.2 & 6 \\
		\hline
		\textbf{OFDM/ OQAM} & $M$ & 0 & Phydias
		\cite{Deliverable2009} & 4 \\ \hline
		\textbf{Lapped FBMC} & $M$ & 0 & Sine \cite{Bellanger2015} & 2 \\ \hline
		\textbf{GFDM} & $M$ & $N_{CP}$ & RRC, rolloff 0.2 & 5 \\ \hline\hline
	\end{tabularx}
	\vspace{-15pt}
\end{table}
