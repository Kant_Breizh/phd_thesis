\subsection{Typical Coexistence Scenario}
A typical coexistence scenario is presented in Fig.~\ref{fig:coex_layout}, where we see that the signals of both the FB based secondary $\mathbf{s}_\textbf{s}$ and the CP-OFDM based incumbent $\mathbf{s}_\textbf{i}$ add up to form the signal $\mathbf{r}$ at the input antenna of the incumbent CP-OFDM receiver. Note that, here, no noise and no channel effects are considered, in an effort to focus on the interference caused by the FB secondary tranmission. Then, the over-the-air signal $\mathbf{r}$ is passed through the CP-removal block which outputs the useful part of the signal, noted $\mathbf{\tilde{r}}$. After demodulation, the vector of estimated symbols  $\mathbf{\hat{d}}$ is obtained. Under this simplified coexistence scenario, $\mathbf{\hat{d}} = \mathbf{d}+\pmb{\eta}$, where $\mathbf{d}$ is the vector of quadrature amplitude modulation (QAM) symbols modulated by the CP-OFDM incumbent transmitter and $\pmb{\eta}$ is the interference caused by the  secondary FB based user.
In Fig.~\ref{fig:coex_spec_view}, we present an example spectral distribution of the incumbent and secondary users. The two users coexist in the same spectral band composed of $M$ subcarriers of width $\Delta\text{F}$, and each one is assigned a different set of subcarriers, $\mathcal{M}_\textbf{s}$ for the secondary and $\mathcal{M}_\textbf{i}$ for the incumbent.

Under this model, the interference power caused by the secondary FB user onto the incumbent CP-OFDM receiver is expressed as the EVM seen after demodulation as
\begin{equation}
I_\text{tot} = E\{|\mathbf{\hat{d}}-\mathbf{d}|^2\} = E\{|\pmb{\eta}|^2\},
\label{eq:I1}
\end{equation}
where $E\{.\}$ is the mathematical expectation. $I_\text{tot}$ can be further decomposed in the sum of the power of interference caused by each subcarrier of the secondary onto each subcarrier of the incumbent as follows \cite{Bodinier2016ICT}:
\begin{equation}
I_\text{tot} = \sum_{m_\textbf{i}\in\mathcal{M}_\textbf{i},m_\textbf{s}\in\mathcal{M}_\textbf{s}}I(l = m_\textbf{s}-m_\textbf{i}), 
\label{eq:I_tot}
\end{equation}
where  $I(l = m_\textbf{s}-m_\textbf{i})$ is the interference caused by the subcarrier $m_\textbf{s}$ of the secondary to the subcarrier $m_\textbf{i}$ of the incumbent such that the spectral distance between these two subcarriers, noted $l$, is equal to $m_\textbf{s} -m_\textbf{i}$. 

$I(l)$ represents the interference power that is injected by a given subcarrier of the secondary FB user onto a subcarrier at spectral distance $l$ of the incumbent CP-OFDM user. It is the key factor that will determine if it is possible to insert efficiently the secondary transmission. Indeed, the faster $I(l)$ decreases with $l$, the more efficiently the secondary user will be able to utilize the free part of the band without interfering onto the subcarriers of the incumbent user. It is therefore crucial to model this value with great accuracy. 

\begin{figure}[!t]
	\captionsetup{justification=justified}
	\subfloat[]{	\begin{tikzpicture}[xscale=0.95]
		\node (I) [blue, text width=2.25cm, align = center, draw] at (0,-0.25) {Incumbent OFDM Transmitting User};
		\node (S) [orange, text width=2.25cm, align = center, draw] at (0,1.75) {Secondary FBMC based Transmitting User};
		\draw (2,0.75) circle(0.4) ;
		\draw (2,1.1) -- (2,0.4);
		\draw (1.65,0.75) -- (2.35,0.75);
		\draw [>=latex,->] (S.east)-- (2, 1.5) node[above]{$\mathbf{s}_\textbf{s}$}-- (2, 1.15) ;
		\draw [>=latex,->] (I.east)-- (2, 0) node[below]{$\mathbf{s}_\textbf{i}$} -- (2, 0.35) ;
		\node[blue, draw, dashed, text width = 4cm, text height= 2.5cm ] (RX) at (5, 0.75){};
		\node[blue,above] at (RX.north){Incumbent OFDM Receiving User};
		\node[draw, blue, align=center, text width = 1.2cm, align = center] (CPR) at (3.8,0.75) {CP\\ Removal};
		\node[draw, blue, align=center, text width = 1.2cm, align = center] (FFT) at (5.8,0.75) {OFDM Demod.};
		\draw[->,>=latex] (2.4,0.75) -- node[near start,above,pos=0.3]{$\mathbf{r}$} (3.1,0.75);
		\draw[->,>=latex] (CPR)-- node[above,pos=0.5]{$\mathbf{\tilde{r}}$}(FFT);
		\draw[->,>=latex] (FFT)-- node[above,pos=0.5]{$\mathbf{\hat{d}}$} (7,0.75);
		\end{tikzpicture}\label{fig:coex_layout}}\\
	\subfloat[]{	\begin{tikzpicture}[xscale=0.95]
		\draw[->,>=latex] (0,0) -- node[at end, below left]{$\frac{f}{\Delta \text{F}}$} (\linewidth,0);
		\draw[xstep=0.3,ystep=100,very thin] (0.3,0) grid (\linewidth*0.85,0.2);
		\node[below] at  (\linewidth*0.85, 0) {$\frac{M}{2}-1$};
		\node[below] at  (0.2, 0) {$-\frac{M}{2}$};
		\draw[->,>=latex] (\linewidth*0.439, -0.3) -- node[at start,left]{$0$} (\linewidth*0.439, 1.5);
		\draw[dashed,blue] (0.75, 0) -- (0.75, 1.5);
		\draw[dashed,blue] (3.15, 0) -- (3.15, 1.5);
		\draw[dashed,orange] (4.05, 0) -- (4.05, 1.5);
		\draw[dashed,orange] (6.15, 0) -- (6.15, 1.5);
		\node[blue] at (1.9,1.2) {$\mathcal{M}_\textbf{i}$}; 
		\node[orange] at (5.1,1.2) {$\mathcal{M}_\textbf{s}$};
		\draw[xstep=0.3, shift={(-0.15,0)},blue](0.9,0) grid (3.3,1);
		\draw[xstep=0.3, shift={(-0.15,0)},orange](4.2,0) grid (6.3,1);
		%\draw[->,->=latex] (4.05,1) -- node[near end, above]{$\delta_\text{f}$} (4.2,1);
		\end{tikzpicture}\label{fig:coex_spec_view}}
	
	\caption{Considered coexistence scenario\\
		(a) Coexistence layout : the signals of the secondary and the incumbent sum up and are passed through the demodulator of the CP-OFDM incumbent receiver.\\
		(b) Spectral representation : the incumbent and secondary systems coexist in the same spectral band, and each one is assigned a different subset of subcarriers. %The secondary system misaligns its subcarriers with respect to the frequency basis of the incumbent by a frequency offset $\delta_\text{f}$.
	}
	
\end{figure}


\subsection{Modeling of $I(l)$ in the literature}
In the literature, the modeling of $I(l)$ is based on the PSD of each subcarrier of the secondary FB transmitter. This model has been initially proposed to rate interference between various OFDM users in a spectrum pooling context in \cite{weiss04}. Precisely, naming $\Phi_\mathbf{s}$ the PSD of each subcarrier of the FB based waveform used by the secondary, $I(l)$ is usually modeled according to the PSD-based model as
\begin{equation}
I_\text{PSD}(l) = \int\limits_{l-\frac{\Delta\text{F}}{2}}^{l+\frac{\Delta\text{F}}{2}} \Phi_\mathbf{s}(f)\ df.
\label{eq:I_PSD}
\end{equation}
The total interference is then obtained by putting \eqref{eq:I_PSD} in \eqref{eq:I_tot}. The obtained expression of the total interference seen by the incumbent receiver is then equivalent to
\begin{equation}
I_\text{PSD,tot} = \int\limits_{\mathcal{M}_\textbf{i}}\mathcal{S}_\textbf{s}(f) df,
\label{eq:I_PSD_tot}
\end{equation}
where $\mathcal{S}_\textbf{s}(f)$ is the total PSD of the signal $\mathbf{s}_\textbf{s}$.
\makeatletter
\def\ifabsgreater #1#2{\ifpdfabsnum 
	\dimexpr#1pt>\dimexpr#2pt\relax
	\expandafter\@firstoftwo\else\expandafter\@secondoftwo\fi }
\makeatother


%%\begin{figure}[!t]
%\begin{tikzpicture}
%\begin{axis}[
%axis lines=middle,
%xmin=-1,xmax=16,
%ymin=-80,
%ymax=4,
%domain=0.01:16,
%samples = 1000,
%]
%\addplot[draw=blue] {20*(ln(abs(sin(pi*deg(x))/(pi*x)))/ln(10))};
%\addplot[draw=orange] {20*ln(abs(sin(pi*deg(x))/(pi*x)+0.971960*(sin(pi*deg(x-1/4))/(pi*(x-1/4))+sin(pi*deg(x+1/4))/(pi*(x+1/4)))+1/sqrt(2)*(sin(pi*deg(x-1/2))/(pi*(x-1/2))+sin(pi*deg(x+1/2))/(pi*(x+1/2)))))/ln(10)};
%\end{axis}
%\end{tikzpicture}
%\end{figure}

From the expression \eqref{eq:I_PSD_tot}, it is clear that the PSD-based model considers interference in the channel, before the input antenna of the incumbent CP-OFDM receiver, and is not consistent with the actual expression of the interference expressed in \eqref{eq:I1}.

