\begin{figure}
		\vspace{-10pt}
	\centering

	\begin{tikzpicture}
	\begin{axis}[xlabel=$\frac{t}{T}$,ylabel= ,ymajorticks = false,domain=-2:2, axis lines=middle,ymax=6, samples=100,smooth,width=\linewidth, height=4cm]
	\addplot[domain = 0:1,mark=none,thick,orange] {1+0.971960*2*cos(deg(2*pi*x/4))+1/sqrt(2)*2*cos(deg(4*pi*x/4))+0.235147*2*cos(deg(6*pi*x/4))};
	\addplot[domain = -2:2,dashed,mark=none,thick,orange] {1+0.971960*2*cos(deg(2*pi*x/4))+1/sqrt(2)*2*cos(deg(4*pi*x/4))+0.235147*2*cos(deg(6*pi*x/4))};
	\draw[blue] (axis cs:1,\pgfkeysvalueof{/pgfplots/ymin}) -- (axis cs:1,\pgfkeysvalueof{/pgfplots/ymax});
	\draw[blue] (axis cs:0,\pgfkeysvalueof{/pgfplots/ymin}) -- (axis cs:0,\pgfkeysvalueof{/pgfplots/ymax});
	\end{axis}
	\draw[<->,>=latex,orange] (0,2.1cm) -- node[above,near start]{Full FB symbol} (\linewidth*.82,2.1cm);
	\draw[<->,>=latex,blue] (\linewidth*.41,0) -- node[below,pos=0.5,text width = \linewidth*0.2]{CP-OFDM receive window} (\linewidth*.615,0);
	\end{tikzpicture}
	\caption{Truncation of the FB filter operated by the CP-OFDM receiver on the receiving window corresponding to a single CP-OFDM symbol.}
	\label{fig:truncation}
		\vspace{-10pt}
\end{figure}
In the former section, we showed that the PSD-based model is not relevant to study the interference $\pmb{\eta}$ and therefore, to rate the EVM $I_\text{tot}$ as defined in \eqref{eq:I1}, which corresponds to the actual value of interference seen by the CP-OFDM incumbent receiver after the OFDM demodulation operations. However, one may think that the PSD-based model is still a good approximation that will give a good idea of the actual values of interference. Hereafter, we explain why it is not the case.

FB waveforms rely on the filtering of each symbol by a filter $g$ that is longer than the time symbol $T=\frac{1}{\Delta\text{F}}$. This means that each FB symbol has a temporal support higher than the time symbol $T$. On the opposite, the CP-OFDM receiver of the incumbent truncates the incoming signal in windows of duration $T$ before performing FFT operations on each of these windows. Therefore, the FB and CP-OFDM signals are not orthogonal, as they are not based on the same temporal support. An example of the way the filter $g$ used by the FB is cut is shown in Fig.~\ref{fig:truncation}. It is shown that only a certain portion of each FB symbol, represented by a solid line, is taken into account by the CP-OFDM receive window. More detailed explanation on this aspect can be found in \cite{Bodinier2016ICT}.

\begin{figure}
	\vspace{-10pt}
	\includegraphics[width=\linewidth]{fig/pre_post_OFDM4.eps}
	\caption{Comparison of the PSD of the FB signal before and after passing through the CP-OFDM demodulator. The CP-OFDM receive operations destroy the good spectral shape of the FB signal, which becomes almost as poorly spectrally localized as an OFDM signal.}
	\label{fig:avant_apres}
	\vspace{-10pt}
\end{figure}
Note that the rigorous mathematical analysis of the effects of this truncation is out of the scope of this paper, but has been led for a particular FB waveform in \cite{Bodinier2016GC}. However, it is readily understood that the rectangular truncation in time of the FB filter  $g$ is equivalent, in frequency, to convolving it with a function $\text{sinc}(fT)$.
From this observation, it is clear that the selective properties of the FB filter $g$ are lost when passing through the CP-OFDM receiver. To illustrate this, we study a particular type of FB waveform, OFDM/OQAM with the PHYDYAS filter. More information on this waveform can be found in \cite{Bellanger2010}. In Fig.~\ref{fig:avant_apres}, we show the PSD of one OFDM/OQAM subcarrier. It is clear from that figure that, after passing through the CP-OFDM receiver, the spectral shape of the OFDM/OQAM signal is deeply altered. Whereas the original OFDM/OQAM signal is almost perfectly localized in frequency, high frequency ripples appear after the CP-OFDM receiver, which are due to the rectangular truncation shown in Fig.~\ref{fig:avant_apres}. Actually, the spectral shape of the altered OFDM/OQAM signal is very close to that of an OFDM signal.
To strengthen our point, we compare in Fig.~\ref{fig:psd_evm} the two ways of computing the values of interference injected by the OFDM/OQAM secondary onto a CP-OFDM incumbent, either through EVM or through PSD. It is very clear from this figure that the PSD measurements are completely irrelevant to measure the interference actually seen by the CP-OFDM incumbent, and do not approximate it in a satisfying way at all.

Though the results presented in Fig.~\ref{fig:avant_apres} and Fig.~\ref{fig:psd_evm} concern here only a particular waveform, the same behavior will be shown by other FB waveforms, as the limiting factor is the receive window of the CP-OFDM incumbent. More generally, because of the effects shown in Fig.~\ref{fig:truncation}, waveforms with different temporal support are bound to cause high interference onto each other. As a matter of fact, the values of $I(l)$ based on EVM measurements have been obtained through simulations in \cite{BodinierICC2016} for a number of other waveforms, such as Generalized Frequency Division Multiplexing (GFDM) \cite{Michailow2014}, Filtered Multi-Tone (FMT) \cite{FMT_Emphatic} and Lapped FBMC \cite{Bellanger2015}. All these waveforms were shown to interfere approximately as much as OFDM/OQAM onto CP-OFDM incumbent users in \cite{BodinierICC2016}.

These results show that, in the context of 5G, to cope with heterogeneity at the PHY level in the network, \textit{usual spectrum masks need to be replaced to efficiently protect incumbent CP-OFDM users}. Indeed, FB based users could easily fit the currently used spectrum masks based on PSD, but still interfere in a critical way onto CP-OFDM incumbent users. More specifically, instead of defining the transmit masks of secondary users, future standards should include the possibility for each incumbent user to define its maximum acceptable EVM post-demodulation, and let the secondary users adapt their transmission to respect that limitation. These new types of masks will be more complex to specify than PSD based spectrum masks, as they need both the properties of the transmitter and the receiver to be taken into account. 

\begin{figure}
	\vspace{-10pt}
	\includegraphics[width=\linewidth]{fig/interfTable.eps}
	\caption{Comparison of values of interference caused by FB secondary onto CP-OFDM primary when using either PSD or EVM measurements.}
	\label{fig:psd_evm}
	\vspace{-10pt}
\end{figure}
