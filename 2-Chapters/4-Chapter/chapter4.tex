%!TEX root = ../../thesis.tex

% First chapter begins here
\chapter{Coexistence with CP-OFDM incumbent systems in CR setups}
\label{chapter:4}

% Write miniTOC just after the title
\minitoc

% Set path for images
\graphicspath{{2-Chapters/4-Chapter/Images/}}
 
\section{Introduction}
\label{sec:intro_4}
In the former chapter, we have thoroughly analyzed the interference caused by enhanced multicarrier waveforms onto a CP-OFDM receiver. One of the key results we have established is that the PSD-based modeling of interference gives inaccurate results and dramatically underestimates the actual interference experienced by CP-OFDM incumbent systems. However, as we have extensively explained in Chapter \ref{chapter:2a}, this PSD-based model has been widely used in the literature to investigate CR setups in which the incumbent system may be based on CP-OFDM. Therefore, given the new insights we brought in Chapter \ref{chapter:3}, it is important to investigate some typical CR problems to understand what are the actual benefits of using enhanced waveforms for secondary transmission in cases where the incumbent relies on CP-OFDM. 

In this chapter, we therefore investigate two CR problems that have been thoroughly discussed in the literature. First, in section \ref{sec:GB_dimensioning} we compute the width of the guard band that is necessary between the two coexisting systems to ensure a given level of protection demanded by the incumbent system. Then, in section \ref{sec:optimal_pa} assuming that the secondary and incumbent systems are assigned fixed bands to transmit, we analyze the optimal power distribution that the secondary system should follow to maximize its achievable rate. 

Furthermore, in section \ref{sec:optimal_trans_tf}, we apply these results in a particular setup in which the secondary system is only given a finite amount of time to transmit. Finally, section \ref{sec:ccl_4} concludes this chapter. Note that the results presented in this chapter have been presented in two conference papers: \cite{Bodinier2016ISWCS} and \cite{BodinierICC2016}, and partially in \cite{Bourrous2017}.

\section{Guard band dimensioning}
\label{sec:GB_dimensioning}
\subsection{System setup}
Usually, in CR scenarios, the incumbent system defines a certain level of interference that it considers to be acceptable. In the following, we define this level as the \textit{interference threshold} $I_\text{th}$ which corresponds to the amount of interference acceptable on each subcarrier of the incumbent system. One of the most studied problems in the CR literature consists in evaluating the width of the guard band that is necessary to ensure that the interference power level seen on each subcarrier of the incumbent CP-OFDM system is lower than $I_\text{th}$. Namely, this is the approach followed in \cite{Ihalainen2008, Datta2011,skrzypczak2012ofdm,KeysightWaveforms,Berg2014743}. 

In this section, we will study this problem in a CR setup in which a secondary device which uses $36$ subcarriers to transmit coexists with a CP-OFDM incumbent system. Then, according to the waveform used by the secondary, we compute the number $N_G$ of guard subcarriers to ensure that the interference experienced on each subcarrier of the incumbent is lower than $I_\text{th}$. In other words, we aim at resolving the following problem:
\begin{flalign}
	\text{Compute } N_G=&\min\limits_{m_S\in\mathcal{M_S}, m_I \in\mathcal{M_I}}|m_S-m_I-1|&\nonumber\\
	&\text{such that}&\nonumber\\
	&\max_{m_I \in \mathcal{M}_I}\left(\underset{I^{S\rightarrow I}_{m_I}}{\underbrace{\sum_{m_S \in \mathcal{M_S}}I^{S\rightarrow I}(m_S,m_I)}}\right)\nonumber < I_\text{th}.& 
\end{flalign}

\begin{figure}
\subfloat[]{\includegraphics[width=0.48\linewidth]{I_PSD}\label{fig:I_PSD_GB}}\hfill
\subfloat[]{\includegraphics[width=0.48\linewidth]{I_EVM}\label{fig:I_EVM_GB}}
\caption{Interference seen on each subcarrier of the incumbent according to (a)the PSD-based model and (b)the EVM-based model presented in chapter 4.}
\label{fig:I_total_3rb}
\end{figure}
\subsection{Obtained results}
Firstly, we compute the interference power on each subcarrier of the incumbent that would be seen at the input antenna according to the PSD-based model and present the obtained results in Fig.~\ref{fig:I_PSD_GB}. Then, we compare them with the interference that would be seen after the CP-OFDM demodulation, computed with the EVM-based approach we presented in chapter 4. Results are shown in Fig.~\ref{fig:I_EVM_GB}. Once again, this gives a glaring example of the discrepancy between the values obtained with the two approaches: while the PSD-based results show significant difference between the different waveforms according to their spectral localization, the EVM-based ones demonstrate that all of them interfere equivalently onto the CP-OFDM incumbent receiver.


\begin{figure}
\subfloat[]{\includegraphics[width=0.48\linewidth]{NG_PSD}\label{fig:NG_PSD_GB}}\hfill
\subfloat[]{\includegraphics[width=0.48\linewidth]{NG_EVM}\label{fig:NG_EVM_GB}}
\caption{Number of guard subcarriers necessary to protect the incumbent as a function of its interference threshold per subcarrier $I_\text{th}$ according to (a) the PSD-based model of the literature and (b) our EVM-based modeling.}
\label{fig:GB_necessary}
\end{figure}
These values of interference can then be used to measure the amount of guard subcarriers that are needed between the two systems to protect the incumbent. To do so, one simply has to find on Fig.~\ref{fig:I_EVM_GB} and Fig.~\ref{fig:I_PSD_GB} the value of $m_I$ above which $I^{S\rightarrow I}_{m_I}<I_\text{th}$. The corresponding values are presented in Fig.~\ref{fig:GB_necessary}. Once again, we present the results obtained by following the PSD-based modeling of the literature and those obtained with our much more accurate EVM-based approach. 

Consistently with the results presented in Fig.~\ref{fig:I_total_3rb}, we see that the PSD-based model greatly underestimates the number of necessary subcarriers $N_G$. For example, for $I_\text{th} = -30\text{ dB}$, the PSD-based model predicts that only ones guard subcarrier is necessary if the secondary uses OFDM/OQAM instead of 28 if the latter was based on CP-OFDM. Our EVM-based approach shows on the opposite that for the same interference threshold, 46 guard subcarriers are necessary for CP-OFDM and 29 for OFDM/OQAM. Overall, while the PSD-based approach predicts a high variation in the results in function of the waveform used by the secondary system, actual results obtained with the EVM-based approach we proposed show that the number of necessary guard subcarriers is comparable with all waveforms tested. This result demonstrates that the insertion of secondary systems in spectral holes is not greatly facilitated by the sole adoption of enhanced multicarrier waveforms.

\section{Optimal power allocation}
\label{sec:optimal_pa}
\subsection{System setup}
The guard band dimensioning problem we have presented in the former section gives some insights on the gains brought by the use of enhanced waveforms in CR setups. However, it is insufficient. Indeed, it follows the idea of spectral masks and assumes that both systems transmit at equal power. However, one could imagine that a secondary system could transmit with more or less power according to the surrounding incumbent systems. Doing so, the secondary system tries to optimize its achievable rate while respecting the interference constraints set by the incumbent. This can be expressed as the following optimization problem:
\begin{equation}
\begin{aligned}
P1: & \underset{P_{m_S}}{ \text{max}} \sum_{m_S\in\mathcal{M}_S} \log_2\left(1+\frac{P_{m_S}G_{SS}}{\sigma_w^2+\sigma_I^2}\right), \\
\text{s.t.}  & \\
& \forall m_I \in \mathcal{M}_I, ~I_{m_I}^{S\rightarrow I} \leq I_\text{th}, \\
& \sum_{m_S \in \mathcal{M}_S} P_{m_S} \leq P_{\text{t}_S},\\
& P_{m_S} \geq 0,  \forall m \in \mathcal{M}_S.\\
\end{aligned}
\end{equation}

In the problem $P1$, the cost function to optimize is the rate achieved by the secondary system, expressed in function of the SINR at the secondary receiver. The first constraint expresses the interference threshold that should not be exceeded on each subcarrier of the incumbent system. The second constraint is related to the total power budget of the secondary system, i.e. how much power is available for the latter to communicate. Finally, the last constraint is straightforward and simply recalls that the power allocated on each subcarrier of the secondary system should be positive. 
Note that problem $P1$ can take different forms according to the waveform used by the secondary. Indeed, if the latter uses a subband-filtered waveform, it can only allocate the same power to every subcarrier and $P1$ is therefore changed into:
\begin{equation}
\begin{aligned}
P2: & \underset{P_{S}}{ \text{max}} \log_2\left(1+\frac{P_S G_{SS}}{\sigma_w^2+\sigma_I^2}\right), \\
\text{s.t.}  & \\
& \forall m_I \in \mathcal{M}_I, ~I_{m_I}^{S\rightarrow I} \leq I_\text{th}, \\
& P_S \leq P_{\text{t}_S},\\
& P_S \geq 0.\\
\end{aligned}
\end{equation}

However, if the secondary system uses a FB-MC waveform, each subcarrier can be dealt with separately and get assigned a different power level. Furthermore, in this case, as we demonstrated in Chapter \ref{chapter:3}, the contribution of each subcarrier of the secondary to the total interference suffered by the incumbent can be computed separately. Therefore, for FB-MC waveforms, $P1$ turns into
\begin{equation}
\begin{aligned}
P3: & \underset{P_{m_S}}{ \text{max}} \sum_{m_S\in\mathcal{M}_S} \log_2\left(1+\frac{P_{m_S} G_{SS}}{\sigma_w^2+\sigma_I^2}\right), \\
\text{s.t.}  & \\
& \forall m_I \in \mathcal{M}_I, ~\sum_{m_S\in\mathcal{M}_S} I^{S\rightarrow I}(m_S,m_I) \leq I_\text{th}, \\
& \sum_{m_S\in\mathcal{M}_S} P_{m_S} \leq P_{\text{t}_S},\\
& \forall m_S \in \mathcal{M}_S, P_{m_S} \geq 0,\\
\end{aligned}
\end{equation}
with $ I^{S\rightarrow I}(m_S,m_I)$ computed as depicted in chapter 4. Both $P2$ and $P3$ are convex optimization problems. Several works in the literature, such as \cite{shaat2010computationally}, have studied these problems and proposed near-optimal low complexity algorithms to solve them. In this study, we are simply interested in studying the optimal solution for each of the studied waveforms and do not focus on any particular means to achieve the said optimum. 

\subsection{Obtained results}
\begin{figure}
\subfloat[]{\includegraphics[width=0.49\linewidth]{Pow_UFMC}}\hfill
\subfloat[]{\includegraphics[width=0.49\linewidth]{Pow_OFDMOQAM}}
\caption{Power allocated on each subcarrier of the secondary system according to the interference threshold set by the incumbent for (a) UFMC and (b) OFDM/OQAM.}
\label{fig:pow_alloc}
\end{figure}
In the following, we study a scenario in which an incumbent system occupies a subset of a band composed of $512$ subcarriers. In details, it uses the $144$ subcarriers of the set $\mathcal{M}_I = [-90 \ldots -19] \cup [20 \ldots 91] $, effectively leaving a spectral hole of $38$ subcarriers in the middle of the band. Out of these $38$ subcarriers, the secondary tries to use the $36$ comprised in $\mathcal{M}_S = [-17 \ldots 18]$, leaving one guard subcarrier on each side of its band. Furthermore, we consider that $G_{SS} = G_{SI}$, which corresponds to a case where the gain of the secondary channel and the interfering channel are similar. This is for example achieved when the secondary transmitter is at comparable distances of the incumbent and secondary receivers. The optimization problem $P2$ for UFMC and f-OFDM and $P3$ for other waveforms is solved by using CVX.
\nomenclature[A]{CVX}{Matlab Software for Disciplined Convex Programming}
\nomenclature[A]{GUI}{Graphical User Interface}

In Fig.~\ref{fig:pow_alloc}, we represent the power that gets allocated on each subcarrier of the secondary system as the interference threshold $I_\text{th}$ set by the incumbent is varied. The results are shown both in the cases where the secondary uses UFMC and OFDM/OQAM. As we have explained in the previous subsection, when the secondary uses UFMC or f-OFDM, we are unable to perform per-subcarrier power allocation because of the subband-filtering. Therefore, the power that can be allocated on the whole secondary band is capped by the level that can be allocated at the edge of the band. Indeed, it is the subcarriers at the edge of the secondary band which cause the most interference to the incumbent. Finally, as the interference threshold increases, the power budget constraint becomes predominant and the power on each subcarrier is capped at $\frac{P_{t_S}}{36}$. On the other hand, if the secondary uses a FB-MC waveform, the center subcarriers can be assigned more power. This is shown in Fig.~\ref{fig:pow_alloc} for OFDM/OQAM, in which we see clearly that the center subcarriers are allocated more power than those at the edge. However, when the interference constraint effects fades out, all subcarriers are assigned the same average power level. This shows that the subcarrier-granularity power allocation can be particularly beneficial in interference constrained setups.

\begin{figure}
\subfloat[]{\includegraphics[width=0.49\linewidth]{Capa_PSD}}\hfill
\subfloat[]{\includegraphics[width=0.49\linewidth]{Capa_EVM}}
\caption{Achievable rate (not accounting for TSE) by each studied waveform in the studied scenario as computed with (a) the PSD-based model and (b) our EVM-based approach.}
\label{fig:achievable_rate}
\end{figure}

Finally, in Fig.~\ref{fig:achievable_rate}, we present the rate achievable by the secondary according to the waveform it uses. Note that the values we present do not account for the TSE of each waveform. Once more, we present the results obtained with the PSD-based modeling of interference and those predicted with our EVM-based approach. We can draw the same conclusions as for the results we have presented on the matter of guard band dimensioning. Indeed, the PSD-based model predicts that FMT, OFDM/OQAM and FBMC-PAM are almost never constrained by the interference constraint thanks to their enhanced spectral localization. In reality, the EVM-based results show that the rate all waveforms achieve is comparable, with OFDM/OQAM, FMT and FBMC-PAM performing only slightly better than GFDM and COQAM, the latter achieving slightly higher rate values than CP-OFDM. Interestingly, we see that the performance of f-OFDM and UFMC is quite disappointing. There are two reasons for this: first, as we have explained here above, f-OFDM and UFMC cannot perform subcarrier-wise power allocation, which is detrimental to the rate they can achieve in the interference constrained regime. Furthermore, in the scenario we have studied here, there is only one guard subcarrier between the secondary and the incumbent systems. However, as we have shown in chapter 2 in Fig.~\ref{fig:PSD_waveforms_intro}, the sidelobes of UFMC and f-OFDM decrease slower than those of FB-MC waveforms, which explains why even the results obtained with the PSD-based model are disappointing.
\section{Optimal transmission in a given time frame}
\label{sec:optimal_trans_tf}
\subsection{System setup}
\begin{figure}[t]
\centering
\begin{tikzpicture}[>=latex]
\draw[fill=green, draw=none] (-1.5,2.5) rectangle (11.5,0.5);
\draw[fill=green, draw=none] (-1.5,4) rectangle (11.5,6);
\draw[->] (0.5,0) -- node[at end,left]{$f$} (0.5,6.5);
\draw[fill=lightgray, draw=none] (-1.5,2.5) rectangle (11.5,4); 
\node[] at (-0, 3.25) {Free band $\mathcal{M}_S$};
\node[] at (5, 5) {Occupied band $\mathcal{M}_I$};
\draw[fill=red, draw=none] (5,2.5) rectangle node{Transmission window}(9,4);
\draw[dashed, gray, line width=2] (-1.5,2.5) -- (11.5, 2.5);
 \draw[dashed, gray, line width=2] (-1.5,4) -- (11.5, 4);
 \draw[<->] (5,2.25) -- node[below]{$T_\text{Tx}$} (9,2.25);
 \draw[->] (0,0.5) -- node[at end,below]{$t$}(10,0.5);
\end{tikzpicture}
\caption{Considered system setup: the secondary device transmits in the free band $\mathcal{M}_S$ that it can use during $T_\text{Tx}$}
\label{fig:sys_layout_time}
\end{figure}
So far, the results we have shown, both in terms of necessary guard subcarriers and achievable rate, have demonstrated that there is little gain to expect from using enhanced waveforms for the secondary transmission when the incumbent is itself based on CP-OFDM. These results clearly contradict the relevant literature. In this last section of this chapter, we build on top of the power allocation study we have devised here above to study a CR setup in which the secondary tries to maximize the amount of data it can transmit in a given transmission window that is limited both in time and frequency. The corresponding system setup is sketched in Fig.~\ref{fig:sys_layout_time}. 
As a static interference constraint is assumed during the whole secondary transmission, we can simply compute the total number of bits that can be transmitted by the secondary system as
\begin{equation}
\label{eq7}
b_S = T_{\text{useful}}\times\sum_{m_S\in\mathcal{M}_S} \log_2\left(1+\frac{P_{m_S} G_{SS}}{\sigma_w^2+\sigma_I^2}\right),
\end{equation}
where $T_{\text{useful}}$ is the duration in which useful symbols can transmitted. As we explained in section \ref{sec:5G_waveforms}, this value depends on the TSE of the utilized waveform and is directly obtained as $T_{\text{useful}} = \left\lfloor TSE\times \Delta\text{F}T_{\text{Tx}}\right\rfloor$.  In this  equation, the value of $P_{m_S}$ is the one obtained by finding the optimal solution of the rate optimization problem discussed in the former section.

\subsection{Obtained results}
\begin{figure}[t]
\subfloat[]{\includegraphics[width=0.49\linewidth]{Ith40}\label{fig:b_f_of_t_a}}\hfill
\subfloat[]{\includegraphics[width=0.49\linewidth]{Ith10}\label{fig:b_f_of_t_b}}
\caption{Total amount of transmittable data as a function of the available time to transmit $T_{\text{Tx}}$ for a) $I_{\text{th}} = -40\text{ dB}$ and b) $I_{\text{th}} = -10\text{ dB}$}
\label{fig:b_f_of_t}
\end{figure}
\begin{figure}[t]
\subfloat[]{\includegraphics[width=0.49\linewidth]{TTI}\label{fig:b_f_of_i_a}}\hfill
\subfloat[]{\includegraphics[width=0.49\linewidth]{200s}\label{fig:b_f_of_i_b}}
\caption{Total amount of transmittable data as a function of the interference constraint $I_{\text{th}}$ for a) a short transmission window $T_{\text{Tx}}=\frac{15}{\Delta\text{F}}$ equivalent to one LTE TTI and b) a long transmission window $T_{\text{Tx}}=\frac{200}{\Delta\text{F}}$.}
\label{fig:b_f_of_i}
\end{figure}

To stay consistent, we study the same spectral layout as in the former section, and therefore consider a scenario in which the band used by the secondary is composed of the $36$ center subcarriers of the total available band, with one guard subcarrier on each side separating it from the band used by the incumbent. In the following, we present results that enable us to better understand the performance achieved by the secondary when it uses one of the waveforms we have studied.

Overall, the results of \eqref{eq7} depend only on two factors: the interference threshold $I_{\text{th}}$ set by the incumbent which influences the maximum achievable rate, and the available time to transmit $T_{\text{Tx}}$ which acts on the number of symbols the secondary is able to emit and receive. Therefore, we study the effects of these two factors in details. In Fig.~\ref{fig:b_f_of_t}, we present the value taken by $b_S$ as the time to transmit $T_{\text{Tx}}$ is increased. Furthermore, we present results obtained in the interference-constrained regime for $I_{\text{th}}=-40\text{ dB}$ in Fig.~\ref{fig:b_f_of_t_a} and in the power-constrained regime for $I_{\text{th}}=-10\text{ dB}$ in Fig.~\ref{fig:b_f_of_t_b}. These two figures give quite interesting teachings. 

We see that when the interference constraint is predominant, the waveform used by the secondary plays a significant role. In particular, FBMC-PAM and OFDM/OQAM outperform all other waveforms. Interestingly, FBMC-PAM is constantly superior to OFDM/OQAM because of its shorter delay. Indeed, we have shown in Fig.~\ref{fig:achievable_rate} that these two waveforms  achieve exactly the same rate, which is due to the fact that both interfere equally onto CP-OFDM as demonstrated in chapter 3. However, FBMC-PAM can start transmitting as soon as $\Delta\text{F}T_{\text{Tx}}\geq 1$ whereas OFDM/OQAM can only transmit from $\Delta\text{F}T_{\text{Tx}}\geq 3.5$ on because of its high filter delay. The advantage of FBMC-PAM over OFDM/OQAM is therefore particularly important for short transmission windows. Nevertheless, FBMC-PAM can constantly transmit approximately $100$ bits more than OFDM/OQAM in the same transmit window. One should note, however, that this is not a significantly interesting gain in any realistic setup.

In the interference constrained regime and for long transmission windows, FMT follows directly OFDM/OQAM. However, it suffers from its very long delay and Gabor density inferior to 1 which make it difficulty applicable in short communications, where GFDM and COQAM tend to slightly outperform it. However, these two latter candidates suffer from their greater leakage and therefore lose their advantage over FMT as the transmission window grows longer. Interestingly, because interference is predominant in that case, CP-OFDM, UFMC and f-OFDM are always outperformed because of their poorer leakage. The performance of UFMC and f-OFDM is particularly poor, which is caused by the fact that they are unable to perform subcarrier-wise power loading as mentioned in the former section.

In Fig.~\ref{fig:b_f_of_t_b}, we take a closer look at the behavior exhibited by each waveform when the interference constraint becomes negligible. In that case, all waveforms can achieve the same rate and only their TSE can differentiate between them. We observe once more the superiority of FBMC-PAM. GFDM and COQAM closely follow OFDM/OQAM. Finally, CP-OFDM, UFMC and f-OFDM achieve the exact same performance, while FMT lags behind because of its poor TSE.

In Fig.~\ref{fig:b_f_of_i}, we investigate the effects of $I_{\text{th}}$ for two kinds of transmission windows. Firstly, we investigate a very short transmission window $T_{\text{Tx}}=\frac{15}{\Delta\text{F}}$ which corresponds approximately to one time transmission interval (TTI) in the LTE standard. As is represented in Fig.~\ref{fig:b_f_of_i_a}, in those very short transmission windows, OFDM/OQAM and FMT cannot achieve interesting performance because of their long prototype filters. Interestingly, FBMC-PAM keeps its lead, as it benefits from both reduced leakage and good TSE. COQAM follows just behind, which shows that the available time to transmit is the predominant factor in this case. This is further confirmed by the surprisingly good performance of CP-OFDM, UFMC and f-OFDM. We also note that GFDM performs quite poorly. Note that this is caused by its block structure, which allows it to transmit only a very limited number of symbols compared to other waveforms.

On the opposite, we represent in Fig.~\ref{fig:b_f_of_i_b} the total amount of data that can be transmitted by the secondary in a long window of duration $T_{\text{Tx}}=\frac{200}{\Delta\text{F}}$. We observe, once again, that FBMC-PAM and OFDM/OQAM lead the race, with a slight advantage to the former. GFDM and COQAM achieve medium performance whatever the value of $I_{\text{th}}$. FMT competes with FBMC-PAM and OFDM/OQAM for very stringent interference constraints but is rapidly limited by its low TSE and is less efficient than CP-OFDM, UFMC and f-OFDM when the interference constraint becomes insignificant.

All these results give interesting insights on the relative importance of the factors $I_{\text{th}}$ and $T_{\text{Tx}}$. However, it is important to put back the numerical values of the results we have presented into perspective: when the interference constraint is relaxed, enhanced multicarrier waveforms can only benefit of their improved TSE over CP-OFDM. However, this results in quite low gains of approximately $12.5 \%$ which correspond to the proportion of the CP wasted by CP-OFDM. Some relatively higher gains are demonstrated when the interference constraint set by the incumbent is predominant, as FBMC-PAM and OFDM/OQAM have been shown to be able to transmit around $1.7$ times as much data as CP-OFDM in the same transmit window. %However, this is quite disappointing given the promises of ten-fold increases in achievable rate expected with the PSD-based model of the literature.


\section{Conclusion}
\label{sec:ccl_4}
In this chapter, we have studied under different angles a CR setup in which a secondary system based on an enhanced multicarrier waveform coexists with a legacy CP-OFDM based incumbent system. Overall, the results we presented have shown that there is little gain to be achieved from using an enhanced waveform for the secondary transmission. In particular, we have shown that the gains that can be expected are insignificant in comparison with some that had been previously advertised in the literature. Realistically, the gains we have exposed make it difficult to justify the hassle associated with a change in the waveform used by secondary devices. Nevertheless, we have only studied setups in which there is only one secondary device that uses a waveform other than CP-OFDM. In reality, legacy networks will coexist will multiple secondary devices; if the latter are all to use enhanced multicarrier waveforms, they could bring important advantages. Therefore, in the following chapter, we focus on this type of setups. In particular, we apply our analysis in a D2D communication scenario, which is both timely and relevant to our study.



%  	\input{2-Chapters/4-Chapter/Acronyms}
% 	\section{Introduction}
% 	\label{sec:intro}
% 	\input{2-Chapters/4-Chapter/intro}
	
% 	\section{In-band Coexistence Scenario}
% 	\label{sec:model}
% 	\input{2-Chapters/4-Chapter/model}
	
% 	\section{Observation of FB Signal after CP-OFDM Demodulation}
% 	\label{sec:analysis}
% 	\input{2-Chapters/4-Chapter/analysis}
	
% 	\section{Revising Published Results}
% 	\label{sec:results}
% 	\input{2-Chapters/4-Chapter/results}
	
% 	\section{Conclusion}
% 	\label{sec:ccl}
% 	\input{2-Chapters/4-Chapter/conclusion}

% 	\section{Introduction}
% \input{2-Chapters/4-Chapter/introICC}

% \section{Considered Scenario and Studied Waveforms}
% \label{sec:system}
% \input{2-Chapters/4-Chapter/system_model}

% \section{Interference Analysis}
% \label{sec:interference}
% \input{2-Chapters/4-Chapter/interference}

% \section{D2D Pair Power Optimization}
% \label{sec:capacity}
% \input{2-Chapters/4-Chapter/capacity}

% \section{Numerical Results}
% \label{sec:results}
% \input{2-Chapters/4-Chapter/resultsICC}

% \section{Conclusion}
% \label{sec:ccl}
% \input{2-Chapters/4-Chapter/conclusionICC}
% 	