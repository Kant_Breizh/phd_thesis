\subsection{Considered scenario}
In this study, we consider an OFDM based incumbent network with subcarrier spacing $\Delta F$ where a certain group of subcarriers is free and can be utilized by a D2D pair. It is assumed that the base station broadcasts information about its unused time-frequency resources. This information includes the number of free subcarriers $M_\text{f}$ and the number of OFDM symbols during which the band will be free $N_\text{f}$. We define $B_\text{i}$ as the band ocuppied by the cellular users, $B_\text{f}$ the free band used by the D2D pair, and $I_\text{th}$ as the interference threshold for $B_\text{i}$. 
It is assumed that the D2D pair can misalign its transmission frequency with respect to $B_\text{f}$. Besides, we assume that, even though the D2D transmission is contained in the duration of the opportunity, time synchronism between D2D and cellular users at the symbol level may not be achieved.
We denote $\delta_\text{t}$ and $\delta_\text{f}$ as the offsets in time and frequency. The scenario is presented in figure \ref{fig:scenario}, where the grey grid represents the time-frequency resources of the incumbent network. These resources 
follow the 3GPP \ac{LTE} standard.

\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{scenario.eps}
	\caption{Time-Frequency layout of the situation with time-frequency misplacement representation.}
	\label{fig:scenario_b}
	\label{fig:scenario}
	\vspace{-15pt}
\end{figure}

\subsection{Waveforms under consideration}
To pave the way for the analysis conducted in Section \ref{sec:interference}, we briefly explain the mathematical representation of the waveforms under study to be used by the D2D pair.
\subsubsection{FMT}
In this scheme, complex \ac{QAM} symbols are linearly pulse shaped using the prototype filter $\mathbf{g} = [g[0]\ldots g[KP-1]]^\text{T}$, where $K$ is the overlapping factor and  $P$ is the number of samples per time symbol. $M$ is the number of subcarriers, $N$ the number of symbols in time, and $\mathbf{d}_m = [d_m[0]\ldots d_m[N-1]]^\text{T} $ the vector of modulated symbols on the $m$th subcarrier. Finally, the FMT signal can be obtained as \cite{FMT_Emphatic}
\begin{eqnarray}
x_\text{FMT}[k] &=& \sum_{n=0}^{N-1}\sum_{m=0}^{M-1}d_m[n] g[k-nP] e^{j2\pi\frac{m}{M}k}, \\&&\nonumber\forall k = 0\ldots  (N+K-1)P-1.
\label{eqFMT}
\end{eqnarray}

\subsubsection{OFDM/OQAM}
In this modulation format, the signal can be derived in a similar way to FMT, where $P=M$, $\{d_m\}$ symbols are drawn from a \ac{PAM} constellation, and a phase factor $\theta_m[n] = e^{j\frac{\pi}{2}\lfloor\frac{n+m}{2}\rfloor}$ is introduced where $n$ and $m$ the time-frequency indexes respectively. Besides, subsequent symbols are separated by $\frac{M}{2}$ samples in time, which implies doubling the symbol rate. Therefore, the transmit signal can be expressed as \cite{Deliverable2009}
\begin{eqnarray}
x_\text{OQAM}[k] &=& \sum_{n=0}^{N-1}\sum_{m=0}^{M-1}\bigg(d_m[n]\theta_m[n] g[k-n\frac{M}{2}] \\&&\times e^{j2\pi\frac{m}{M}(k-\frac{KM-1}{2})}\bigg)\nonumber,\\\nonumber&&\forall k = 0\ldots (P+K-\frac{1}{2})M-1.
\label{eqOQAM}
\end{eqnarray}

\subsubsection{Lapped FBMC}
This scheme is in essence similar to OFDM/OQAM systems. However, for this modulation the symbol rate is not doubled. As defined in \cite{Bellanger2015}, the emitted signal can be written as
\begin{eqnarray}
x_\text{Lapped}[k] &=& \sum_{n=0}^{N-1}\sum_{m=0}^{M-1} d_m[n]g[k-nM] e^{j(k-\frac{1}{2}+\frac{M}{2})(m-\frac{1}{2})\frac{\pi}{M}},\nonumber\\&& \forall k = 0\ldots (P+1)M-1.
\label{eqLapped}
\end{eqnarray}
It is worth mentioning that the used filter is not tunable and is written as
\begin{equation}
\label{lappedFilter}
g_\text{Lapped}[k] = -\sin\left[(k-\frac{1}{2})\frac{\pi}{2M}\right], \forall k=0\ldots 2M-1.
\end{equation}

\subsubsection{GFDM}
The data is packed in blocks of $N_\text{b}$ symbols and a \ac{CP} is added to the beginning of each block. Every symbol is circularly convolved with a time shifted version of the same circular filter $\mathbf{g}$. Denoting as $\text{mod}$ the modulo operation, the signal corresponding to one GFDM block is expressed as follows  \cite{Michailow2014}
\begin{align}
  & x_\text{GFDM}[k] = \sum\limits_{n=0}^{N_\text{b}-1} \sum\limits_{m=0}^{M-1} d_m[n] g\left[(k-nM) \text{mod}(N_\text{b}M)\right] e^{j2\pi k\frac{m}{M}},&\nonumber\\ &
\hspace{50pt}\forall k = 0\ldots MN_\text{b}-1.
\label{eq:GFDMMod}
\end{align} 
