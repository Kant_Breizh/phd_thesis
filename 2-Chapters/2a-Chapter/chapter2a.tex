%!TEX root = ../../thesis.tex

% First chapter begins here
\chapter{Related work and corresponding contributions}
\label{chapter:2a}

% Write miniTOC just after the title
\minitoc

% Set path for images
\graphicspath{{2-Chapters/2a-Chapter/Images/}}

\section{Introduction}
In the previous chapter, we have laid out the main research questions that we propose to answer based on our review of the current state of wireless networks and their foreseen evolution. In particular, we have pointed out that the coexistence between enhanced multicarrier waveforms and CP-OFDM has not been well studied in the literature. In this chapter, we review the different approaches that are relevant to our work. In particular, we focus in section \ref{sec:bcg_interf_model} on the different ways that interference has been modeled in coexistence scenarios, and we identify two main approaches in the literature: a PSD-based approach on the one hand, and Asynchronous Interference Modeling (AIM) on the other hand. Furthermore, in section \ref{sec:experimental_studies}, we list the different experimental studies that address the problem of spectral coexistence between asynchronous users. In section \ref{sec:5G_coex_OFDM} we review works that have studied the application of 5G waveforms for coexistence with OFDM-based networks, in particular in the context of CR and D2D communication. Finally, we conclude this chapter in section \ref{sec:litt_lim_and_contrib} by summarizing the key limitations and flaws of currently available studies. Based on this analysis, we detail the major contributions that we will present in the following of this thesis.

\section{Interference modeling in coexistence scenarios}
\label{sec:bcg_interf_model}
\subsection{Spectral coexistence system model}
\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{graph_subcarriers}
	\caption[Spectral representation of the coexistence scenario]{Spectral representation of the considered scenario. The incumbent and secondary systems coexist in the same spectral band, and each one is assigned a different subset of subcarriers. Because the systems are not coordinated, the secondary system may misalign its subcarriers with respect to the frequency basis of the incumbent by a frequency offset $\delta_\text{f}$.
}
	\label{fig:spectral_distribution}
\end{figure}

\begin{figure}
\centering
	\includegraphics[width=0.425\linewidth]{PSD_model_total}
	\caption[Principle of the PSD-based modeling of interference]{Principle of the PSD-based modeling of interference. Note that this representation is given in the case where the two signals arrive with the same power at the receiver, i.e. $P_SG_{SI}=P_IG_{II}$. The PSD-based modeling of interference consists in integrating the PSD of the interfering signal on the band of interest.}
	\label{fig:difference_approach}
\end{figure}

Let us consider a scenario in which two systems, a secondary $\mathcal{S}$ and an incumbent $\mathcal{I}$, coexist in the same spectral band as is represented in Fig.~\ref{fig:spectral_distribution} in which they use adjacent, non-overlapping spectral resources. In the following, parameters indexed as $\cdot_\textsl{I}$ and $\cdot_\textsl{S}$ refer to the incumbent and secondary system respectively. This setup is a canonical example of spectral coexistence and a vast amount of works have aimed at answering the question: ``How can we model the interference caused by each user onto the other ?''. The first works addressing this question date back to the beginning of the century when the cognitive radio concept increasingly gained popularity after Mitola's Ph.D. dissertation \cite{Mitola2000}. The need for a model of interference usable for cognitive radio deployments in spectral coexistence setups was soon identified by Haykin in \cite{HaykinBrainEmpowered} where he defined the term \textit{interference temperature}. However, at that time, the research community did not identify clearly the links between this concept and the PHY layer problematics, especially the choice of waveform. It is only when Weiss and Jondral coined the term "spectrum pooling" in \cite{Weiss2004} that they defined a waveform-dependent model to compute interference between systems coexisting on the same spectral band. The model they defined went on to be known as the "PSD-based model" in the literature. 

This model has then been extensively used to compute interference between secondary and incumbent users in cognitive radio scenarios in which both systems would utilize CP-OFDM \cite{Weiss2004, shaat2010computationally}. Because of the high OOB emissions of CP-OFDM, research works based on the PSD-based model that we will present later on showed that CP-OFDM is not well adapted for spectrum sharing between secondary and incumbent users \cite{Mahmoud2009}. As we mentioned it in the previous chapter, this observation was one of the many arguments that fostered the research of a new waveform able to replace CP-OFDM in 5G systems.  Whatever the specific details of the aforementioned waveforms, they all achieve lower OOB emissions than CP-OFDM thanks to their inherent filtering of the transmitted symbols. Because of these advantageous PSD properties that we illustrated in Fig.~\ref{fig:PSD_waveforms_intro}, an important amount of research works have investigated the benefits of using these FB-MC waveforms for coexistence with legacy CP-OFDM incumbent users, for example \cite{Noguet2011, shaat2010computationally, skrzypczak2012ofdm, Su2016, Michailow2011}. Given the wide adoption of the PSD-based model, we detail its principle in the following.

\subsection{PSD-based modeling}
\subsubsection{Model definition}
The PSD-based model consists in computing the leakage caused by users onto each other by integrating the PSD of the interfering signal on the band that suffers from the interference. 
Therefore, in the scenario that we described in Fig.~\ref{fig:spectral_distribution}, according to the PSD-based model, the interference caused by the secondary system onto the  incumbent is simply obtained by integrating the PSD of the secondary signal on the band $\mathcal{M}_\textsc{I}$ of the incumbent CP-OFDM system. Defining $I_\text{PSD}^{\textsc{S}\rightarrow\textsc{I}}$ the interference injected by the secondary system onto the incumbent one according to the PSD based model, it is expressed as 
\begin{equation}
I_\text{PSD}^{\textsl{S}\rightarrow\textsl{I}} = P_\textsl{S}G_{SI}\int\limits_{\mathcal{M}_\textsl{I}} \Phi_\textsl{S}(f) \dif f,
\end{equation}
where $\Phi_\textsl{S}$ represents the PSD of the secondary signal modulated by secondary system $\textsl{S}$, $P_\textsl{S}$ is the transmit power of the secondary and $G_{SI}$ is the gain associated with the channel $h_{SI}$.

Considering that the secondary system modulates i.i.d. symbols with unitary power on each subcarrier, its PSD is defined as 
\begin{equation}
\forall f \in \mathbb{R}, \Phi_\textsl{S}(f) = \sum\limits_{m \in \mathcal{M}_\textsl{S}}\Phi_{m}(f) = \sum\limits_{m \in \mathcal{M}_\textsl{S}}\Phi_{\text{WF},\textsl{S}}(f-m\Delta\text{F}) ,
\end{equation}
where $\Phi_{m,\textsl{S}}(f)$ is the PSD of the $m$th subcarrier of system $\textsl{S}$, and is directly obtained from $\Phi_{\text{WF},\textsl{S}}$, the PSD of one subcarrier of the waveform used by the secondary system, as presented in Fig.~\ref{fig:PSD_waveforms}.
Therefore, it follows that, according to the PSD-based model, 
\begin{equation}
I_\text{PSD}^{\textsl{S}\rightarrow\textsl{I}} = \sum\limits_{m_s \in \mathcal{M}_\textsl{S}}\sum\limits_{m_i \in \mathcal{M}_\textsl{I}} I_\text{PSD}^{\textsl{S}\rightarrow\textsl{I}}(m_s - m_i), 
\end{equation}
with 
\begin{equation}
\forall l \in \mathbb{Z}, I_\text{PSD}^{\textsl{S}\rightarrow\textsl{I}}(l) = P_\textsl{S}G_{SI} \int\limits_{l\Delta\text{F}-\frac{\Delta\text{F}}{2}+\delta{\text{f}}}^{l\Delta\text{F}+\frac{\Delta\text{F}}{2}+\delta{\text{f}}}\Phi_{\text{WF},\textsl{S}}(f) \dif f.
\end{equation}
With this notation, $I_\text{PSD}^{\textsl{S}\rightarrow\textsl{I}}(l)$ corresponds to the amount of interference injected by a certain subcarrier $m_s$ of the secondary onto a subcarrier $m_i$ of the incumbent such that $m_s - m_i = l$.

\begin{figure}[t]
	\centering
	\subfloat[]{\includegraphics[trim={1cm 0 1cm 0},clip,width=0.5\linewidth]{PSD_model_CPOFDM}}
	\subfloat[]{\includegraphics[trim={1cm 0 1cm 0},clip,width=0.5\linewidth]{PSD_model_OFDMOQAM}}
	\caption[Principle of PSD-based modeling of interference, applied to two waveforms.]{Principle of PSD-based modeling of interference, applied to two waveforms. (a)~:~CP-OFDM, (b)~:~OFDM/OQAM. It applies in the exact same way to other FB-MC waveforms analyzed in this thesis.}
	\label{fig:PSD_model_principle}
\end{figure}

Following the symmetric reasoning, the total interference caused by the incumbent system on the secondary is expressed, according to the PSD-based model, as

\begin{equation}
	I_\text{PSD}^{\textsl{I}\rightarrow\textsl{S}} = \sum\limits_{m_s \in \mathcal{M}_\textsl{S}}\sum\limits_{m_i \in \mathcal{M}_\textsl{I}} I_\text{PSD}^{\textsl{I}\rightarrow\textsl{S}}(m_s - m_i),
\end{equation}
\begin{equation}
\forall l \in \mathbb{Z}, I_\text{PSD}^{\textsl{I}\rightarrow\textsl{S}}(l) = P_\textsl{I}G_{IS} \int\limits_{l-\frac{\Delta\text{F}}{2}}^{l+\frac{\Delta\text{F}}{2}}\Phi_{\text{WF},\textsl{I}}(f) \dif f,
\end{equation}
where $P_I$ is the transmit power of the incumbent and $G_{IS}$ the gain associated with channel $h_{IS}$.

An illustration of how the PSD-based model is used to compute the interference caused by the CP-OFDM and OFDM/OQAM waveforms is given in Fig.~\ref{fig:PSD_model_principle}. We show that the interference caused by the subcarrier of index $0$ onto the subcarrier of index $l$ is computed by integrating the PSD of subcarrier $0$ on the width corresponding to subcarrier $l$. Note that this model applies exactly in the same way to the other FB-MC waveforms we analyze in this study. 

\subsubsection{PSD-based model applied to FB-MC waveforms}

\begin{figure}
	\includegraphics[trim={1cm 0 1cm 0},clip,width=\linewidth]{PSD_waveforms}
	\caption[PSD estimate of studied waveforms]{Welch estimate of the PSD level of studied waveforms with a Hanning Window of length $\frac{100}{\Delta\text{F}}$ and a frequential resolution of $500$ points per subcarrier. PSD values are expressed relatively to their maximum.}
	\label{fig:PSD_waveforms}
\end{figure}

In Fig.~\ref{fig:PSD_waveforms}, we present the PSD of one subcarrier of the aforementioned waveforms with the chosen parameters. Note that, to achieve a fair comparison, we consider that not one, but two adjacent subcarriers of the FBMC-PAM system are active. The presented PSD curves can be used to compute the value of interference caused by each waveform at any given spectral distance according to the PSD-based model.
Note that the PSD-based model is not applicable to subband-filtered waveforms, as it relies on the fact that all subcarriers have the same PSD. Therefore, it is only applicable to FB-MC waveforms which perform a per-subcarrier filtering. We present in Table \ref{tab:PSD_model_results} the values of $I_\text{PSD}^{\textsl{S}\rightarrow\textsl{I}}(l)$ obtained for all studied FB-MC waveforms in the case where $P_\textsl{S}G_{SI} = 0~ dB$ and $\delta\text{f}=0$. As expected, we see that the PSD-based model predicts that waveforms with better spectral localization will inject the lowest amount of interference onto the incumbent CP-OFDM system.
\begin{table}
	\caption{Interference tables in dB computed according to the PSD-based model for studied waveforms.}
	\label{tab:PSD_model_results}
	\begin{tabularx}{\linewidth}{|c||m|m|m|m|m|m|}
		\hline
		$l$~ & CP-OFDM & FMT & OFDM/\newline OQAM & FBMC-PAM & GFDM & COQAM
	\end{tabularx}		
	\setlength{\tabcolsep}{0pt} % whitespace between columns % whitespace between rows (multiplicative factor)
	\renewcommand{\arraystretch}{0}
	\begin{tabularx}{\linewidth}{|c||M|M|M|M|M|M|}
		\hline
		\hline
		~~$0$~~ & -1.28 & -1.26 & -0.99  & -2.64 & -1.59 & -1.47 \\
		\hline
		$1$ & -12.3 & -10.8 & -12.3  & -13.1 & -10.0 & -10.3 \\
		\hline
		$2$ & -20.1 & -57.2 & -65.4  & -37.2 & -24.1 & -24.7 \\
		\hline
		$3$ & -23.9 & -62.7 & -80.7  & -45.3 & -28.1 & -28.9 \\
		\hline
		$4$ & -26.4 & -65.7 & -89.6  & -50.6 & -30.8 & -31.5 \\
		\hline
		$5$ & -28.0 & -68.0 & -96.1  & -54.6 & -32.8 & -33.4 \\
		\hline
		$6$ & -29.2 & -70.0 & -101. & -58.0 & -34.2 & -35.0 \\
		\hline
		$7$ & -30.3 & -71.0 & -105. & -60.6 & -35.5 & -36.2 \\
		\hline
		$8$ & -31.4 & -72.2 & -109. & -63.0 & -36.7 & -37.4 \\
		\hline
		$9$ & -32.6 & -73.2 & -112. & -65.1 & -37.8 & -38.5 \\
		\hline
	\end{tabularx}
	\centering\includegraphics[width=\linewidth]{scale}
\end{table}
In details, we see clearly that the PSD-based model predicts that OFDM/OQAM, FMT and FBMC-PAM can be used efficiently to coexist with CP-OFDM incumbent systems. On the contrary, GFDM and COQAM systems only reduce the interference onto incumbent systems by $5$ dB compared to CP-OFDM based secondary systems. This is caused by the circular filtering which incurs steep variations in the transmit signal and therefore causes projections on the whole spectrum.

\subsubsection{Limitations of the PSD-based model}

Though the PSD-based model resented above is widely used in the literature, it suffers some limitations: in particular, being based purely on frequency considerations, it is unable to encompass phenomenons related to time synchronism. This was tackled by Medjahdi \textit{et. al.} who showed in \cite{Medjahdi2010a,Medjahdi2012phd} that the main drawback of the PSD-based model lies in the fact that it cannot encompass time asynchronism between coexisting systems. This is a major drawback that does not enable us to analyze the effects of the time misalignment between users. For example, the PSD-based model predicts that two coexisting CP-OFDM incumbent systems will interfere onto each other, even if they are synchronized within the CP, which is in contradiction with the results we presented if Fig.~\ref{fig:TO_CFO}. In the following, we present alternative modeling approaches that enable one to take into account this asynchronism.

\subsection{Asynchronous interference modeling}
\subsubsection{Model definition}
Consider the system model of Fig.~\ref{fig:block_diagram_comm_system} and the spectral coexistence layout of Fig.~\ref{fig:spectral_distribution}. As there is no explicit synchronization between the coexisting systems, the secondary system \textsl{S} may be desynchronized with a timing offset $\delta\text{t}$ with regards to the incumbent system \textsl{I}. Not taking into account the effects of channel propagation and noise, the signal received at the incumbent receiver is expressed as
\begin{equation}
y_\textsl{I}(t) = s_\textsl{I}(t)+s_\textsl{S}(t-\delta\text{t}).
\end{equation}
Then, after demodulation, the interfering signal on the $n_0$-th symbol and $m_0$-th subcarrier of the incumbent system $\textsl{I}$ is obtained by computing the convolution product of the receive filter of the incumbent on subcarrier $m_0$ with the secondary signal $s_\textsl{S}(t-\delta\text{t})$ as follows
\begin{equation}
\boldsymbol\eta_{\textsl{I}, m_0}[n_0] = \int\limits_{-\infty}^\infty f_{n_0,\text R, \textsl I(t-n\Delta\text{T})}s_\textsl{S}(t-\delta\text{t})e^{-j2\pi m_0(t-n\Delta\text{T})} \dif t.
\end{equation}
Furthermore, given the expression of the multicarrier signal given in \eqref{eq:signal_sum_subcarriers}, 
\begin{equation}
s_\textsl{S}(t-\delta\text{t}) = \sum_{n\in\mathbb{Z}}\sum_{m\in\mathcal{M}_\textsl{S}}\mathbf{d}_\textsl{S,m}[n]f_{n,\text T,\textsl{I}}(t-\delta\text(t))e^{j2\pi m(t-n\Delta\text{T}-\delta\text{t})},
\end{equation}
and therefore, 
\begin{equation}
\boldsymbol\eta_{\textsl{I}, m_0}[n_0] = \sum_{m\in\mathcal{M}_\textsl{S}}\underset{\boldsymbol\eta_{\textsl{I}, m\rightarrow m_0}}{\underbrace{\sum_{n\in\mathbb{Z}}\mathbf{d}_\textsl{S,m}[n]\int\limits_{-\infty}^\infty f_{n,\text T,\textsl{I}}(t-\delta\text t)f_{n_0,\text R, \textsl I(t-n\Delta\text{T})}s_\textsl{S}(t-\delta\text{t})e^{j2\pi m(t-n\Delta\text{T}-\delta\text{t})}e^{-j2\pi m_0(t-n\Delta\text{T})} \dif t}},
\end{equation}
where $\boldsymbol\eta_{\textsl{I}, m\rightarrow m_0}$ represents the share of interference that is caused by subcarrier $m$. Then, the average amount of interference caused by subcarrier $m$ onto subcarrier $m_0$ is expressed as
\begin{equation}
I(m,m_0) = E_{\delta\text{t},\mathbf{d}_\textsl{S}}\left(|\boldsymbol\eta_{\textsl{I}, m\rightarrow m_0}|^2\right).
\end{equation}
Furthermore, as Medjahdi states in \cite{Medjahdi2012phd}, only the value difference $m-m_0$ affects the value of $\boldsymbol\eta_{\textsl{I}, m\rightarrow m_0}$. Therefore, defining $l=m-m_0$ as for the PSD-based model, we have
\begin{equation}
I_\text{AIM}^{\textsl{S}\rightarrow\textsl{I}}(l) = E_{\delta\text{t},\mathbf{d}_\textsl{S}}\left(|\boldsymbol\eta_{\textsl{I}, m\rightarrow m_0}|^2\right),
\end{equation}
where AIM stands for asynchronous interference modeling. Note that this model can directly be extended to take into account frequency misalignement between user $\delta\text{f}$.
\nomenclature[A]{AIM}{Asynchronous Interference Modeling}

\subsubsection{Asynchronous interference modeling applied to FB-MC waveforms}
In his thesis \cite{Medjahdi2012phd}, Medjahdi applied the AIM approach he defined to compute interference between asynchronous users based on either CP-OFDM or OFDM/OQAM. In the case of CP-OFDM, he obtained closed-form expressions of $I_\text{AIM}^{\textsl{S}\rightarrow\textsl{I}}(l)$ in the case where the timing offset between users $\delta\text{t}$ is uniformly distributed. However, explicit closed-forms are more difficult to obtain in the case of OFDM/OQAM due to the more intricate expression of the used prototype filter. Nevertheless, it was shown in \cite{Medjahdi2011} that using this modeling approach yielded accurate evaluation of key metrics in asynchronous networks, such as SINR, BER or spectral efficiency.

Furthermore, this approach can be straightforwardly applied to any other FB-MC waveforms, as it only requires the knowledge of the transmit and receive prototype filters used by the considered systems. However, as for the PSD-based model, this approach is not applicable to UFMC and f-OFDM. Indeed, the AIM approach computes the interference created by one subcarrier onto another, whereas f-OFDM and UFMC apply filtering on groups of subcarriers. Overall, whereas it is feasible to design systematic interference models for FB-MC waveforms because of the equivalence of each subcarrier, it is much more complicate to achieve the same for subband-filtered waveforms, in which different subcarriers are differently affected by the transmit and receive filter.

\subsubsection{Limitations of the asynchronous interference model}
Apart from its inapplicability to subband-filtered waveforms, the asynchronous interference modeling approach suffers a number of other shortcomings. In particular, the distribution followed by the interference signal is not clearly defined by this model, which only computes the average power of interference. The study in \cite{Medjahdi2012phd} assumes that the distribution of interference is Gaussian, but this has to be further validated. Furthermore, this approach has only been applied in homogeneous coexistence scenarios, in which the incumbent and secondary systems share the same waveform design. However, as we stated previously in this thesis, we are interested in studying heterogeneous coexistence deployments, in which only the secondary utilizes an enhanced waveform while the incumbent sticks to CP-OFDM. It is not yet clear how the AIM approach would apply in that particular context and this matter has to be investigated.

\subsection{Summary}
Given a spectral coexistence scenario in which two systems share the same spectral band, each of them transmitting on a subset of all available subcarriers, several approaches have been used to measure the level of interference that arises between them. The PSD-based approach has been widely used, as it gives a simple way to compute interference caused by the secondary system onto the incumbent by looking purely at the spectral localization of the secondary signal. However, because this model is based only on the properties of the transmit signal, it is inaccurate and a modeling approach that takes into account the receive operations by the incumbent system is necessary. In particular, the AIM approach has been proposed to take into account potential time offsets between the secondary transmitter and the incumbent receiver. This approach has proven to be much more accurate than the PSD-based model, but does not directly apply to the heterogeneous coexistence scenario we consider in which the secondary and the incumbent systems are based on different waveforms. The only work available on that topic so far is \cite{Ahmed2016} in which the authors study the coexistence between UFMC and CP-OFDM. However, a thorough study of coexistence between FB-MC waveforms and CP-OFDM is still missing.

Based on these observations, we provide the following contributions in chapter \ref{chapter:3}:
\begin{enumerate}
	\item We extend the AIM approach to our coexistence problem, and propose an EVM-based modeling of the interference between FB-MC and CP-OFDM users.
	\item We carefully analyze the statistical distribution of the interference signal arising between FB-MC and CP-OFDM users.
	\item We study the difference between the PSD-based model results and our modeling of interference.
	\item We provide closed-form expressions of the interference caused by multiple FB-MC waveforms onto CP-OFDM users.
\end{enumerate}
\nomenclature[A]{AIM}{Asynchronous Interference Modeling}

\section{Experimental studies}
\label{sec:experimental_studies}
In addition to theoretical and simulation-based studies cited here above, a number of experimental works have studied the coexistence between enhanced multicarrier waveforms and CP-OFDM. In \cite{Berg2014743}, the authors present a hardware implementation of an OFDM/OQAM secondary system that they propose to use to coexist with DVB-T systems in TVWS setups. They show that the secondary system can transmit with more power if it uses OFDM/OQAM instead of CP-OFDM. However, this result is based on qualitative observation of the TV signal quality received by the incumbent systems, which makes it hardly reproducible. In \cite{EurecomExp, EURECOM+4725}, the authors study the coexistence of multiple waveforms with legacy CP-OFDM based receivers. In details, they measure
experimentally the layer 3 goodput of a LTE uplink receiver confronted with interference coming from an asynchronous secondary transmitter. They show that if the latter uses GFDM, it can transmit approximately with 3 dB more power than a secondary that would use CP-OFDM. However, this work is based on high-level metrics, and, in the case where the secondary is based on CP-OFDM, it assumes the worst case scenario in which the secondary and incumbent system have a CFO of half a subcarrier. 
Furthermore, in \cite{KeysightVideo,KeysightWaveforms}, the authors measure the EVM and NMSE of a CP-OFDM receiver that faces interference from different enhanced waveforms. However, they do not properly compare the values that are achieved according to the waveform used by the secondary system, which makes it hard to use the results. Most recently in \cite{Wunsch2017Dyspan,Wunsch2017TCCN}, the authors have described an OFDM/OQAM overlay system that they used in the DySPAN 2017 spectrum contest to coexist with a CP-OFDM based incumbent network. However, their article only describes the architecture of the system and does not provide any insight on the gains obtained by using OFDM/OQAM instead of CP-OFDM. Overall, available experiments fail to provide low-level, reproducible results. At the end of chapter \ref{chapter:3}, we aim at solving this issue by presenting a software radio testbed of our own design that can accurately measure the interference caused by any FB-MC waveform on each subcarrier of a CP-OFDM receiver. The presented testbed can then be used to validate our theoretical analysis of interference.

\section{Enhanced waveforms for coexistence with incumbent CP-OFDM systems}
\label{sec:5G_coex_OFDM}
In the former section, we reviewed works that aim at precisely computing the level of interference created by the secondary user onto the primary system according to the waveform used by the former. Here, we summarize relevant works which do not focus on interference modeling but rather build up on available models to study the impact of the waveform used by the secondary system in different coexistence setups. First, we review some works addressing the impact of the used waveform in the field of CR. Then, we study the available literature addressing the use of enhanced multicarrier waveforms in the context of D2D communication. 

\subsection{CR context}
There is a plethora of research on the topic of the applicability of enhanced waveforms in CR setups, and we will therefore not make a list of all available works on the topic. Rather, we select representative studies and we pay a close attention to the assumptions that are made on the waveform used by both the secondary and the incumbent systems. Works on this subject can be roughly classified in two approaches.

The first category of works focuses on the improved spectral localization associated with the use of enhanced multicarrier waveforms to investigate to what extent they can facilitate the insertion of a secondary system in a spectral hole. These works usually analyze the amount of subcarriers that can be turned on in a given spectral hole, or the width of the guard band that is necessary to protect the incumbent system given a certain spectral mask \cite{skrzypczak2012ofdm,Bellanger2010RWS,Datta2011,kollar2010modulation,bogucka2015dynamic,Noguet2011,schellmann2014fbmc}. Overall, none of these works properly defines what waveform the incumbent system uses and they solely rely on the PSD of the transmit secondary signal to draw conclusions.

Another stream of research works has studied optimal resource and power allocation techniques for the secondary system \cite{shaat2010computationally,dikmese2014spectrumb,Zhang2012z}. These works usually rely on the PSD-based model to compute the level of interference that is injected by the secondary system onto the incumbent. Therefore, they do not take into account the specificities of the incumbent system either. On the other hand, some works explicitly study homogeneous coexistence scenario, in which both the incumbent and secondary systems utilize the same waveform \cite{Medjahdi2011,Zhang2010Eur,zhang2010uplink} and rely on the AIM-based model to take into account the effects of time misalignements between the two systems.
Overall, we therefore see that works on the application of enhanced waveforms in CR setups either overlook the impact of the waveform used by the incumbent system, or have only considered setups in which both systems use the same waveform. Thus, it is crucial to accurately study CR setups in which the secondary system uses an enhanced multicarrier waveform whereas the incumbent is based on CP-OFDM.

\subsection{D2D context}
As we have mentioned in chapter \ref{chapter:2}, enhanced waveforms look particularly appropriate in the realm of D2D communications given the fact that accurate synchronization between different D2D pairs, as well as between D2D pairs and cellular users, may be difficult or even impossible to achieve. Given the rising popularity of D2D in the latest years, some works have recently studied the potentials gains brought by the use of new waveforms in this new kind of communication setups. To the best of our knowledge, \cite{Xing2014} has been the first work to address this topic. In that work, the authors study a setup in which OFDM/OQAM based D2D users underlay an OFDMA network. However, they fail to properly study the interference arising between CP-OFDM and OFDM/OQAM users, and rely once more on PSD-based measures of interference. It is in fact the only work available in the literature that explicitly studies the coexistence between CP-OFDM based cellular users and D2D systems based on another enhanced multicarrier waveform. Some other works \cite{Pischella2016b,Pischella2016a,li2016downlink,Mukherjee2015,Haddadin2016,Wu2016,Wu2017} have investigated the use of enhanced multicarrier waveforms for D2D communication, but either did not consider the presence of cellular users or assumed that both cellular systems and D2D users are based on the same waveform. 
Therefore, exactly as in the context of CR, we see that it is crucial to properly investigate scenarios of coexistence between D2D systems based on enhanced multicarrier waveforms and legacy CP-OFDM based users.

\subsection{Summary}
Both in the D2D and CR context, heterogeneous setups in which only the secondary system utilizes an enhanced waveform whereas the incumbent still relies on CP-OFDM have not been properly analyzed in the literature. In chapter \ref{chapter:4}, we therefore study typical CR setups and present the following contributions:
\begin{enumerate}
	\item We use the interference model we will devise in chapter \ref{chapter:3} to compute the width of the guard band that is necessary to protect a CP-OFDM incumbent user, in function of the waveform used by the secondary.
	\item We analyze the optimal power distribution that can be assigned to FB-MC based secondary devices to maximize their capacity while limiting their interference to CP-OFDM based incumbent systems.
	\item Taking into account the TSE of each waveform we introduced in chapter \ref{chapter:2}, we compare the amount of data they are capable of transmitting in a window of specific time and frequency dimensions.
\end{enumerate}
Furthermore, in the context of D2D, we present the following contributions in chapter \ref{chapter:5}:
\begin{enumerate}
	\item We study a mono-cellular setup in which D2D devices can use either CP-OFDM or OFDM/OQAM. Doing so, we demonstrate the interest of using enhanced multi-carrier waveforms for D2D transmission.
	\item We extend this analysis to a multi-cellular setup and consider multiple enhanced waveforms for D2D transmissions. We investigate the performances of both the D2D and cellular systems for a variety of parameter values to better understand in what kind of deployments it is the most beneficial to change the waveform used by D2D systems.
\end{enumerate}


\section{Conclusion}
In this chapter, we have reviewed works that address the potential use of enhanced multicarrier waveforms to facilitate coexistence with incumbent systems. We have identified several flaws in the works available so far in the literature which are mostly due to the fact that the impact of the waveform used by the incumbent system has been largely overlooked. In particular, we have pointed out that no proper modeling of the interference caused by enhanced waveforms on CP-OFDM receivers has been proposed. This lack for a precise interference model casts doubts on the validity of some results presented in the context of CR or D2D. Furthermore, we have also pointed out that experiments studying the coexistence between CP-OFDM incumbent systems and secondary devices based on enhanced waveforms provide either high-level or non-reproducible results that are difficult to exploit. All these observations serve as basis for our work and the contributions we present in the three following chapters of this thesis.

\label{sec:litt_lim_and_contrib}

\nomenclature[A]{TSE}{Time-Spectral Efficiency}