\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\addvspace {10\p@ }
\contentsline {xchapter}{General Introduction}{3}{chapter.1}
\contentsline {figure}{\numberline {1.1}{\ignorespaces Radar chart representing some of the foreseen 5G use cases and the relevance of several technical challenges for each of them}}{5}{figure.caption.8}
\contentsline {figure}{\numberline {1.2}{\ignorespaces Comparison of OFDM and FB-MC in a coexistence scenario}}{9}{figure.caption.9}
\addvspace {10\p@ }
\contentsline {xchapter}{Background and problem statement}{15}{chapter.2}
\contentsline {figure}{\numberline {2.1}{\ignorespaces Block diagram of a generic digital communication system}}{17}{figure.caption.18}
\contentsline {figure}{\numberline {2.2}{\ignorespaces Representation of the three different channel propagation effects}}{19}{figure.caption.20}
\contentsline {figure}{\numberline {2.3}{\ignorespaces Time and frequency response of a channel realization corresponding to (a)~EPA and (b)~ETU channel models.}}{21}{figure.caption.22}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{21}{subfigure.3.1}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{21}{subfigure.3.2}
\contentsline {figure}{\numberline {2.4}{\ignorespaces Detailed structure of a multicarrier waveform modulation block}}{22}{figure.caption.24}
\contentsline {figure}{\numberline {2.5}{\ignorespaces Detailed structure of a multicarrier waveform demodulation block}}{23}{figure.caption.25}
\contentsline {figure}{\numberline {2.6}{\ignorespaces Detailed implementation of CP-OFDM transmission and reception chains.\relax }}{25}{figure.caption.27}
\contentsline {figure}{\numberline {2.7}{\ignorespaces Illustration of overlay OSA and underlay}}{27}{figure.caption.29}
\contentsline {figure}{\numberline {2.8}{\ignorespaces Example of a TV white space deployment}}{29}{figure.caption.31}
\contentsline {figure}{\numberline {2.9}{\ignorespaces Spectral masks defined for portable and fixed TVWS}}{30}{figure.caption.32}
\contentsline {figure}{\numberline {2.10}{\ignorespaces Coexistence of D2D communications and classical user equipments in cellular networks.\relax }}{31}{figure.caption.34}
\contentsline {figure}{\numberline {2.11}{\ignorespaces Typical classification of D2D spectrum allocation schemes}}{32}{figure.caption.35}
\contentsline {figure}{\numberline {2.12}{\ignorespaces GFDM prototype filters $\mathaccentV {tilde}07E{g}_n(t)$ obtained from an original RRC prototype filter $g$ with roll-off 0.25, $N_\text {b} = 5$ and $T_\text {CP} = \frac {T}{4}$.\relax }}{37}{figure.caption.39}
\contentsline {figure}{\numberline {2.13}{\ignorespaces PSD of each studied waveform in a setup where 12 subcarriers are activated.\relax }}{41}{figure.caption.47}
\contentsline {figure}{\numberline {2.14}{\ignorespaces Sensitivity of enhanced multicarrier waveforms to (a) TO and (b) CFO.\relax }}{43}{figure.caption.49}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{43}{subfigure.14.1}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{43}{subfigure.14.2}
\contentsline {figure}{\numberline {2.15}{\ignorespaces Time-spectral efficiency of enhanced multicarrier waveforms\relax }}{45}{figure.caption.51}
\contentsline {figure}{\numberline {2.16}{\ignorespaces Uncoded BER achieved by studied waveforms on an AWGN channel for an uncoded 16-QAM constellation.\relax }}{46}{figure.caption.53}
\addvspace {10\p@ }
\contentsline {xchapter}{Related work and corresponding contributions}{51}{chapter.3}
\contentsline {figure}{\numberline {3.1}{\ignorespaces Spectral representation of the coexistence scenario}}{52}{figure.caption.56}
\contentsline {figure}{\numberline {3.2}{\ignorespaces Principle of the PSD-based modeling of interference}}{53}{figure.caption.57}
\contentsline {figure}{\numberline {3.3}{\ignorespaces Principle of PSD-based modeling of interference, applied to two waveforms.}}{55}{figure.caption.59}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{55}{subfigure.3.1}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{55}{subfigure.3.2}
\contentsline {figure}{\numberline {3.4}{\ignorespaces PSD estimate of studied waveforms}}{56}{figure.caption.61}
\addvspace {10\p@ }
\contentsline {xchapter}{Analysis of interference between CP-OFDM and FB-MC waveforms}{65}{chapter.4}
\contentsline {figure}{\numberline {4.1}{\ignorespaces Coexistence system model}}{67}{figure.caption.67}
\contentsline {figure}{\numberline {4.2}{\ignorespaces EVM-based measurement of interference}}{71}{figure.caption.68}
\contentsline {figure}{\numberline {4.3}{\ignorespaces Interference caused onto a CP-OFDM based incumbent system by a secondary system based on multiple waveforms}}{73}{figure.caption.69}
\contentsline {figure}{\numberline {4.4}{\ignorespaces Key phenomenon explaining the difference between the EVM-based measurement of interference and the predictions of the PSD-based model}}{75}{figure.caption.70}
\contentsline {figure}{\numberline {4.5}{\ignorespaces Time axis view of the interference caused by an OFDM/OQAM transmission on CP-OFDM receiving windows $n_\textbf {i} = 0$ and $n_\textbf {i} = 1$.\relax }}{75}{figure.caption.71}
\contentsline {figure}{\numberline {4.6}{\ignorespaces Representation of the interference signal for the considered parameters}}{77}{figure.caption.72}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {4-QAM}}}{77}{subfigure.6.1}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {16-QAM}}}{77}{subfigure.6.2}
\contentsline {subfigure}{\numberline {(c)}{\ignorespaces {64-QAM}}}{77}{subfigure.6.3}
\contentsline {figure}{\numberline {4.7}{\ignorespaces BER of the incumbent system for different constellation orders}}{78}{figure.caption.73}
\contentsline {figure}{\numberline {4.8}{\ignorespaces Interference in dB caused by an active subcarrier 0 on 20 neighboring CP-OFDM subcarriers of the incumbent in function of $\delta _\text {t}$ for different waveforms.}}{80}{figure.caption.74}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {CP-OFDM}}}{80}{subfigure.8.1}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {FMT}}}{80}{subfigure.8.2}
\contentsline {subfigure}{\numberline {(c)}{\ignorespaces {OFDM/OQAM}}}{80}{subfigure.8.3}
\contentsline {subfigure}{\numberline {(d)}{\ignorespaces {FBMC-PAM}}}{80}{subfigure.8.4}
\contentsline {subfigure}{\numberline {(e)}{\ignorespaces {GFDM}}}{80}{subfigure.8.5}
\contentsline {subfigure}{\numberline {(f)}{\ignorespaces {COQAM}}}{80}{subfigure.8.6}
\contentsline {figure}{\numberline {4.9}{\ignorespaces Comparison between simulated values of average interference based on EVM (crosses) and analytical approximation of \textup {\hbox {\mathsurround \z@ \normalfont (\ignorespaces \ref {eq:eq25}\unskip \@@italiccorr )}} (solid line) for a) OFDM/OQAM, b) FMT, c) GFDM and d) COQAM.\relax }}{81}{figure.caption.75}
\contentsline {figure}{\numberline {4.10}{\ignorespaces Average sum power of noise and interference in dB on each subcarrier $m_I$ of the incumbent CP-OFDM system.}}{85}{figure.caption.78}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{85}{subfigure.10.2}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{85}{subfigure.10.3}
\contentsline {figure}{\numberline {4.11}{\ignorespaces Experimentation setup}}{86}{figure.caption.79}
\contentsline {figure}{\numberline {4.12}{\ignorespaces Comparison of the PSD of tested waveforms as seen on a spectrum analyzer.}}{88}{figure.caption.80}
\contentsline {figure}{\numberline {4.13}{\ignorespaces Developed GUI}}{89}{figure.caption.81}
\contentsline {figure}{\numberline {4.14}{\ignorespaces EVM on each subcarrier of the CP-OFDM incumbent receiver when a secondary user is transmitting on subcarriers $0$ to $35$ with different waveforms.}}{90}{figure.caption.82}
\addvspace {10\p@ }
\contentsline {xchapter}{Coexistence with CP-OFDM incumbent systems in CR setups}{93}{chapter.5}
\contentsline {figure}{\numberline {5.1}{\ignorespaces Interference seen on each subcarrier of the incumbent according to (a)the PSD-based model and (b)the EVM-based model presented in chapter 4.\relax }}{95}{figure.caption.83}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{95}{subfigure.1.1}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{95}{subfigure.1.2}
\contentsline {figure}{\numberline {5.2}{\ignorespaces Number of guard subcarriers necessary to protect the incumbent as a function of its interference threshold per subcarrier $I_\text {th}$ according to (a) the PSD-based model of the literature and (b) our EVM-based modeling.\relax }}{96}{figure.caption.84}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{96}{subfigure.2.1}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{96}{subfigure.2.2}
\contentsline {figure}{\numberline {5.3}{\ignorespaces Power allocated on each subcarrier of the secondary system according to the interference threshold set by the incumbent for (a) UFMC and (b) OFDM/OQAM.\relax }}{98}{figure.caption.85}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{98}{subfigure.3.1}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{98}{subfigure.3.2}
\contentsline {figure}{\numberline {5.4}{\ignorespaces Achievable rate (not accounting for TSE) by each studied waveform in the studied scenario as computed with (a) the PSD-based model and (b) our EVM-based approach.\relax }}{100}{figure.caption.86}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{100}{subfigure.4.1}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{100}{subfigure.4.2}
\contentsline {figure}{\numberline {5.5}{\ignorespaces Considered system setup: the secondary device transmits in the free band $\mathcal {M}_S$ that it can use during $T_\text {Tx}$\relax }}{101}{figure.caption.87}
\contentsline {figure}{\numberline {5.6}{\ignorespaces Total amount of transmittable data as a function of the available time to transmit $T_{\text {Tx}}$ for a) $I_{\text {th}} = -40\text { dB}$ and b) $I_{\text {th}} = -10\text { dB}$\relax }}{102}{figure.caption.88}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{102}{subfigure.6.1}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{102}{subfigure.6.2}
\contentsline {figure}{\numberline {5.7}{\ignorespaces Total amount of transmittable data as a function of the interference constraint $I_{\text {th}}$ for a) a short transmission window $T_{\text {Tx}}=\frac {15}{\Delta \text {F}}$ equivalent to one LTE TTI and b) a long transmission window $T_{\text {Tx}}=\frac {200}{\Delta \text {F}}$.\relax }}{103}{figure.caption.89}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{103}{subfigure.7.1}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{103}{subfigure.7.2}
\addvspace {10\p@ }
\contentsline {xchapter}{Asynchronous D2D operation in 5G networks}{105}{chapter.6}
\contentsline {figure}{\numberline {6.1}{\ignorespaces Simplified diagram showing two D2D pairs and one cellular user with both interference channels (dashed lines), and useful channels (solid black lines) outlined.\relax }}{107}{figure.caption.90}
\contentsline {figure}{\numberline {6.2}{\ignorespaces Interference tables measuring the value of interference injected between different couples of waveforms according to Chapter \ref {chapter:3} and \cite {Medjahdi2010a}.\relax }}{109}{figure.caption.93}
\contentsline {figure}{\numberline {6.3}{\ignorespaces Example of clustered scenario consisting of 10 D2D pairs.\relax }}{113}{figure.caption.101}
\contentsline {figure}{\numberline {6.4}{\ignorespaces (a) Clustered and (b) non-clustered scenario consisting of 10 D2D pairs.\relax }}{115}{figure.caption.103}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{115}{subfigure.4.1}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{115}{subfigure.4.2}
\contentsline {figure}{\numberline {6.5}{\ignorespaces Average rate per D2D pair for different numbers of D2D pairs.\relax }}{116}{figure.caption.105}
\contentsline {figure}{\numberline {6.6}{\ignorespaces Average rate per D2D pair versus number of D2D users in a cluster of fixed radius of 70m.\relax }}{117}{figure.caption.107}
\contentsline {figure}{\numberline {6.7}{\ignorespaces Average rate per D2D pair versus cluster radius for a fixed number of D2D pairs.\relax }}{118}{figure.caption.108}
\contentsline {figure}{\numberline {6.8}{\ignorespaces Average rate per D2D pair versus distance from cluster centre to BS.\relax }}{119}{figure.caption.109}
\contentsline {figure}{\numberline {6.9}{\ignorespaces Frequency reuse scheme}}{121}{figure.caption.111}
\contentsline {figure}{\numberline {6.10}{\ignorespaces Representative examples of interference tables}}{126}{figure.caption.118}
\contentsline {figure}{\numberline {6.11}{\ignorespaces Example scenario consisting of 19 cells}}{128}{figure.caption.121}
\contentsline {figure}{\numberline {6.12}{\ignorespaces The box plots of DUE SINR show that a large performance increase can be obtained by choosing an appropriate alternative waveform.\relax }}{131}{figure.caption.125}
\contentsline {figure}{\numberline {6.13}{\ignorespaces Rate performance of DUEs taking into account bandwidth efficiency.\relax }}{132}{figure.caption.127}
\contentsline {figure}{\numberline {6.14}{\ignorespaces The DUE to CUE interference is similar to the value of noise per resource block for coexistence cases.\relax }}{133}{figure.caption.129}
\contentsline {figure}{\numberline {6.15}{\ignorespaces Increasing DUE transmit power results in an increase in DUE SINR at the cost of increased interference to CUEs.\relax }}{134}{figure.caption.131}
\contentsline {figure}{\numberline {6.16}{\ignorespaces As the cell radius increases, DUE SINR increases and reduction in CUE SINR decreases.\relax }}{136}{figure.caption.133}
\contentsline {figure}{\numberline {6.17}{\ignorespaces Employing an appropriate alternative waveform for DUEs yields the greatest benefit in small clusters in which inter-DUE leakage interference is most significant.\relax }}{138}{figure.caption.135}
\contentsline {figure}{\numberline {6.18}{\ignorespaces DUE SINR performance as the maximum permitted timing offset is varied.\relax }}{140}{figure.caption.138}
\contentsline {figure}{\numberline {6.19}{\ignorespaces DUE SINR performance as the maximum CFO is varied.\relax }}{142}{figure.caption.140}
\addvspace {10\p@ }
\contentsline {xchapter}{Conclusions and perspectives}{145}{chapter.7}
\addvspace {10\p@ }
\contentsline {xchapter}{Derivation of $A_{\Pi _T,g}(\tau ,\nu )$}{153}{Appendix.a.A}
\addvspace {10\p@ }
\contentsline {xchapter}{SCEE Testbed}{157}{Appendix.a.B}
\contentsline {figure}{\numberline {B.1}{\ignorespaces Testbed organization and USRP identification\relax }}{159}{figure.caption.160}
\contentsline {figure}{\numberline {B.2}{\ignorespaces Ethernet switch cabling\relax }}{160}{figure.caption.161}
\contentsline {figure}{\numberline {B.3}{\ignorespaces Detailed cabling diagram of all testbed components. USRPs are represented via their id.\relax }}{160}{figure.caption.162}
\contentsline {figure}{\numberline {B.4}{\ignorespaces Interface of the SCEE testbed monitor.}}{161}{figure.caption.163}
\addvspace {10\p@ }
\contentsline {xchapter}{Network-level simulator}{165}{Appendix.a.C}
\contentsline {figure}{\numberline {C.1}{\ignorespaces Class diagram of the Network Simulator\relax }}{166}{figure.caption.164}
\addvspace {10\p@ }
\contentsline {xchapter}{Publications and Involvement in R\&D Projects}{173}{Appendix.a.D}
